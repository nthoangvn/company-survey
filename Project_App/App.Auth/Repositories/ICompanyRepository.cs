﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Common.Domain;
using App.Common.Repositories;

namespace App.Auth.Repositories
{
    public interface ICompanyRepository : IGenericRepository<BaseEntities_BranchCompany>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        BaseEntities_BranchCompany GetBranchCompanyByDirectorId(int id);

        /// <summary>
        /// Get All Branch Company
        /// </summary>
        /// <returns></returns>
        List<BaseEntities_BranchCompany> GetAllBranchCompany();
        BaseEntities_BranchCompany GetBranchCompanyById(int branchCompanyId);


        /// <summary>
        /// Get Selected Branches
        /// </summary>
        /// <param name="branchIds"></param>
        /// <returns></returns>
        List<BaseEntities_BranchCompany> GetSelectedBranches(List<int> branchIds);


    }
}
