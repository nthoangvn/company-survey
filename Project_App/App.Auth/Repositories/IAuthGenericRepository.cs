﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Common.Repositories;

namespace App.Auth.Repositories
{
    public interface IAuthGenericRepository<T> : IGenericRepository<T>
    {
        void SaveChangeWithDisableDetectChange();
    }
}
