﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Common.Domain;
using App.Common.Repositories;

namespace App.Auth.Repositories
{
    public class CompanyRepository : AuthGenericRepository<BaseEntities_BranchCompany>, ICompanyRepository
    {
//        public CompanyRepository(CompanyContext dbContext) : base(dbContext)
//        {
//
//        }

        public BaseEntities_BranchCompany GetBranchCompanyByDirectorId(int id)
        {
            return DbContext.BaseEntities_BranchCompany.FirstOrDefault(c => c.Director_Id.Value == id);
        }

        public List<BaseEntities_BranchCompany> GetAllBranchCompany()
        {
            return FindAll(u => u.Director_Id.HasValue).ToList();
        }

        public BaseEntities_BranchCompany GetBranchCompanyById(int branchCompanyId)
        {
            return Find(u => u.Id == branchCompanyId);
        }

        public List<BaseEntities_BranchCompany> GetSelectedBranches(List<int> branchIds)
        {
            return FindAllByIdList(branchIds, b => b.BaseEntities_Director).ToList();
        }
    }
}
