﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using App.Common.Domain;
using App.Common.Repositories;

namespace App.Auth.Repositories
{
    public class AuthRepository : AuthGenericRepository<BaseEntities_IdentityUser>, IAuthRepository
    {
//        public AuthRepository(CompanyContext dbContext) : base(dbContext)
//        {
//        }

        public BaseEntities_IdentityUser GetUserByEmail(string email)
        {
            return DbContext.BaseEntities_IdentityUser.Include(u => u.BaseEntities_Director).FirstOrDefault(u => u.BaseEntities_Director.Email == email);
        }
        public string GetPasswordFromEmailUser(string email)
        {
            var password = Find(u => u.BaseEntities_Director.Email == email)?.Password;
            return password;
        }
    }
}
