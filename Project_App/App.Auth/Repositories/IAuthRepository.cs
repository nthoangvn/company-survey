﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Common.Domain;
using App.Common.Repositories;

namespace App.Auth.Repositories
{
    public interface IAuthRepository : IGenericRepository<BaseEntities_IdentityUser>
    {
        string GetPasswordFromEmailUser(string email);

        /// <summary>
        /// Get User By Email
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        BaseEntities_IdentityUser GetUserByEmail(string email);
    }
}
