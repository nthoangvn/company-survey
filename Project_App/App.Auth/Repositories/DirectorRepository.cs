﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Common.Domain;
using App.Common.Repositories;

namespace App.Auth.Repositories
{
    public class DirectorRepository : AuthGenericRepository<BaseEntities_Director>, IDirectorRepository
    {
//        public DirectorRepository(CompanyContext dbContext) : base(dbContext)
//        {
//
//        }

        public BaseEntities_Director GetUserById(int id)
        {
            return DbContext.BaseEntities_Director.FirstOrDefault(u => u.Id == id);
        }

    }
}
