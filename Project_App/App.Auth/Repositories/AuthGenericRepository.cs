﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Auth.Repositories;
using App.Common.Domain;
using App.Common.Factory;
using App.Common.Repositories;

namespace App.Auth.Repositories
{
    public class AuthGenericRepository<T> : GenericRepository<T, AuthDbContext>, IAuthGenericRepository<T> where T : BaseEntity
    {
        public override AuthDbContext DbContext => DbContextFactory.GetContextPerRequest<AuthDbContext>();

        public void SaveChangeWithDisableDetectChange()
        {
            DbContext.Configuration.AutoDetectChangesEnabled = false;
            Commit();
            DbContext.Configuration.AutoDetectChangesEnabled = true;
        }
    }
}
