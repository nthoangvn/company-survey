﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Common.Domain;
using App.Common.Repositories;

namespace App.Auth.Repositories
{
    public interface IDirectorRepository : IGenericRepository<BaseEntities_Director>
    {
        BaseEntities_Director GetUserById(int id);
    }
}
