﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Auth.Representations
{
    public class BranchCompanyRepresentation
    {
        public int Id { get; set; }
        public string CountryName { get; set; }

        public string City { get; set; }

        public string Address { get; set; }
    }
}
