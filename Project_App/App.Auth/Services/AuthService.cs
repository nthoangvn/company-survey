﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Auth.Helper;
using App.Auth.Repositories;
using App.Common.Domain;

namespace App.Auth.Services
{
    public class AuthService : IAuthService
    {
        private readonly IAuthRepository _authRepository;
        private readonly IDirectorRepository _directorRepository;
        public AuthService(IAuthRepository authRepository, IDirectorRepository directorRepository)
        {
            _authRepository = authRepository;
            _directorRepository = directorRepository;
        }

        public BaseEntities_IdentityUser Authenticate(string username, string password)
        {
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
                return null;

            var user = _authRepository.GetUserByEmail(username);

            // check if username exists
            if (user == null)
                return null;

            // check if password is correct
            if (!CompareHashPasswordwithUserPassword(password, username))
                return null;

            // authentication successful
            return user;
        }

        public BaseEntities_Director GetUserById(int id)
        {
            var user = _directorRepository.GetUserById(id);
            return user;
        }

        public bool CompareHashPasswordwithUserPassword(string userPassword, string email)
        {
            if (string.IsNullOrEmpty(email) || string.IsNullOrEmpty(userPassword))
            {
                return false;
            }
            var saltKey = AuthHelper.GetSalt();
            var truePassword = _authRepository.GetPasswordFromEmailUser(email);
            if (string.IsNullOrEmpty(truePassword))
            {
                return false;
            }
            var hashTruePassword = AuthHelper.GetHash(truePassword, saltKey);
            return AuthHelper.CompareHash(userPassword, hashTruePassword, saltKey);
        }

        private static bool VerifyPasswordHash(string password, byte[] storedHash, byte[] storedSalt)
        {
            if (password == null) throw new ArgumentNullException("password");
            if (string.IsNullOrWhiteSpace(password)) throw new ArgumentException("Value cannot be empty or whitespace only string.", "password");
            if (storedHash.Length != 64) throw new ArgumentException("Invalid length of password hash (64 bytes expected).", "passwordHash");
            if (storedSalt.Length != 128) throw new ArgumentException("Invalid length of password salt (128 bytes expected).", "passwordHash");

            using (var hmac = new System.Security.Cryptography.HMACSHA512(storedSalt))
            {
                var computedHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
                for (int i = 0; i < computedHash.Length; i++)
                {
                    if (computedHash[i] != storedHash[i]) return false;
                }
            }

            return true;
        }
    }
}
