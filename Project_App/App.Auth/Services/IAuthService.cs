﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Common.Domain;

namespace App.Auth.Services
{
    public interface IAuthService
    {
        /// <summary>
        /// Compare Password in DB with User Password
        /// </summary>
        /// <param name="userPassword"></param>
        /// <param name="email"></param>
        /// <returns></returns>
        bool CompareHashPasswordwithUserPassword(string userPassword, string email);

        /// <summary>
        /// Authenticate User
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        BaseEntities_IdentityUser Authenticate(string username, string password);

        /// <summary>
        /// Get User By Director Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        BaseEntities_Director GetUserById(int id);
    }
}
