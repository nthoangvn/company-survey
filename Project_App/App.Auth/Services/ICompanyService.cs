﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Common.Domain;

namespace App.Auth.Services
{
    public interface ICompanyService
    {
        /// <summary>
        /// Get Branch Company By Director Id
        /// </summary>
        /// <param name="director"></param>
        /// <returns></returns>
        BaseEntities_BranchCompany GetBranchCompanyByDirectorId(BaseEntities_Director director);

        /// <summary>
        /// Get All Branch Company
        /// </summary>
        /// <returns></returns>
        List<BaseEntities_BranchCompany> GetAllBranchCompany();
        BaseEntities_BranchCompany GetBranchCompanyById(int branchCompanyId);

        /// <summary>
        /// Get Selected Branches include Director
        /// </summary>
        /// <param name="branchIds"></param>
        /// <returns></returns>
        List<BaseEntities_BranchCompany> GetSelectedBranches(List<int> branchIds);
    }
}
