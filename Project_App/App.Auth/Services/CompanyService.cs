﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Auth.Repositories;
using App.Common.Enum;
using App.Common.Domain;

namespace App.Auth.Services
{
    public class CompanyService : ICompanyService
    {
        private readonly ICompanyRepository _companyRepository;

        public CompanyService(ICompanyRepository companyRepository)
        {
            _companyRepository = companyRepository;
        }

        public BaseEntities_BranchCompany GetBranchCompanyByDirectorId(BaseEntities_Director director)
        {
            var company = new BaseEntities_BranchCompany();
            if (director.IsActive && director.Role == DirectorRole.BranchDirector)
            {
                company = _companyRepository.GetBranchCompanyByDirectorId(director.Id);
            }

            return company;
        }

        public List<BaseEntities_BranchCompany> GetAllBranchCompany()
        {
            return _companyRepository.GetAllBranchCompany();
        }

        public BaseEntities_BranchCompany GetBranchCompanyById(int branchCompanyId)
        {
            return _companyRepository.GetBranchCompanyById(branchCompanyId);
        }

        public List<BaseEntities_BranchCompany> GetSelectedBranches(List<int> branchIds)
        {
            return _companyRepository.GetSelectedBranches(branchIds);
        }

//        public Dictionary<int, List<BaseEntities_Department>> FindAllDepartmentOfCompanyDic(List<int> branchIds)
//        {
//            
//        }
    
    }
}
