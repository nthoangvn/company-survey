﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;
using App.Auth.Dto;
using App.Auth.Helper;
using App.Auth.Representations;
using App.Auth.Services;
using App.Common.Controllers;
using App.Common.Domain;
using App.Common.Enum;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Extensions.Options;

namespace App.Auth.Controllers
{
    [RoutePrefix("api/auth")]
    public class AuthController : BaseApiController
    {
        private readonly IAuthService _authService;
        private readonly ICompanyService _companyService;
        public AuthController(IAuthService authService, ICompanyService companyService)
        {
            _authService = authService;
            _companyService = companyService;
        }

        [HttpGet]
        [Route("{id}/me")]
        public UserRepresentation GetUser(int id)
        {
            var user = _authService.GetUserById(id);
            var result = Mapper.Map<BaseEntities_Director, UserRepresentation>(user);
            if (user.Role == DirectorRole.BranchDirector)
            {
                var company = _companyService.GetBranchCompanyByDirectorId(user);
                result.Company = Mapper.Map<BaseEntities_BranchCompany, BranchCompanyRepresentation>(company);
            }

            return result;
        }

        [HttpPost]
        [Route("login")]
        public IHttpActionResult LoginToAuthenticate([FromBody] UserDto userInfo)
        {
            if (userInfo.IsLogined)
            {
                ThrowHttpResponseException(HttpStatusCode.BadRequest, "Error when Login");
            }
            var isAcceptedUser = _authService.CompareHashPasswordwithUserPassword(userInfo.Password, userInfo.Email);
            userInfo.IsLogined = isAcceptedUser;
            if (!userInfo.IsLogined)
            {
                userInfo.LoginFailMessage = "Login request is unaccepted. Please try again";
            }
            return Ok(userInfo);
        }

        [HttpPost]
        [Route("authenticate")]
        public IHttpActionResult Authenticate([FromBody]UserDto userDto)
        {
            var user = _authService.Authenticate(userDto.Email, userDto.Password);

            if (user == null)
                return Unauthorized();

            var tokenHandler = new JwtSecurityTokenHandler();
            //var key = Encoding.ASCII.GetBytes("1024");
            var key = new RSACryptoServiceProvider(2048);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.Id.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new RsaSecurityKey(key), SecurityAlgorithms.RsaSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            var tokenString = tokenHandler.WriteToken(token);
            var countryName = _companyService.GetBranchCompanyByDirectorId(user.BaseEntities_Director).CountryName;
            var result = new UserDto
            {
                Id = user.BaseEntities_Director.Id,
                Email = user.BaseEntities_Director.Email,
                FirstName = user.BaseEntities_Director.FirstName,
                LastName = user.BaseEntities_Director.LastName,
                IsLogined = true,
                Role = user.BaseEntities_Director.Role,
                CountryName = countryName,
                Token = tokenString
            };
            // return basic user info (without password) and token to store client side
            return Ok(result);
        }

        public UserRepresentation GetUserById(int id)
        {
            var user = _authService.GetUserById(id);
            var result = Mapper.Map<BaseEntities_Director, UserRepresentation>(user);
            if (user.Role == DirectorRole.BranchDirector)
            {
                var company = _companyService.GetBranchCompanyByDirectorId(user);
                result.Company = Mapper.Map<BaseEntities_BranchCompany, BranchCompanyRepresentation>(company);
            }

            return result;
        }

        [HttpGet]
        [Route("branches")]
        public List<BranchCompanyRepresentation> GetAllBranchCompany()
        { 
            var branchCompanyList = _companyService.GetAllBranchCompany();
            return Mapper.Map<List<BaseEntities_BranchCompany>, List<BranchCompanyRepresentation>>(branchCompanyList);
        }

        [HttpGet]
        [Route("branches/{branchId}")]
        public BranchCompanyRepresentation GetBranchCompanyById(int branchId)
        {
            var branchCompany = _companyService.GetBranchCompanyById(branchId);
            return Mapper.Map<BaseEntities_BranchCompany, BranchCompanyRepresentation>(branchCompany);
        }

        [HttpGet]
        [Route("branches/user/{directorId}")]
        public BranchCompanyRepresentation GetBranchCompanyByDirectorId(int directorId)
        {
            var director = _authService.GetUserById(directorId);
            var branchCompany = _companyService.GetBranchCompanyByDirectorId(director);
            return Mapper.Map<BaseEntities_BranchCompany, BranchCompanyRepresentation>(branchCompany);
        }
    }
}
