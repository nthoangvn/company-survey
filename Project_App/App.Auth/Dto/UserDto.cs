﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Common.Enum;

namespace App.Auth.Dto
{
    public class UserDto
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Password { get; set; }
        public bool IsLogined { get; set; }
        public DirectorRole Role { get; set; }
        public string LoginFailMessage { get; set; }
        public string Token { get; set; }
        public string CountryName { get; set; }
    }
}
