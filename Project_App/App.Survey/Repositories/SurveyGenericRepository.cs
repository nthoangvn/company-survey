﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Common.Domain;
using App.Common.Factory;
using App.Common.Repositories;

namespace App.Survey.Repositories
{
    public class SurveyGenericRepository<T> : GenericRepository<T, SurveyDbContext>, ISurveyGenericRepository<T> where T : BaseEntity
    {
        public override SurveyDbContext DbContext => DbContextFactory.GetContextPerRequest<SurveyDbContext>();

        public void SaveChangeWithDisableDetectChange()
        {
            DbContext.Configuration.AutoDetectChangesEnabled = false;
            Commit();
            DbContext.Configuration.AutoDetectChangesEnabled = true;
        }
    }
}
