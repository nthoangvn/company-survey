﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using App.Common.Domain;
using App.Common.Repositories;

namespace App.Survey.Repositories
{
    public class TotalSurveyRepository : SurveyGenericRepository<BaseEntities_TotalSurvey>, ITotalSurveyRepository
    {
//        public TotalSurveyRepository(CompanyContext dbContext) : base(dbContext)
//        {
//        }

        public IList<BaseEntities_TotalSurvey> GetTotalSurveyByDirector(int directorId)
        {
            var totalSurveyList =  FindAll(x=> x.DirectorId == directorId, x => x.BaseEntities_SingleSurvey);
            return totalSurveyList;
        }
        public BaseEntities_TotalSurvey GetTotalSurveyById(int id)
        {
            var totalSurvey = Find(x => x.Id == id);
            return totalSurvey;
        }

        public BaseEntities_TotalSurvey GetTotalSurveyDetail(int id)
        {
            return Find(x => x.Id == id, x => x.BaseEntities_TotalSurveyHistory);
        }

        public BaseEntities_TotalSurvey GetTotalSurveyDetailWithSingleSurvey(int id)
        {
            return Find(x => x.Id == id, x => x.BaseEntities_TotalSurveyHistory, x => x.BaseEntities_SingleSurvey);
        }

        public void AddOrUpdateTotalSurvey(BaseEntities_TotalSurvey totalSurvey)
        {
            AddOrUpdate(totalSurvey);
            Commit();
        }
        
        public void DeleteTotalSurvey(int directorId, int surveyId)
        {
            var totalSurvey = Find(x => x.Id == surveyId, x => x.BaseEntities_TotalSurveyHistory);
            var success = Remove(totalSurvey, true);
        }
    }
}
