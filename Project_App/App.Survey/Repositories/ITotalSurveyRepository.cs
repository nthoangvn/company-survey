﻿using System.Collections.Generic;
using App.Common.Domain;
using App.Common.Repositories;

namespace App.Survey.Repositories
{
    public interface ITotalSurveyRepository : IGenericRepository<BaseEntities_TotalSurvey>
    {
        IList<BaseEntities_TotalSurvey> GetTotalSurveyByDirector(int directorId);

        void DeleteTotalSurvey(int directorId, int surveyId);

        /// <summary>
        /// Add Or Update Total Survey
        /// </summary>
        /// <param name="totalSurvey"></param>
        void AddOrUpdateTotalSurvey(BaseEntities_TotalSurvey totalSurvey);

        /// <summary>
        /// Get Total Survey By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        BaseEntities_TotalSurvey GetTotalSurveyById(int id);

        /// <summary>
        /// GetTotalSurveyDetail
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        BaseEntities_TotalSurvey GetTotalSurveyDetail(int id);

        /// <summary>
        /// GetTotalSurveyDetailWithSingleSurvey
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        BaseEntities_TotalSurvey GetTotalSurveyDetailWithSingleSurvey(int id);
    }
}
