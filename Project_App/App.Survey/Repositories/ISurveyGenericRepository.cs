﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Common.Repositories;

namespace App.Survey.Repositories
{
    public interface ISurveyGenericRepository<T> : IGenericRepository<T>
    {
        void SaveChangeWithDisableDetectChange();
    }
}
