﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Common.Domain;
using App.Common.Repositories;

namespace App.Survey.Repositories
{
    public class AccountInSurveyRepository : SurveyGenericRepository<BaseEntities_AccountsInSingleSurvey>, IAccountInSurveyRepository
    {
        //        public AccountInSurveyRepository(CompanyContext dbContext) : base(dbContext)
        //        {
        //        }
        public IList<BaseEntities_AccountsInSingleSurvey> GetAccountInSingleSurveyByIdList(List<int> idList)
        {
            var accountInSingleSurvey = FindAll(x => idList.Contains(x.Id));
            return accountInSingleSurvey;
        }
    }
}
