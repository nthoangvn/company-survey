﻿
using App.Common.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Common.Domain;

namespace App.Survey.Repositories
{
    public interface ISingleSurveyRepository : IGenericRepository<BaseEntities_SingleSurvey>
    {
        IList<BaseEntities_SingleSurvey> GetSingleSurveyByBranchCompany(int branchCompanyId);
        IList<BaseEntities_SingleSurvey> GetAllSingleSurveys();
        void DeleteSingleSurvey(int directorId, int surveyId);

        /// <summary>
        /// Get Single Surveys By Total Survey Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        List<BaseEntities_SingleSurvey> GetSingleSurveysByTotalSurveyId(int id);

        /// <summary>
        /// Get Single Survey Detail
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        BaseEntities_SingleSurvey GetSingleSurveyDetail(int id);
    }
}
