﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using App.Common.Domain;
using App.Common.Repositories;

namespace App.Survey.Repositories
{
    public class SingleSurveyRepository : SurveyGenericRepository<BaseEntities_SingleSurvey>, ISingleSurveyRepository
    {
//        public SingleSurveyRepository(CompanyContext dbContext) : base(dbContext)
//        {
//        }

        public IList<BaseEntities_SingleSurvey> GetSingleSurveyByBranchCompany(int branchCompanyId)
        {
            var singleSurveyList = FindAll(x => x.BranchCompanyId == branchCompanyId, x => x.BaseEntities_TotalSurvey);
            return singleSurveyList;
        }

        public IList<BaseEntities_SingleSurvey> GetAllSingleSurveys()
        {
            var singleSurveyList = FindAll();
            return singleSurveyList;
        }

        public void DeleteSingleSurvey(int directorId, int surveyId)
        {
            var singleSurvey = Find(x => x.Id == surveyId);
            var success = Remove(singleSurvey, true);
        }

        public List<BaseEntities_SingleSurvey> GetSingleSurveysByTotalSurveyId(int id)
        {
            return FindAll(s => s.TotalSurveyId == id).OrderBy(s => s.TotalSurveyId).ToList();
        }

        public BaseEntities_SingleSurvey GetSingleSurveyDetail(int id)
        {
            var singleSurvey = Find(x => x.Id == id, x => x.BaseEntities_BranchCompany,
                x => x.BaseEntities_AccountsInSingleSurvey,
                x => x.BaseEntities_TotalSurvey, 
                x => x.BaseEntities_BranchCompany,
                x => x.BaseEntities_BranchCompany.BaseEntities_Director,
                x => x.BaseEntities_SingleSurveyHistory);
            return singleSurvey;
        }
    }
}
