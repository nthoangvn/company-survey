﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Common.Domain;
using App.Common.Repositories;

namespace App.Survey.Repositories
{
    public interface IAccountInSurveyRepository : IGenericRepository<BaseEntities_AccountsInSingleSurvey>
    {
        /// <summary>
        /// GetAccountInSingleSurveyByIdList
        /// </summary>
        /// <param name="idList"></param>
        /// <returns></returns>
        IList<BaseEntities_AccountsInSingleSurvey> GetAccountInSingleSurveyByIdList(List<int> idList);
    }
}
