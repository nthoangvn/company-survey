﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Common.Domain;

namespace App.Survey.Dto
{
    public class SingleSurveyAndStaffDto
    {
        public BaseEntities_SingleSurvey singleSurvey { get; set; }

        public List<BaseEntities_AccountOfStaff> staffAccountsList { get; set; }
    }
}
