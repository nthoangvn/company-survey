﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Survey.Dto
{
    public class TotalSurveyDto
    {
        public string SurveyName { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime? CloseDate { get; set; }

        public List<int> BranchCompanyIdList { get; set; }

        public int? DirectorId { get; set; }

        public DateTime PublishDeadline { get; set; }

        public DateTime SubmitDeadline { get; set; }

        public DateTime ValidateDeadline { get; set; }

        public int SurveyYear { get; set; }
    }
}
