﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Survey.Dto
{
    public class SingleSurveyDetailVO
    {
        public int Id { get; set; }
        public List<SalaryValueVO> SalaryValues { get; set; }
    }
}
