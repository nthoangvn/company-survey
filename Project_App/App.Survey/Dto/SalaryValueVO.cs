﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Survey.Dto
{
    public class SalaryValueVO
    {
        public int SalaryId { get; set; }
        public decimal SalaryValue { get; set; }
    }
}
