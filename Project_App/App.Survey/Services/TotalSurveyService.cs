﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Auth.Services;
using App.Survey.Repositories;
using App.Common.Domain;
using App.Common.Enum;
using App.Survey.Dto;
using AutoMapper;
using System.Web.Http;
using App.StaffAccounts.Services;
using System.Transactions;
using Ninject.Infrastructure.Language;
using IsolationLevel = System.Transactions.IsolationLevel;

namespace App.Survey.Services
{
    public class TotalSurveyService : ITotalSurveyService
    {
        private readonly ITotalSurveyRepository _totalSurveyRepository;
        private readonly ISingleSurveyRepository _singleSurveyRepository;
        private readonly ISingleSurveyService _singleSurveyService;
        private readonly IAuthService _authService;
        private readonly IDepartmentService _departmentService;
        private readonly ICompanyService _companyService;
        private readonly IAccountInSurveyRepository _accountInSurveyRepository;

        public TotalSurveyService(ITotalSurveyRepository totalSurveyRepository, IAuthService authService, 
            ISingleSurveyRepository singleSurveyRepository, IDepartmentService departmentService, 
            ICompanyService companyService, ISingleSurveyService singleSurveyService,
            IAccountInSurveyRepository accountInSurveyRepository)
        {
            _totalSurveyRepository = totalSurveyRepository;
            _authService = authService;
            _singleSurveyRepository = singleSurveyRepository;
            _departmentService = departmentService;
            _companyService = companyService;
            _singleSurveyService = singleSurveyService;
            _accountInSurveyRepository = accountInSurveyRepository;
        }

        public List<BaseEntities_TotalSurvey> GetTotalSurveyByDirector(int directorId)
        {
            var totalSurveyList = _totalSurveyRepository.GetTotalSurveyByDirector(directorId).ToList();
            List<BaseEntities_TotalSurvey> totalSurveys = null;
            if (totalSurveyList != null)
            {
                totalSurveys = totalSurveyList.ToList();
            }
            return totalSurveys;
        }

        public BaseEntities_TotalSurvey GetTotalSurveyDetail(int id)
        {
            var totalSurvey = _totalSurveyRepository.GetTotalSurveyDetail(id);
            return totalSurvey;
        }

        public BaseEntities_TotalSurvey GetTotalSurveyDetailWithSingleSurvey(int id)
        {
            var totalSurvey = _totalSurveyRepository.GetTotalSurveyDetailWithSingleSurvey(id);
            var singleSurveyList = totalSurvey.BaseEntities_SingleSurvey;
            var branchCompany =
                _companyService.GetSelectedBranches(singleSurveyList.Select(s => s.BranchCompanyId).ToList());
            singleSurveyList.Map(single =>
            {
                var branch = branchCompany.FirstOrDefault(b => b.Id == single.BranchCompanyId);
                if (branch != null)
                {
                    single.BaseEntities_BranchCompany = branch;
                }
            });
            totalSurvey.BaseEntities_SingleSurvey = singleSurveyList;
            return totalSurvey;
        }

        public BaseEntities_TotalSurvey UpdateTotalSurvey(int id, TotalSurveyDto updatedDto)
        {
            var totalSurvey = GetTotalSurveyDetail(id);
            totalSurvey.SurveyName = updatedDto.SurveyName;
            totalSurvey.BranchCompanyIdList = string.Join(",", updatedDto.BranchCompanyIdList);
            totalSurvey.PublishDeadline = updatedDto.PublishDeadline;
            totalSurvey.SubmitDeadline = updatedDto.SubmitDeadline;
            totalSurvey.ValidateDeadline = updatedDto.ValidateDeadline;
            totalSurvey.SurveyYear = updatedDto.SurveyYear;
            foreach (var noteHistory in totalSurvey.BaseEntities_TotalSurveyHistory)
            {
                noteHistory.Date = DateTime.Now;
                noteHistory.SurveyName = totalSurvey.SurveyName;
            }
            _totalSurveyRepository.AddOrUpdate(totalSurvey);
            _totalSurveyRepository.Commit();
            return totalSurvey;
        }

        public BaseEntities_TotalSurvey CreateTotalSurvey(BaseEntities_TotalSurvey totalSurvey)
        {
            if (totalSurvey.DirectorId == null)
            {
                return null;
            }


            totalSurvey.Status = TotalSurveyStatus.Created;

            
            totalSurvey.CreatedDate = DateTime.Now;
            totalSurvey.SingleSurveyQuantity = 0;
            _totalSurveyRepository.AddOrUpdateTotalSurvey(totalSurvey);

            var currentUser = _authService.GetUserById(totalSurvey.DirectorId.Value);
            var author = $"{currentUser.GetFullName()} - General Company";
            totalSurvey.BaseEntities_TotalSurveyHistory.Add(new BaseEntities_TotalSurveyHistory
            {
                Author = author,
                Date = DateTime.Now,
                BaseEntities_TotalSurvey = null,
                SurveyName = totalSurvey.SurveyName,
                SurveyStatus = totalSurvey.Status.ToString(),
                TotalSurveyId = totalSurvey.Id
            });
            _totalSurveyRepository.AddOrUpdateTotalSurvey(totalSurvey);
            return totalSurvey;
        }

        public BaseEntities_TotalSurvey CompleteSurvey(int id)
        {
            var survey = _totalSurveyRepository.GetTotalSurveyById(id);
            survey.Status = TotalSurveyStatus.Completed;
            var currentUser = _authService.GetUserById(survey.DirectorId.Value);
            var author = $"{currentUser.GetFullName()} - General Company";
            survey.BaseEntities_TotalSurveyHistory.Add(new BaseEntities_TotalSurveyHistory
            {
                Author = author,
                Date = DateTime.Now,
                SurveyName = survey.SurveyName,
                SurveyStatus = survey.Status.ToString(),
                TotalSurveyId = survey.Id
            });
            _totalSurveyRepository.AddOrUpdate(survey);
            _totalSurveyRepository.Commit();
            return survey;
        }

        public BaseEntities_TotalSurvey InstantiateSingleSurveys(int id)
        {
            var survey = _totalSurveyRepository.GetTotalSurveyById(id);
            if (survey.BranchCompanyIdList == null) return null;
            var selectedBranchIds = survey.BranchCompanyIdList.Split(',').Select(int.Parse).ToList();
            var allAccountInSelectedBranchDic = _departmentService.GetStaffOfBranchCompanyDic(selectedBranchIds);
            var singleSurveyList = new List<BaseEntities_SingleSurvey>();
            var selectedBranches = _companyService.GetSelectedBranches(selectedBranchIds);
            var currentUser = _authService.GetUserById(survey.DirectorId.Value);
            var author = $"{currentUser.GetFullName()} - General Company";
            using (var scope = new TransactionScope(TransactionScopeOption.Required,
                new TransactionOptions
                {
                    IsolationLevel = IsolationLevel.ReadCommitted
                }))
            {
                foreach (var branch in selectedBranches)
                {
                    var newSingleSurvey = new BaseEntities_SingleSurvey
                    {
                        Status = SingleSurveyStatus.Open,
                        CreatedDate = DateTime.Now,
                        BranchCompanyId = branch.Id,
                        TotalSurveyId = survey.Id,
                        BaseEntities_TotalSurvey = survey

                    };
                    survey.BaseEntities_SingleSurvey.Add(newSingleSurvey);

                }

                // survey.BaseEntities_SingleSurvey = singleSurveyList;
                survey.SingleSurveyQuantity = survey.BaseEntities_SingleSurvey.Count;
                survey.Status = TotalSurveyStatus.Published;
                //                _singleSurveyRepository.AddOrUpdate(singleSurveyList);
                //                _singleSurveyRepository.Commit();
                
                survey.BaseEntities_TotalSurveyHistory.Add(new BaseEntities_TotalSurveyHistory
                {
                    Author = author,
                    Date = DateTime.Now,
                    SurveyName = survey.SurveyName,
                    SurveyStatus = survey.Status.ToString(),
                    TotalSurveyId = survey.Id
                });
                _totalSurveyRepository.AddOrUpdate(survey);
                _totalSurveyRepository.Commit();
                scope.Complete();
            }
            
            foreach (var singleSurvey in survey.BaseEntities_SingleSurvey)
            {
                singleSurvey.BaseEntities_SingleSurveyHistory.Add(new BaseEntities_SingleSurveyHistory
                {
                    Author = author,
                    Date = DateTime.Now,
                    SurveyName = survey.SurveyName,
                    SurveyStatus = singleSurvey.Status.ToString(),
                    SingleSurveyId = singleSurvey.Id
                });
                _singleSurveyRepository.AddOrUpdate(singleSurvey);
            }
            _singleSurveyRepository.Commit();

            InstantiateAccountInSingleSurveys(allAccountInSelectedBranchDic, survey);
            
            return survey;
        }

        public void InstantiateAccountInSingleSurveys(Dictionary<int, List<BaseEntities_AccountOfStaff>> allAccountInSelectedBranchDic, 
            BaseEntities_TotalSurvey survey)
        {
            var singleSurveyList = survey.BaseEntities_SingleSurvey;
            var accountsInSingleSurveyList = new List<BaseEntities_AccountsInSingleSurvey>();
            using (var scope = new TransactionScope(TransactionScopeOption.Required,
                new TransactionOptions
                {
                    IsolationLevel = IsolationLevel.ReadCommitted
                }))
            {
                foreach (var singleSurvey in singleSurveyList)
                {
                    if (allAccountInSelectedBranchDic.ContainsKey(singleSurvey.BranchCompanyId))
                    {
                        foreach (var account in allAccountInSelectedBranchDic[singleSurvey.BranchCompanyId])
                        {
                            var accountInSingleSurvey = new BaseEntities_AccountsInSingleSurvey
                            {
                                SingleSurveyId = singleSurvey.Id,
                                AccountOfStaffId = account.Id
                            };
                            accountsInSingleSurveyList.Add(accountInSingleSurvey);
                        }
                    }
                }
                _accountInSurveyRepository.AddOrUpdate(accountsInSingleSurveyList);
                _accountInSurveyRepository.Commit();
                scope.Complete();
            }
        }

        public void DeleteTotalSurvey(int directorId, int surveyId)
        {
            _totalSurveyRepository.DeleteTotalSurvey(directorId, surveyId);
        }
    }
}
