﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using App.Common.Domain;
using App.Survey.Dto;

namespace App.Survey.Services
{
    public interface ITotalSurveyService
    {
        /// <summary>
        /// Get Total Survey By Director
        /// </summary>
        /// <param name="directorId"></param>
        /// <returns></returns>
        List<BaseEntities_TotalSurvey> GetTotalSurveyByDirector(int directorId);

        /// <summary>
        /// UpdateTotalSurvey
        /// </summary>
        /// <param name="id"></param>
        /// <param name="updatedDto"></param>
        /// <returns></returns>
        BaseEntities_TotalSurvey UpdateTotalSurvey(int id, TotalSurveyDto updatedDto);

        /// <summary>
        /// Create Total Survey
        /// </summary>
        /// <param name="totalSurvey"></param>
        BaseEntities_TotalSurvey CreateTotalSurvey(BaseEntities_TotalSurvey totalSurvey);

        /// <summary>
        /// Instantiate Single Surveys
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        BaseEntities_TotalSurvey InstantiateSingleSurveys(int id);

        /// <summary>
        /// Complete Survey
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        BaseEntities_TotalSurvey CompleteSurvey(int id);

        void DeleteTotalSurvey(int directorId, int surveyId);

        /// <summary>
        /// GetTotalSurveyDetail
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        BaseEntities_TotalSurvey GetTotalSurveyDetail(int id);

        /// <summary>
        /// GetTotalSurveyDetailWithSingleSurvey
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        BaseEntities_TotalSurvey GetTotalSurveyDetailWithSingleSurvey(int id);
    }
}
