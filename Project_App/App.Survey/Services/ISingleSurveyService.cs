﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Common.Domain;
using App.Survey.Dto;

namespace App.Survey.Services
{
    public interface ISingleSurveyService
    {
        /// <summary>
        /// Get Single Survey By Branch Director ID
        /// </summary>
        /// <param name="branchCompanyId"></param>
        /// <returns></returns>
        IList<BaseEntities_SingleSurvey> GetSingleSurveyByBranchCompany(int branchCompanyId);
        IList<BaseEntities_SingleSurvey> GetAllSingleSurveys();
        void DeleteSingleSurvey(int directorId, int surveyId);

        /// <summary>
        /// Get Single Surveys By Total Survey Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        List<BaseEntities_SingleSurvey> GetSingleSurveysByTotalSurveyId(int id);

        /// <summary>
        /// Get Single Survey Detail
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        SingleSurveyAndStaffDto GetSingleSurveyDetail(int id);

        /// <summary>
        /// Correcting Single Survey
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        SingleSurveyAndStaffDto CorrectingSingleSurvey(int id);

        /// <summary>
        /// Submit Single Survey
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        SingleSurveyAndStaffDto SubmitSingleSurvey(int id);

        /// <summary>
        /// Close Single Survey
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        SingleSurveyAndStaffDto CloseSingleSurvey(int id);



        /// <summary>
        /// UpdateSingleSurveyDetail
        /// </summary>
        /// <param name="id"></param>
        /// <param name="singleSurveyDetailVO"></param>
        /// <returns></returns>
        SingleSurveyAndStaffDto UpdateSingleSurveyDetail(int id, SingleSurveyDetailVO singleSurveyDetailVO);
    }
}
