﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using App.Survey.Repositories;
using App.Common.Domain;
using App.Common.Enum;
using App.StaffAccounts.Services;
using App.Survey.Dto;

namespace App.Survey.Services
{
    public class SingleSurveyService : ISingleSurveyService
    {
        private readonly ISingleSurveyRepository _singleSurveyRepository;
        private readonly IStaffAccountService _staffAccountService;
        private readonly IAccountInSurveyRepository _accountInSurveyRepository;

        public SingleSurveyService(ISingleSurveyRepository singleSurveyRepository, IStaffAccountService staffAccountService, 
            IAccountInSurveyRepository accountInSurveyRepository)
        {
            _singleSurveyRepository = singleSurveyRepository;
            _staffAccountService = staffAccountService;
            _accountInSurveyRepository = accountInSurveyRepository;
        }

        public IList<BaseEntities_SingleSurvey> GetSingleSurveyByBranchCompany(int branchCompanyId)
        {
            return (List<BaseEntities_SingleSurvey>) _singleSurveyRepository.GetSingleSurveyByBranchCompany(branchCompanyId);
        }

        public IList<BaseEntities_SingleSurvey> GetAllSingleSurveys()
        {
            return (List<BaseEntities_SingleSurvey>)_singleSurveyRepository.GetAllSingleSurveys();
        }

        public void DeleteSingleSurvey(int directorId, int surveyId)
        {
            _singleSurveyRepository.DeleteSingleSurvey(directorId, surveyId);
        }

        public List<BaseEntities_SingleSurvey> GetSingleSurveysByTotalSurveyId(int id)
        {
            return _singleSurveyRepository.GetSingleSurveysByTotalSurveyId(id);
        }

        public SingleSurveyAndStaffDto GetSingleSurveyDetail(int id)
        {
            var singleSurvey = _singleSurveyRepository.GetSingleSurveyDetail(id);
            var staffAccounts = _staffAccountService.GetActivatedStaffAccountsByIdList(singleSurvey
                .BaseEntities_AccountsInSingleSurvey.Select(a => a.AccountOfStaffId).ToList());
            
            return new SingleSurveyAndStaffDto {singleSurvey = singleSurvey, staffAccountsList = staffAccounts};
        }

        public SingleSurveyAndStaffDto UpdateSingleSurveyDetail(int id, SingleSurveyDetailVO singleSurveyDetailVO)
        {
            var accountInSingleSurveyToUpdate =
                _accountInSurveyRepository.GetAccountInSingleSurveyByIdList(singleSurveyDetailVO.SalaryValues
                    .Select(x => x.SalaryId).ToList());
            var updatedList = new List<BaseEntities_AccountsInSingleSurvey>();
            foreach (var salaryValue in singleSurveyDetailVO.SalaryValues)
            {
                var updatedAccountInSingleSurvey =
                    accountInSingleSurveyToUpdate.FirstOrDefault(a => a.Id == salaryValue.SalaryId);
                if (updatedAccountInSingleSurvey != null)
                {
                    updatedAccountInSingleSurvey.SalaryValue = salaryValue.SalaryValue;
                    updatedList.Add(updatedAccountInSingleSurvey);
                }
            }
            _accountInSurveyRepository.AddOrUpdate(updatedList);
            _accountInSurveyRepository.Commit();


            var singleSurveyToUpdate = _singleSurveyRepository.GetSingleSurveyDetail(id);
            if (singleSurveyToUpdate != null)
            {
                var author = $"{singleSurveyToUpdate.BaseEntities_BranchCompany.BaseEntities_Director.GetFullName()} - {singleSurveyToUpdate.BaseEntities_BranchCompany.City}";
                singleSurveyToUpdate.Status = SingleSurveyStatus.Appending;
                singleSurveyToUpdate.FillingTime = DateTime.Now;
                var appendStatusHistory = singleSurveyToUpdate.BaseEntities_SingleSurveyHistory.FirstOrDefault(h =>
                    h.SingleSurveyId == singleSurveyToUpdate.Id &&
                    h.SingleSurveyStatus == SingleSurveyStatus.Appending);
                if (appendStatusHistory == null)
                {
                    singleSurveyToUpdate.BaseEntities_SingleSurveyHistory.Add(new BaseEntities_SingleSurveyHistory
                    {
                        Author = author,
                        Date = DateTime.Now,
                        SingleSurveyId = singleSurveyToUpdate.Id,
                        SurveyStatus = SingleSurveyStatus.Appending.ToString(),
                        SurveyName = singleSurveyToUpdate.BaseEntities_TotalSurvey.SurveyName
                    });
                }
                else
                {
                    appendStatusHistory.Date = DateTime.Now;
                }
                _singleSurveyRepository.AddOrUpdate(singleSurveyToUpdate);
                _singleSurveyRepository.Commit();
            }
            return GetSingleSurveyDetail(id);
        }

        public SingleSurveyAndStaffDto CorrectingSingleSurvey(int id)
        {
            var singleSurveyToUpdate = _singleSurveyRepository.GetSingleSurveyDetail(id);
            if (singleSurveyToUpdate != null)
            {
                var author = $"{singleSurveyToUpdate.BaseEntities_BranchCompany.BaseEntities_Director.GetFullName()} - {singleSurveyToUpdate.BaseEntities_BranchCompany.City}";
                singleSurveyToUpdate.Status = SingleSurveyStatus.Correcting;
                var correctingStatusHistory = singleSurveyToUpdate.BaseEntities_SingleSurveyHistory.FirstOrDefault(h =>
                    h.SingleSurveyId == singleSurveyToUpdate.Id &&
                    h.SingleSurveyStatus == SingleSurveyStatus.Correcting);
                if (correctingStatusHistory == null)
                {
                    singleSurveyToUpdate.BaseEntities_SingleSurveyHistory.Add(new BaseEntities_SingleSurveyHistory
                    {
                        Author = author,
                        Date = DateTime.Now,
                        SingleSurveyId = singleSurveyToUpdate.Id,
                        SurveyStatus = SingleSurveyStatus.Correcting.ToString(),
                        SurveyName = singleSurveyToUpdate.BaseEntities_TotalSurvey.SurveyName
                    });
                }
                else
                {
                    correctingStatusHistory.Date = DateTime.Now;
                }

                _singleSurveyRepository.AddOrUpdate(singleSurveyToUpdate);
                _singleSurveyRepository.Commit();
            }
            return GetSingleSurveyDetail(id);
        }

        public SingleSurveyAndStaffDto SubmitSingleSurvey(int id)
        {
            var singleSurveyToUpdate = _singleSurveyRepository.GetSingleSurveyDetail(id);
            if (singleSurveyToUpdate != null)
            {
                var author = $"{singleSurveyToUpdate.BaseEntities_BranchCompany.BaseEntities_Director.GetFullName()} - {singleSurveyToUpdate.BaseEntities_BranchCompany.City}";
                singleSurveyToUpdate.Status = SingleSurveyStatus.Submitted;
                var submittedStatusHistory = singleSurveyToUpdate.BaseEntities_SingleSurveyHistory.FirstOrDefault(h =>
                    h.SingleSurveyId == singleSurveyToUpdate.Id &&
                    h.SingleSurveyStatus == SingleSurveyStatus.Correcting);
                if (submittedStatusHistory == null)
                {
                    singleSurveyToUpdate.BaseEntities_SingleSurveyHistory.Add(new BaseEntities_SingleSurveyHistory
                    {
                        Author = author,
                        Date = DateTime.Now,
                        SingleSurveyId = singleSurveyToUpdate.Id,
                        SurveyStatus = SingleSurveyStatus.Submitted.ToString(),
                        SurveyName = singleSurveyToUpdate.BaseEntities_TotalSurvey.SurveyName
                    });
                }
                else
                {
                    submittedStatusHistory.Date = DateTime.Now;
                }

                _singleSurveyRepository.AddOrUpdate(singleSurveyToUpdate);
                _singleSurveyRepository.Commit();
            }
            return GetSingleSurveyDetail(id);
        }

        public SingleSurveyAndStaffDto CloseSingleSurvey(int id)
        {
            var singleSurveyToUpdate = _singleSurveyRepository.GetSingleSurveyDetail(id);
            if (singleSurveyToUpdate != null)
            {
                var author = $"{singleSurveyToUpdate.BaseEntities_BranchCompany.BaseEntities_Director.GetFullName()} - {singleSurveyToUpdate.BaseEntities_BranchCompany.City}";
                singleSurveyToUpdate.Status = SingleSurveyStatus.Closed;
                singleSurveyToUpdate.BaseEntities_SingleSurveyHistory.Add(new BaseEntities_SingleSurveyHistory
                {
                    Author = author,
                    Date = DateTime.Now,
                    SingleSurveyId = singleSurveyToUpdate.Id,
                    SurveyStatus = SingleSurveyStatus.Closed.ToString(),
                    SurveyName = singleSurveyToUpdate.BaseEntities_TotalSurvey.SurveyName
                });

                _singleSurveyRepository.AddOrUpdate(singleSurveyToUpdate);
                _singleSurveyRepository.Commit();
            }
            return GetSingleSurveyDetail(id);
        }
    }
}
