﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.StaffAccounts.Representations;
using WebApi.Hal;

namespace App.Survey.Representations
{
    public class AccountInSurveyRepresentation
    {
        public int Id { get; set; }

        public StaffAccountRepresentation Staff { get; set; }

        public int SingleSurveyId { get; set; }

        public int AccountOfStaffId { get; set; }

        public decimal SalaryValue { get; set; }
    }
}
