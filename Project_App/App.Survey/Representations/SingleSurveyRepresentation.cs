﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApi.Hal;
using App.Common.Domain;
using App.Common.Enum;

namespace App.Survey.Representations
{
    public class SingleSurveyRepresentation
    {
        public int Id { get; set; }

        public string SurveyName { get; set; }
        public SingleSurveyStatus Status { get; set; }
        public DateTime SubmitDeadline { get; set; }
        public int SurveyYear { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime? CloseDate { get; set; }

        public int DirectorId { get; set; }

        public int TotalSurveyId { get; set; }

        public int BranchCompanyId { get; set; }

        public string BranchCompany { get; set; }

        //        public string PreviousSurveyIdList { get; set; }

        //        public Ba Director { get; set; }
        //
        //        public List<SingleSurvey> SingleSurveys { get; set; }

    }
}
