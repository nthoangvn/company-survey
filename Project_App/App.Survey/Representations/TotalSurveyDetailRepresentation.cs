﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Auth.Representations;

namespace App.Survey.Representations
{
    public class TotalSurveyDetailRepresentation : TotalSurveyRepresentations
    {
        public List<TotalSurveyNotes> Notes { get; set; }

        public List<BranchCompanyRepresentation> BranchCompanyList { get; set; }
    }
}
