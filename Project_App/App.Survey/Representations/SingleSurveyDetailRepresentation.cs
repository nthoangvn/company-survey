﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Survey.Representations
{
    public class SingleSurveyDetailRepresentation : SingleSurveyRepresentation
    {
        public DateTime TotalSurveyCreatedDate { get; set; }

        public DateTime PublishDeadline { get; set; }

        public DateTime ValidateDeadline { get; set; }

        public DateTime? FillingTime { get; set; }

        public DateTime? SubmitTime { get; set; }

        public string DirectorName { get; set; }

        public List<AccountInSurveyRepresentation> AccountInSurvey { get; set; }

        public List<SingleSurveyNotes> Notes { get; set; }
    }
}
