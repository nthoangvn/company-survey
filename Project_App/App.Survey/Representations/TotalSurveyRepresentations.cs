﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApi.Hal;
using App.Common.Domain;
using App.Common.Enum;

namespace App.Survey.Representations
{
    public class TotalSurveyRepresentations : Representation
    {
        public int Id { get; set; }

        public TotalSurveyStatus Status { get; set; }

        public string SurveyName { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime? CloseDate { get; set; }

        public int? DirectorId { get; set; }

        public int SurveyYear { get; set; }

        public bool CanCompletedSurvey { get; set; }

        public int SingleSurveyQuantity { get; set; }

        public DateTime PublishDeadline { get; set; }

        public DateTime SubmitDeadline { get; set; }

        public DateTime ValidateDeadline { get; set; }

        public List<int> BranchCompanyIdList { get; set; }
        //        public string PreviousSurveyIdList { get; set; }

        //        public Director Director { get; set; }
        //
        //        public List<SingleSurvey> SingleSurveys { get; set; }
        //
        //        public List<TotalSurvey> PreviousTotalSurveys { get; set; }
    }
}
