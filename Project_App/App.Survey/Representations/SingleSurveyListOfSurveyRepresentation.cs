﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Survey.Representations
{
    public class SingleSurveyListOfSurveyRepresentation : TotalSurveyRepresentations
    {
        public List<SingleSurveyRepresentation> SingleSurveyList { get; set; }
    }
}
