﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Common.Enum;

namespace App.Survey.Representations
{
    public class SingleSurveyNotes
    {
        public int Id { get; set; }
        public SingleSurveyStatus SingleSurveyStatus { get; set; }
        public DateTime Date { get; set; }
        public string Author { get; set; }
        public string SurveyName { get; set; }
    }
}
