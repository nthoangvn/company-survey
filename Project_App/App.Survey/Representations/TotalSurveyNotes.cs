﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Common.Enum;
using WebApi.Hal;

namespace App.Survey.Representations
{
    public class TotalSurveyNotes
    {
        public int Id { get; set; }
        public TotalSurveyStatus TotalSurveyStatus { get; set; }
        public DateTime Date { get; set; }
        public string Author { get; set; }
        public string SurveyName { get; set; }
    }
}
