﻿using App.Common.Controllers;
using App.Common.Domain;
using App.Survey.Representations;
using App.Survey.Services;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using App.Auth.Services;
using App.StaffAccounts.Representations;
using App.Survey.Dto;
using Ninject.Infrastructure.Language;
using System.Net.Http;
using App.Common.Dto;
using App.Common.Helper;

namespace App.Survey.Controllers
{
    [RoutePrefix("api/singlesurvey")]
    public class SingleSurveyController : BaseApiController
    {
        private readonly ISingleSurveyService _singleSurveyService;
        private readonly IAuthService _authService;
        private readonly ICompanyService _companyService;

        public SingleSurveyController(ISingleSurveyService singleSurveyService, IAuthService authService, 
            ICompanyService companyService)
        {
            _singleSurveyService = singleSurveyService;
            _authService = authService;
            _companyService = companyService;
        }

        [HttpGet]
        [Route("{directorId}")]
        public List<SingleSurveyRepresentation> GetSingleSurveysOfDirector(int directorId)
        {
            var director = _authService.GetUserById(directorId);
            var branchCompany = _companyService.GetBranchCompanyByDirectorId(director);
            var singleSurveyList = _singleSurveyService.GetSingleSurveyByBranchCompany(branchCompany.Id).ToList();
            var result = new List<SingleSurveyRepresentation>();
            foreach (var singleSurvey in singleSurveyList)
            {
                var singleSurveyRepr = Mapper.Map<BaseEntities_SingleSurvey, SingleSurveyRepresentation>(singleSurvey);
                singleSurveyRepr.SurveyName = singleSurvey.BaseEntities_TotalSurvey.SurveyName;
                singleSurveyRepr.SurveyYear = singleSurvey.BaseEntities_TotalSurvey.SurveyYear;
                singleSurveyRepr.SubmitDeadline = singleSurvey.BaseEntities_TotalSurvey.SubmitDeadline;
                result.Add(singleSurveyRepr);
            }
            
            return result;
        }

//        [HttpGet]
//        [Route("{directorId}")]
//        public List<SingleSurveyRepresentation> GetSingleSurveysOfBranchCompany(int branchCompanyId)
//        {
//            var singleSurvey = _singleSurveyService.GetSingleSurveyByBranchCompany(branchCompanyId).ToList();
//            var result = Mapper.Map<List<BaseEntities_SingleSurvey>, List<SingleSurveyRepresentation>>(singleSurvey);
//            return result;
//        }

        [HttpGet]
        [Route("{directorId}/info")]
        public IList<BaseEntities_SingleSurvey> GetSingleSurveysOfDirectorInfo(int directorId)
        {
            var temp = _singleSurveyService.GetSingleSurveyByBranchCompany(directorId);
            return temp;
        }

        [HttpDelete]
        [Route("{directorId}/{surveyId}")]
        public IHttpActionResult DeleteSingleSurvey(int directorId, int surveyId)
        {
            _singleSurveyService.DeleteSingleSurvey(directorId, surveyId);
            return Ok();
        }

        [HttpGet]
        [Route("{id}/detail")]
        public SingleSurveyDetailRepresentation GetSingleSurveyDetail(int id)
        {
            var singleSurveyDto = _singleSurveyService.GetSingleSurveyDetail(id);
            var staffAccounts = singleSurveyDto.staffAccountsList;
            var result = ToSingleSurveyDetailRepresentation(singleSurveyDto, staffAccounts);
            return result;
        }



        private SingleSurveyDetailRepresentation ToSingleSurveyDetailRepresentation(SingleSurveyAndStaffDto singleSurveyDto,
            List<BaseEntities_AccountOfStaff> staffAccounts)
        {
            var result = new SingleSurveyDetailRepresentation();
            result = Mapper.Map<BaseEntities_SingleSurvey, SingleSurveyDetailRepresentation>(singleSurveyDto
                .singleSurvey);
            result.AccountInSurvey =
                Mapper.Map<List<BaseEntities_AccountsInSingleSurvey>, List<AccountInSurveyRepresentation>>(
                    singleSurveyDto.singleSurvey.BaseEntities_AccountsInSingleSurvey.ToList());
            var accountsInSingleSurveyRepr = new List<AccountInSurveyRepresentation>();
            foreach (var staff in staffAccounts)
            {
                var accountInSurvey = result.AccountInSurvey.FirstOrDefault(a => a.AccountOfStaffId == staff.Id);
                if (accountInSurvey != null)
                {
                    accountInSurvey.Staff = Mapper.Map<BaseEntities_AccountOfStaff, StaffAccountRepresentation>(staff);
                    accountInSurvey.Staff.DepartmentName = staff.BaseEntities_Department.Name;
                }

                accountsInSingleSurveyRepr.Add(accountInSurvey);
            }

            result.AccountInSurvey.Map(a =>
            {
                a.Staff = accountsInSingleSurveyRepr.FirstOrDefault(x => x.Id == a.Id)?.Staff;
                a.SalaryValue = accountsInSingleSurveyRepr.FirstOrDefault(x => x.Id == a.Id)?.SalaryValue != null
                    ? (decimal) accountsInSingleSurveyRepr.FirstOrDefault(x => x.Id == a.Id)?.SalaryValue
                    : 0;
            });
            result.PublishDeadline = singleSurveyDto.singleSurvey.BaseEntities_TotalSurvey.PublishDeadline;
            result.ValidateDeadline = singleSurveyDto.singleSurvey.BaseEntities_TotalSurvey.ValidateDeadline;
            if (singleSurveyDto.singleSurvey.BaseEntities_BranchCompany.Director_Id != null)
            {
                result.DirectorId = singleSurveyDto.singleSurvey.BaseEntities_BranchCompany.Director_Id.Value;
            }

            result.SurveyYear = singleSurveyDto.singleSurvey.BaseEntities_TotalSurvey.SurveyYear;
            result.SurveyName = singleSurveyDto.singleSurvey.BaseEntities_TotalSurvey.SurveyName;
            result.FillingTime = singleSurveyDto.singleSurvey.FillingTime;
            result.BranchCompanyId = singleSurveyDto.singleSurvey.BaseEntities_BranchCompany.Id;
            result.BranchCompany = singleSurveyDto.singleSurvey.BaseEntities_BranchCompany.GetFullAddress();
            result.DirectorName =
                singleSurveyDto.singleSurvey.BaseEntities_BranchCompany.BaseEntities_Director.GetFullName();
            result.Notes =
                Mapper.Map<List<BaseEntities_SingleSurveyHistory>, List<SingleSurveyNotes>>(singleSurveyDto.singleSurvey
                    .BaseEntities_SingleSurveyHistory);
            return result;
        }

        [HttpPost]
        [Route("{id}/updatesalary")]
        public SingleSurveyDetailRepresentation UpdateSingleSurveyDetail(int id,
            [FromBody] SingleSurveyDetailVO singleSurveyDetailVO)
        {
            var singleSurveyDto = _singleSurveyService.UpdateSingleSurveyDetail(id, singleSurveyDetailVO);
            var staffAccounts = singleSurveyDto.staffAccountsList;
            var result = ToSingleSurveyDetailRepresentation(singleSurveyDto, staffAccounts);
            return result;
        }

        [HttpPost]
        [Route("{id}/correcting")]
        public SingleSurveyDetailRepresentation CorrectingSingleSurvey(int id)
        {
            var singleSurveyDto = _singleSurveyService.CorrectingSingleSurvey(id);
            var staffAccounts = singleSurveyDto.staffAccountsList;
            var result = ToSingleSurveyDetailRepresentation(singleSurveyDto, staffAccounts);
            return result;
        }

        [HttpPost]
        [Route("{id}/submit")]
        public SingleSurveyDetailRepresentation SubmitSingleSurvey(int id)
        {
            var singleSurveyDto = _singleSurveyService.SubmitSingleSurvey(id);
            var staffAccounts = singleSurveyDto.staffAccountsList;
            var result = ToSingleSurveyDetailRepresentation(singleSurveyDto, staffAccounts);
            return result;
        }

        [HttpPost]
        [Route("{id}/close")]
        public SingleSurveyDetailRepresentation CloseSingleSurvey(int id)
        {
            var singleSurveyDto = _singleSurveyService.CloseSingleSurvey(id);
            var staffAccounts = singleSurveyDto.staffAccountsList;
            var result = ToSingleSurveyDetailRepresentation(singleSurveyDto, staffAccounts);
            return result;
        }

        [HttpGet]
        [Route("{id}/generatereport")]
        public HttpResponseMessage GenerateReport(int id, [FromUri] ReportDataVO reportDataVO)
        {
            return GetReport(id, reportDataVO);
        }

        private HttpResponseMessage GetReport(int singleId, ReportDataVO reportDataVO)
        {
            var pdf = ReportHelper.GenerateReport(singleId, reportDataVO);
            return ReportHelper.CreateDownloadPDFResponse(pdf, $"SalaryReport{reportDataVO.fileType}");
        }
    }
}
