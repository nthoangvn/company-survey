﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using App.Survey.Services;
using App.Common.Controllers;
using System.Web.Http;
using System.Web.Http.Results;
using App.Auth.Representations;
using App.Auth.Services;
using App.Survey.Representations;
using App.Common.Domain;
using App.Common.Enum;
using App.Common.Helper;
using App.Survey.Dto;
using Ninject.Infrastructure.Language;

namespace App.Survey.Controllers
{
    [RoutePrefix("api/totalsurvey")]
    public class TotalSurveyController : BaseApiController
    {
        private readonly ITotalSurveyService _totalSurveyService;
        private readonly ICompanyService _companyService;

        public TotalSurveyController(ITotalSurveyService totalSurveyService, ICompanyService companyService)
        {
            _totalSurveyService = totalSurveyService;
            _companyService = companyService;
        }

        [HttpGet]
        [Route("{directorId}")]
        public List<TotalSurveyRepresentations> GetListTotalSurveyByDirector(int directorId)
        {
            var list = _totalSurveyService.GetTotalSurveyByDirector(directorId);
            var result = Mapper.Map<List<BaseEntities_TotalSurvey>, List<TotalSurveyRepresentations>>(list);
            result.Map(surveyRepr =>
            {
                var survey = list.First(x => x.Id == surveyRepr.Id);
                if (survey != null && survey.BaseEntities_SingleSurvey.All(k => k.Status == SingleSurveyStatus.Closed))
                {
                    surveyRepr.CanCompletedSurvey = true;
                }
            });
            return result;
        }

        [HttpPost]
        [Route("create")]
        public TotalSurveyRepresentations CreateTotalSurvey([FromBody] TotalSurveyDto totalSurveyDto)
        {
            var totalSurveyToCreate = Mapper.Map<TotalSurveyDto, BaseEntities_TotalSurvey>(totalSurveyDto);
            totalSurveyToCreate.BranchCompanyIdList = string.Join(",", totalSurveyDto.BranchCompanyIdList);
            var totalSurvey = _totalSurveyService.CreateTotalSurvey(totalSurveyToCreate);
            var result = Mapper.Map<BaseEntities_TotalSurvey, TotalSurveyRepresentations>(totalSurvey);
            return result;
        }

        [HttpPost]
        [Route("{id}/create-singlesurvey")]
        public TotalSurveyRepresentations InstantiateSingleSurvey(int id)
        {
            var totalSurvey = _totalSurveyService.InstantiateSingleSurveys(id);

            var result = Mapper.Map<BaseEntities_TotalSurvey, TotalSurveyRepresentations>(totalSurvey);
            result.BranchCompanyIdList = totalSurvey.BranchCompanyIdList.Split(',').Select(int.Parse).ToList();
            return result;
        }

        [HttpDelete]
        [Route("{directorId}/{surveyId}")]
        public IHttpActionResult DeleteTotalSurvey(int directorId, int surveyId)
        {
            _totalSurveyService.DeleteTotalSurvey(directorId, surveyId);
            return Ok();
        }

        [HttpGet]
        [Route("{id}/detail")]
        public TotalSurveyDetailRepresentation GetTotalSurveyDetail(int id)
        {
            var totalSurvey = _totalSurveyService.GetTotalSurveyDetail(id);
            var selectedBranchIds = totalSurvey.BranchCompanyIdList.Split(',').Select(int.Parse).ToList();
            var companyList = _companyService.GetSelectedBranches(selectedBranchIds);
            var result = Mapper.Map<BaseEntities_TotalSurvey, TotalSurveyDetailRepresentation>(totalSurvey);
            result.BranchCompanyList =
                Mapper.Map<List<BaseEntities_BranchCompany>, List<BranchCompanyRepresentation>>(companyList);
            result.Notes =
                Mapper.Map<List<BaseEntities_TotalSurveyHistory>, List<TotalSurveyNotes>>(totalSurvey
                    .BaseEntities_TotalSurveyHistory);
            return result;
        }

        [HttpGet]
        [Route("{id}/singlesurveys")]
        public SingleSurveyListOfSurveyRepresentation GetSingleSurveyListOfSurvey(int id)
        {
            var totalSurvey = _totalSurveyService.GetTotalSurveyDetailWithSingleSurvey(id);
            var companyList =
                _companyService.GetSelectedBranches(totalSurvey.BaseEntities_SingleSurvey.Select(s => s.BranchCompanyId)
                    .ToList());
            var result = Mapper.Map<BaseEntities_TotalSurvey, SingleSurveyListOfSurveyRepresentation>(totalSurvey);
            result.SingleSurveyList =
                Mapper.Map<List<BaseEntities_SingleSurvey>, List<SingleSurveyRepresentation>>(
                    totalSurvey.BaseEntities_SingleSurvey.ToList());
            result.SingleSurveyList.Map(single =>
            {
                var branch = companyList.FirstOrDefault(b => b.Id == single.BranchCompanyId);
                single.BranchCompany = $"{branch?.City} - {branch?.CountryName}";
            });
            return result;
        }

        [HttpPost]
        [Route("{id}/update")]
        public TotalSurveyRepresentations UpdateTotalSurvey(int id, [FromBody] TotalSurveyDto totalSurveyDto)
        {
            var totalSurvey = _totalSurveyService.UpdateTotalSurvey(id, totalSurveyDto);
            var result = Mapper.Map<BaseEntities_TotalSurvey, TotalSurveyRepresentations>(totalSurvey);
            return result;
        }

        [HttpPost]
        [Route("{id}/complete")]
        public TotalSurveyRepresentations CompleteSurvey(int id)
        {
            var totalSurvey = _totalSurveyService.CompleteSurvey(id);
            var result = Mapper.Map<BaseEntities_TotalSurvey, TotalSurveyRepresentations>(totalSurvey);
            result.BranchCompanyIdList = totalSurvey.BranchCompanyIdList.Split(',').Select(int.Parse).ToList();
            return result;
        }


    }
}