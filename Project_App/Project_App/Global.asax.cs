﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using App.Common.Domain;
using App.Common.Migrations;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.SessionState;
using App.Common.Factory;
using log4net.Config;
using WebApi.Hal;
using Configuration = App.Common.Migrations.Configuration;
namespace Project_App
{
    public class WebApiApplication : HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
            XmlConfigurator.Configure();
            // AreaRegistration.RegisterAllAreas();
            // GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            //            GlobalConfiguration.Configure(WebApiConfig.Register);
            //            AreaRegistration.RegisterAllAreas();
            //            
            //            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            //            RouteConfig.RegisterRoutes(RouteTable.Routes);
            GlobalConfiguration.Configuration.EnsureInitialized();
            GlobalConfiguration.Configuration.Formatters.Add(new JsonHalMediaTypeFormatter());
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<CompanyContext, Configuration>());
        }

        protected void Application_PostAuthorizeRequest()
        {
            if (IsWebApiRequest())
            {
                HttpContext.Current.SetSessionStateBehavior(SessionStateBehavior.Required);
            }
        }

        private bool IsWebApiRequest()
        {
            return HttpContext.Current.Request.AppRelativeCurrentExecutionFilePath != null
                   && HttpContext.Current.Request.AppRelativeCurrentExecutionFilePath.StartsWith(WebApiConfig.UrlPrefixRelative, StringComparison.Ordinal);
        }

        void Application_EndRequest(object sender, EventArgs e)
        {
            DbContextFactory.Close();
        }
    }
}
