﻿using App.Auth.Repositories;
using App.Auth.Services;
using App.StaffAccounts.Repositories;
using App.StaffAccounts.Services;
using App.Survey.Repositories;
using App.Survey.Services;
using AutoMapper;

using Ninject.Extensions.Interception.Infrastructure.Language;
using Ninject.Modules;

namespace Project_App.Register
{
    public class StaffAccountModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IStaffAccountService>().To<StaffAccountService>();
            Bind<IStaffAccountRepository>().To<StaffAccountsRepository>();
        }
    }
}