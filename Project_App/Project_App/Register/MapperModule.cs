﻿using App.Auth.Repositories;
using App.Auth.Services;
using App.StaffAccounts.Repositories;
using App.StaffAccounts.Services;
using App.Survey.Repositories;
using App.Survey.Services;
using AutoMapper;
using Ninject.Modules;

namespace Project_App.Register
{
    public class MapperModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IMapper>().ToConstant(ConfigMapper.Mapper).InSingletonScope();
            Bind<IConfigurationProvider>().ToConstant(ConfigMapper.MapperConfiguration).InSingletonScope();
            Bind<IAuthService>().To<AuthService>().InSingletonScope();
            Bind<IDirectorRepository>().To<DirectorRepository>().InSingletonScope();
            Bind<IAuthRepository>().To<AuthRepository>().InSingletonScope();
            Bind<ICompanyRepository>().To<CompanyRepository>().InSingletonScope();
            Bind<ICompanyService>().To<CompanyService>().InSingletonScope();
            Bind<IDepartmentService>().To<DepartmentService>().InSingletonScope();
            Bind<IDepartmentRepository>().To<DepartmentRepository>().InSingletonScope();
            Bind<ISingleSurveyRepository>().To<SingleSurveyRepository>().InSingletonScope();
            Bind<ISingleSurveyService>().To<SingleSurveyService>().InSingletonScope();
            Bind<ITotalSurveyRepository>().To<TotalSurveyRepository>().InSingletonScope();
            Bind<ITotalSurveyService>().To<TotalSurveyService>().InSingletonScope();
            Bind<IAccountInSurveyRepository>().To<AccountInSurveyRepository>().InSingletonScope();

        }
    }
}