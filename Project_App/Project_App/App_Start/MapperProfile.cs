﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using App.Auth.Representations;
using App.Common.Domain;
using App.StaffAccounts.Representations;
using App.Survey.Dto;
using App.Survey.Representations;
using AutoMapper;

namespace Project_App
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<BaseEntities_SingleSurvey, SingleSurveyRepresentation>().ReverseMap();
            CreateMap<BaseEntities_TotalSurvey, TotalSurveyRepresentations>().ReverseMap();
            CreateMap<BaseEntities_AccountOfStaff, StaffAccountRepresentation>().ReverseMap();
            CreateMap<BaseEntities_Director, UserRepresentation>().ReverseMap();
            CreateMap<BaseEntities_BranchCompany, BranchCompanyRepresentation>().ReverseMap();
            CreateMap<BaseEntities_Department, DepartmentRepresentation>().ReverseMap();
            CreateMap<TotalSurveyDto, BaseEntities_TotalSurvey>().ReverseMap();
            CreateMap<TotalSurveyDto, TotalSurveyRepresentations>().ReverseMap();
            CreateMap<BaseEntities_SingleSurvey, SingleSurveyDetailRepresentation>().ReverseMap();
            CreateMap<BaseEntities_AccountsInSingleSurvey, AccountInSurveyRepresentation>().ReverseMap();
            CreateMap<BaseEntities_TotalSurveyHistory, TotalSurveyNotes>().ReverseMap();
            CreateMap<BaseEntities_SingleSurveyHistory, SingleSurveyNotes>().ReverseMap();
            CreateMap<BaseEntities_TotalSurvey, TotalSurveyDetailRepresentation>().ReverseMap();
            CreateMap<CreateStaffAccountDto, BaseEntities_AccountOfStaff>().ReverseMap();
            CreateMap<UpdateStaffAccountDto, BaseEntities_AccountOfStaff>().ReverseMap();
            CreateMap<BaseEntities_TotalSurvey, SingleSurveyListOfSurveyRepresentation>().ReverseMap();
        }
    }
}
