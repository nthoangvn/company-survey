﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;

namespace Project_App
{
    public static class ConfigMapper
    {
        static ConfigMapper()
        {
            CreatingMapping();
        }

        public static IMapper Mapper { get; set; }

        public static IConfigurationProvider MapperConfiguration { get; set; }

        /// <summary>
        /// Configure mapping, call once when creating application kernel
        /// </summary>
        public static void CreatingMapping()
        {
//            AutoMapper.Mapper.Initialize(cfg =>
//            {
//                cfg.AddProfile<MapperProfile>();
//            });
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<MapperProfile>();
            });
            MapperConfiguration = config;
            Mapper = config.CreateMapper();
        }
    }
}