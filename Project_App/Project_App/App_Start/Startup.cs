﻿
using App.Common.Constants;
using Microsoft.Owin;
using Ninject;
using Ninject.Syntax;
using Owin;
using Project_App;

[assembly: OwinStartup(typeof(Startup))]

namespace Project_App
{
    public class Startup
    {
        public void Configuration(IAppBuilder app) {
            var kernel = NinjectWebCommon.CreateKernelNonHttpModule();
        }
    }
}
