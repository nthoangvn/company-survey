﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Cors;
using System.Web.Http.Dependencies;
using System.Web.Http.Routing;
using App.Auth.Repositories;
using App.Auth.Services;
using App.Common;
using App.Common.Helper;
using App.StaffAccounts.Repositories;
using App.StaffAccounts.Services;
using App.Survey.Repositories;
using App.Survey.Services;
using AutoMapper;
using Unity;
using Unity.Exceptions;
using Unity.Lifetime;

namespace Project_App
{
    public static class WebApiConfig
    {
        public static string UrlPrefix => "api";
        public static string UrlPrefixRelative => "~/api";

        public static void Register(HttpConfiguration config)
        {
            // Global Exception handling
            config.Services.Replace(typeof(IHttpActionInvoker), new ApiControllerActionInvoker());

            // Config format date time json is UTC
            var settings = AppSettings.CreateJsonSerializerSettings();
            GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings = settings;
            var cors = new EnableCorsAttribute("http://localhost:9000", "*", "*");
            config.EnableCors(cors);
            // Web API configuration and services
            // Use Ninject (in NinjectWebCommon.cs & MapperModule.cs) for DI instead of Unity
            //            var container = new UnityContainer();
            //            container.RegisterType<ISingleSurveyService, SingleSurveyService>(new HierarchicalLifetimeManager());
            //            container.RegisterType<ISingleSurveyRepository, SingleSurveyRepository>(new HierarchicalLifetimeManager());
            //            container.RegisterType<IStaffAccountService, StaffAccountService>(new HierarchicalLifetimeManager());
            //            container.RegisterType<IStaffAccountRepository, StaffAccountsRepository>(new HierarchicalLifetimeManager());
            //            container.RegisterType<IAuthService, AuthService>(new HierarchicalLifetimeManager());
            //            container.RegisterType<IAuthRepository, AuthRepository>(new HierarchicalLifetimeManager());
            //            config.DependencyResolver = new UnityResolver(container);


            // Web API routes
            config.MapHttpAttributeRoutes(new CustomDirectRouteProvider());

            config.Routes.MapHttpRoute(
                name: "ActionApi",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional },
                constraints: new { id = @"\d+" }
            );

            // FINMAEHP-1809: remove resource name when a 404 error occurs
            config.MessageHandlers.Add(new Remove404ResourceNameHandler());
        }
    }

    public class CustomDirectRouteProvider : DefaultDirectRouteProvider
    {
        protected override IReadOnlyList<IDirectRouteFactory> GetActionRouteFactories(
            HttpActionDescriptor actionDescriptor)
        {
            return actionDescriptor.GetCustomAttributes<IDirectRouteFactory>(true);
        }
    }

    public class Remove404ResourceNameHandler : DelegatingHandler
    {
        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request,
            CancellationToken cancellationToken)
        {

            var response = await base.SendAsync(request, cancellationToken);

            return RemoveResourceName(response);
        }

        private HttpResponseMessage RemoveResourceName(HttpResponseMessage response)
        {
            if (response.StatusCode == HttpStatusCode.NotFound)
            {
                response.Content = new StringContent("Resource not found");
            }

            return response;
        }
    }

    public class UnityResolver : IDependencyResolver
    {
        protected IUnityContainer container;

        public UnityResolver(IUnityContainer container)
        {
            if (container == null)
            {
                throw new ArgumentNullException("container");
            }
            this.container = container;
        }

        public object GetService(Type serviceType)
        {
            try
            {
                return container.Resolve(serviceType);
            }
            catch (ResolutionFailedException)
            {
                return null;
            }
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            try
            {
                return container.ResolveAll(serviceType);
            }
            catch (ResolutionFailedException)
            {
                return new List<object>();
            }
        }

        public IDependencyScope BeginScope()
        {
            var child = container.CreateChildContainer();
            return new UnityResolver(child);
        }

        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            container.Dispose();
        }
    }
}
