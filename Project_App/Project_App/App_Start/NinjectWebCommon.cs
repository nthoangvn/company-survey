using System;
using System.Linq;
using System.Web;
using App.Auth.Repositories;
using App.Auth.Services;
using App.StaffAccounts.Repositories;
using App.StaffAccounts.Services;
using App.Survey.Repositories;
using App.Survey.Services;
using Microsoft.Web.Infrastructure.DynamicModuleHelper;
using Ninject;
using Ninject.Web.Common;
using Ninject.Web.Common.WebHost;
using Ninject.Web.WebApi;
using Project_App;
using Project_App.Register;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(NinjectWebCommon), "Stop")]

namespace Project_App
{
    public static class NinjectWebCommon
    {
        private static readonly Bootstrapper _bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start()
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            _bootstrapper.Initialize(CreateKernel);
        }

        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            _bootstrapper.ShutDown();
        }

        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        public static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Creates the kernel that will manage your hangfire jobs.
        /// </summary>
        /// <returns></returns>
        public static IKernel CreateKernelNonHttpModule()
        {
            var kernel = new StandardKernel();
            try
            {
                RegisterServices(kernel, false);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel, bool isWebContext = true)
        {
            if (isWebContext)
            {
                System.Web.Http.GlobalConfiguration.Configuration.DependencyResolver = new NinjectDependencyResolver(kernel);
            }
            // Go through all EHP* modules and register
            var x = AppDomain.CurrentDomain.GetAssemblies().Where(i => i.GetName().Name.Contains("App")
                                                                       && !i.GetName().Name.Contains("EntityFrameworkDynamicProxies")).ToList();
            x.ForEach(i =>
                {
                    kernel.Load(i);
                });
        }
    }
}
