import { CreateStaffComponent } from './staff/create-staff/create-staff.component';
import { AuthService } from './commons/authentications/auth.service';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule, CookieXSRFStrategy, XSRFStrategy } from '@angular/http';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { LoginComponent } from './commons/authentications/login/login.component';
import { BackendHttpService } from './commons/services/http.service';
import { ToastProvider } from './commons/services/toast.provider';
import { ProcessingService } from './commons/services/processing.service';
import { NotFoundComponent } from './commons/notFound/notFound.component';
import { HomeComponent } from './home/home.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-modialog';
import { BootstrapModalModule } from 'ngx-modialog/plugins/bootstrap';
import { ToastrModule } from 'ngx-toastr';
import {NgBusyModule} from 'ng-busy';
import { FormFieldComponent } from './commons/shared/form/form-field/form-field.component';
import { AbstractFormComponent } from './commons/shared/form/form.component';
import { BaseFormComponent } from './commons/shared/form/baseForm.component';
import { CardModule } from 'primeng/card';
import { SingleSurveysComponent } from './single-surveys/single-surveys.component';
import { TotalSurveysComponent } from './total-surveys/total-surveys.component';
import { UsersComponent } from './users/users.component';
import { UserDetailComponent } from './user-detail/user-detail.component';
import { InMemoryDataService } from './in-memory-data.service';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { NavigationComponent } from './commons/navigation/navigation.component';
import { FooterComponent } from './commons/footer/footer.component';
import { SubMenuComponent } from './commons/sub-menu/sub-menu.component';
import { AlertModalComponent } from './commons/modals/alert-modal/alert-modal.component';
import { AuthGuard } from './commons/services/AuthGuard.service';
import { AlertService } from './commons/services/alert.service';
import {MatStepperModule} from '@angular/material/stepper';
import { CreateASurveyComponent } from './create-survey/create-a-survey/create-a-survey.component';
import {MatFormFieldModule, MatInputModule, MatFormFieldControl, MatSelectModule} from '@angular/material';
import {MatIconModule} from '@angular/material/icon';
import { TableModule } from 'primeng/table';
import {PickListModule} from 'primeng/picklist';
import { ButtonModule } from 'primeng/button';
import { DataViewModule } from 'primeng/dataview';
import { DataListModule, DialogModule, ConfirmDialogModule, MessagesModule, MessageModule, ConfirmationService } from 'primeng/primeng';
import {CalendarModule} from 'primeng/calendar';
import { ToastModule } from 'primeng/toast';
import { SingleSurveyDetailComponent } from './single-survey-detail/single-survey-detail.component';
import { BranchStaffsComponent } from './staff/branch-staffs/branch-staffs.component';
import { SurveyStatusNotificationComponent } from './survey-status-notification/survey-status-notification.component';
import { TotalSurveyDetailComponent } from './total-surveys/total-survey-detail/total-survey-detail.component';
import { TotalSurveyPropertiesComponent } from './total-surveys/total-survey-properties/total-survey-properties.component';
import { OnlyNumberDirective } from './commons/directives/onlynumber.directive';
import {DropdownModule} from 'primeng/dropdown';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { SingleSurveyNotificationComponent } from './single-survey-notification/single-survey-notification.component';
import { SinglesurveyListOfSurveyComponent } from './total-surveys/singlesurvey-list-of-survey/singlesurvey-list-of-survey.component';
import { EditStaffComponent } from './staff/edit-staff/edit-staff.component';
import {ToggleButtonModule} from 'primeng/togglebutton';

export function xsrfFactory() {
  return new CookieXSRFStrategy('XSRF-TOKEN', 'X-XSRF-TOKEN');
}

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    NavigationComponent,
    SubMenuComponent,
    FooterComponent,
    NotFoundComponent,
    HomeComponent,
    OnlyNumberDirective,
    FormFieldComponent,
    SingleSurveysComponent,
    TotalSurveysComponent,
    UsersComponent,
    UserDetailComponent,
    AlertModalComponent,
    CreateASurveyComponent,
    SingleSurveyDetailComponent,
    BranchStaffsComponent,
    SurveyStatusNotificationComponent,
    TotalSurveyDetailComponent,
    TotalSurveyPropertiesComponent,
    SingleSurveyNotificationComponent,
    CreateStaffComponent,
    SinglesurveyListOfSurveyComponent,
    EditStaffComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule.forRoot(),
    HttpClientModule,
    HttpModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    NgBusyModule,
    ModalModule.forRoot(),
    BootstrapModalModule,
    ToastrModule.forRoot(),
    CardModule,
    MatStepperModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    TableModule,
    ButtonModule,
    DataViewModule,
    DataListModule,
    PickListModule,
    MatSelectModule,
    DialogModule,
    ConfirmDialogModule,
    MessagesModule,
    MessageModule,
    CalendarModule,
    ToggleButtonModule,
    ToastModule,
    DropdownModule,
    // The HttpClientInMemoryWebApiModule module intercepts HTTP requests
    // and returns simulated server responses.
    // Remove it when a real server is ready to receive requests.
    HttpClientInMemoryWebApiModule.forRoot(
      InMemoryDataService, { dataEncapsulation: false }
    ),
    NgbModule.forRoot()
  ],
  providers: [
    ToastProvider,
    AlertService,
    ProcessingService,
    AuthGuard,
    BackendHttpService,
    AuthService,
    ConfirmationService,
    { provide: XSRFStrategy, useFactory: xsrfFactory },
  ],
  entryComponents: [
    AlertModalComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
