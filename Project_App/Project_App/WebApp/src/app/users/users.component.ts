import { Component, OnInit } from '@angular/core';
import { USERS } from './mock-users';
import { User } from '../commons/user/user';
import { UserService } from '../user.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  users: User[];

  constructor(private userService: UserService) { }

  ngOnInit() {
    console.log('Users component on init');
    this.getUsers();
  }

  getUsers(): void {
    this.userService.getUsers().then(
      res => { // Success
        console.log('Get Users response, size: ', res);
        this.users = res;
        console.log('Get Users mapping: ', this.users);
        }
    );
  }
}
