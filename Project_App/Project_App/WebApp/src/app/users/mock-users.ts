import { User } from "../commons/user/user";

export const USERS: User[] = [
    { Id: 100, FirstName: 'Hoang', LastName: 'Nguyen', StaffVisa: '123455', Phone: '0987695113', Activated: true},
    { Id: 101, FirstName: 'Nhan', LastName: 'Nguyen', StaffVisa: '123456', Phone: '0987695114', Activated: true},
    { Id: 102, FirstName: 'David', LastName: 'Luis', StaffVisa: '123457', Phone: '0987695115', Activated: false}
  ];