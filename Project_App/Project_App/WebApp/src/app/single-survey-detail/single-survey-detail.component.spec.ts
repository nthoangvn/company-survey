import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleSurveyDetailComponent } from './single-survey-detail.component';

describe('SingleSurveyDetailComponent', () => {
  let component: SingleSurveyDetailComponent;
  let fixture: ComponentFixture<SingleSurveyDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SingleSurveyDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleSurveyDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
