import { DirectorRole } from './../commons/objects/director';
import { SalaryValueVO, SingleSurveyDetailVO } from './../commons/objects/single-survey-detail-VO';
import { SingleSurveysService } from './../single-surveys/single-surveys.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { AuthService } from '../commons/authentications/auth.service';
import { ToastProvider } from '../commons/services/toast.provider';
import { TotalSurveysService } from '../total-surveys/total-surveys.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Modal } from 'ngx-modialog';
import { differenceWith, isEqual } from 'lodash';
import { SingleSurveyDetail, SingleSurveyNotes } from '../commons/surveys/single-survey';
import { AccountInSurvey } from '../commons/surveys/account-in-single-survey';
import { SingleSurveyStatus } from '../commons/surveys/single-survey-status';
import { Utils } from '../commons/helper/utils';
import { ReportDataVO } from '../commons/objects/report-data-VO';
import * as cloneDeep from 'lodash.clonedeep';

@Component({
  selector: 'app-single-survey-detail',
  templateUrl: './single-survey-detail.component.html',
  styleUrls: ['./single-survey-detail.component.css']
})
export class SingleSurveyDetailComponent implements OnInit {
  busy: any;
  isBranchDirector: boolean;
  singleSurveyId: number;
  data: SingleSurveyDetail = <SingleSurveyDetail>{};
  accountInSurveyData: AccountInSurvey[] = [];
  cols: any[];
  notes: SingleSurveyNotes[];
  reportData: ReportDataVO[] = [
    <ReportDataVO>{ fileType: '.pdf', format: 'PDF'},
    <ReportDataVO>{ fileType: '.xlsx', format: 'Excel'}
  ];
  selectedReportData: ReportDataVO = <ReportDataVO>{ fileType: '.pdf', format: 'PDF'};
  rowGroupMetadata: any;
  status = {
    Open: SingleSurveyStatus.Open,
    Appending: SingleSurveyStatus.Appending,
    Submitted: SingleSurveyStatus.Submitted,
    Correcting: SingleSurveyStatus.Correcting,
    Closed: SingleSurveyStatus.Closed
  };
  salaryVOList: SalaryValueVO[] = [];
  selectedRow: any;
  singleSurveyDetailVO: SingleSurveyDetailVO = <SingleSurveyDetailVO>{};
  SingleSurveyStatus: typeof SingleSurveyStatus = SingleSurveyStatus;
  constructor(private _formBuilder: FormBuilder, protected userService: AuthService,
    protected toastProvider: ToastProvider, public totalSurveysService: TotalSurveysService,
    public singleSurveyService: SingleSurveysService,
    public router: Router, private route: ActivatedRoute, public modal: Modal) {}

  ngOnInit() {
    this.isBranchDirector = this.userService.loggedInUser.Role === DirectorRole.BranchDirector;
    this.cols = [
      { field: 'FirstName', header: 'First Name'},
      { field: 'LastName', header: 'Last Name'},
      { field: 'StaffVisa', header: 'Visa' },
      { field: 'StartDate', header: 'Start Date' },
      { field: 'Position', header: 'Role'},
      { field: 'SalaryValue', header: 'Salary Value'}
    ];
    this.route.params.subscribe( param => {
      this.singleSurveyId = +param['id'];
      this.busy = this.singleSurveyService.getSingleSurveyDetail(this.singleSurveyId).then(response => {
        this.data = response;
        this.accountInSurveyData = this.data.AccountInSurvey;
        this.data.TotalSurveyCreatedDate = new Date(this.data.TotalSurveyCreatedDate);
        this.data.PublishDeadline = new Date(this.data.PublishDeadline);
        this.data.SubmitDeadline = new Date(this.data.SubmitDeadline);
        this.data.ValidateDeadline = new Date(this.data.ValidateDeadline);
        this.data.CreatedDate = new Date(this.data.CreatedDate);
        this.notes = this.data.Notes;
        this.updateRowGroupMetaData();
        // this.notes = this.data.Notes;
      });
    });
  }

  get disableSubmit() {
    return this.accountInSurveyData && this.accountInSurveyData.some(x => x.SalaryValue === 0 || x.SalaryValue.toString() === '');
  }

  save() {
    this.salaryVOList = [];
    const edittedAccount = this.accountInSurveyData.filter(x => x.SalaryValue !== 0);
    edittedAccount.forEach(a => {
      const salaryVO = new SalaryValueVO();
      salaryVO.SalaryId = a.Id;
      salaryVO.SalaryValue = a.SalaryValue;
      this.salaryVOList.push(salaryVO);
    });
    this.singleSurveyDetailVO = <SingleSurveyDetailVO>{
      Id: this.singleSurveyId,
      SalaryValues: this.salaryVOList
    };
    this.busy = this.singleSurveyService.updateSingleSurveyDetail(this.singleSurveyId, this.singleSurveyDetailVO).subscribe(
      (response) => {
        this.data = response;
        this.accountInSurveyData = this.data.AccountInSurvey;
        this.data.TotalSurveyCreatedDate = new Date(this.data.TotalSurveyCreatedDate);
        this.data.PublishDeadline = new Date(this.data.PublishDeadline);
        this.data.SubmitDeadline = new Date(this.data.SubmitDeadline);
        this.data.ValidateDeadline = new Date(this.data.ValidateDeadline);
        this.data.CreatedDate = new Date(this.data.CreatedDate);
        this.notes = this.data.Notes;
        this.updateRowGroupMetaData();
        this.toastProvider.showSuccessToast('Saved Single Survey Successfully!');
      });
  }

  onCorrecting() {
    this.busy = this.singleSurveyService.correctingSingleSurvey(this.singleSurveyId).subscribe(
      (response) => {
        this.data = response;
        this.accountInSurveyData = this.data.AccountInSurvey;
        this.data.TotalSurveyCreatedDate = new Date(this.data.TotalSurveyCreatedDate);
        this.data.PublishDeadline = new Date(this.data.PublishDeadline);
        this.data.SubmitDeadline = new Date(this.data.SubmitDeadline);
        this.data.ValidateDeadline = new Date(this.data.ValidateDeadline);
        this.data.CreatedDate = new Date(this.data.CreatedDate);
        this.notes = this.data.Notes;
        this.updateRowGroupMetaData();
        this.onBackClick();
        this.toastProvider.showSuccessToast('Change status of Single Survey to Correcting Successfully!');
      });
  }

  onClose() {
    this.busy = this.singleSurveyService.closeSingleSurvey(this.singleSurveyId).subscribe(
      (response) => {
        this.data = response;
        this.accountInSurveyData = this.data.AccountInSurvey;
        this.data.TotalSurveyCreatedDate = new Date(this.data.TotalSurveyCreatedDate);
        this.data.PublishDeadline = new Date(this.data.PublishDeadline);
        this.data.SubmitDeadline = new Date(this.data.SubmitDeadline);
        this.data.ValidateDeadline = new Date(this.data.ValidateDeadline);
        this.data.CreatedDate = new Date(this.data.CreatedDate);
        this.notes = this.data.Notes;
        this.updateRowGroupMetaData();
        this.onBackClick();
        this.toastProvider.showSuccessToast('Change status of Single Survey to Close Successfully!');
      });
  }

  submit() {
    this.busy = this.singleSurveyService.submitSingleSurvey(this.singleSurveyId).subscribe(
      (response) => {
        this.data = response;
        this.accountInSurveyData = this.data.AccountInSurvey;
        this.data.TotalSurveyCreatedDate = new Date(this.data.TotalSurveyCreatedDate);
        this.data.PublishDeadline = new Date(this.data.PublishDeadline);
        this.data.SubmitDeadline = new Date(this.data.SubmitDeadline);
        this.data.ValidateDeadline = new Date(this.data.ValidateDeadline);
        this.data.CreatedDate = new Date(this.data.CreatedDate);
        this.notes = this.data.Notes;
        this.updateRowGroupMetaData();
        this.toastProvider.showSuccessToast('Change status of Single Survey to Submitted Successfully!');
      });
  }

  generateReport() {
    this.singleSurveyService.generateReport(this.singleSurveyId, this.selectedReportData);
  }

  onSort() {
    this.updateRowGroupMetaData();
  }

  updateRowGroupMetaData() {
    this.rowGroupMetadata = {};
    if (this.accountInSurveyData) {
        for (let i = 0; i < this.accountInSurveyData.length; i++) {
            const rowData = this.accountInSurveyData[i];
            const departMent = rowData.Staff.DepartmentName;
            if (i === 0) {
                this.rowGroupMetadata[departMent] = { index: 0, size: 1 };
            } else {
                const previousRowData = this.accountInSurveyData[i - 1];
                const previousDepartMent = previousRowData.Staff.DepartmentName;
                if (departMent === previousDepartMent) {
                  this.rowGroupMetadata[departMent].size++;
                } else {
                  this.rowGroupMetadata[departMent] = { index: i, size: 1 };
                }
            }
        }
    }
  }

  onBackClick() {
    this.router.navigate([`../../../single-survey-list`], { relativeTo: this.route });
  }
}
