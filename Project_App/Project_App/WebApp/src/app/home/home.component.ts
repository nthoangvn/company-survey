import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { from } from 'rxjs';
import { DirectorRole } from '../commons/objects/director';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  isGeneralDirector;

  constructor() {
  }

  ngOnInit() {
    var user = JSON.parse(localStorage.getItem('currentUser'));
    this.isGeneralDirector = (user.Role === DirectorRole.GeneralDirector);
  }

}
