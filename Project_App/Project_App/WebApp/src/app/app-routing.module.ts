import { CreateStaffComponent } from './staff/create-staff/create-staff.component';
import { TotalSurveyPropertiesComponent } from './total-surveys/total-survey-properties/total-survey-properties.component';
import { SingleSurveyDetailComponent } from './single-survey-detail/single-survey-detail.component';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { NotFoundComponent } from './commons/notFound/notFound.component';
import { LoginComponent } from './commons/authentications/login/login.component';
import { HomeComponent } from './home/home.component';
import { UsersComponent } from './users/users.component';
import { UserDetailComponent } from './user-detail/user-detail.component';
import { AuthGuard } from './commons/services/AuthGuard.service';
import { DirectorRole } from './commons/objects/director';
import { TotalSurveysComponent } from './total-surveys/total-surveys.component';
import { SingleSurveysComponent } from './single-surveys/single-surveys.component';
import { CreateASurveyComponent } from './create-survey/create-a-survey/create-a-survey.component';
import { BranchStaffsComponent } from './staff/branch-staffs/branch-staffs.component';
import { TotalSurveyDetailComponent } from './total-surveys/total-survey-detail/total-survey-detail.component';
import { SinglesurveyListOfSurveyComponent } from './total-surveys/singlesurvey-list-of-survey/singlesurvey-list-of-survey.component';
import { EditStaffComponent } from './staff/edit-staff/edit-staff.component';

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'error-page', component: NotFoundComponent },
  {
    path: 'total-survey-list',
    component: TotalSurveysComponent,
    // canActivate: [AuthGuard],
    // data: {
    //   allowedTypes: [DirectorRole.GeneralDirector]
    // }
  },
  {
    path: 'single-survey-list',
    component: SingleSurveysComponent,
    // canActivate: [AuthGuard],
    // data: {
    //   allowedTypes: [DirectorRole.BranchDirector]
    // }
  },
  { path: 'total-survey-list/create-survey', component: CreateASurveyComponent },
  // { path: 'home', component: HomeComponent, canActivate: [AuthGuard] },
  { path: 'home', component: HomeComponent},
  { path: 'users', component: UsersComponent },
  { path: 'user/:id', component: UserDetailComponent },
  { path: 'single-survey', component: SingleSurveysComponent },
  { path: 'single-survey/:id/detail', component: SingleSurveyDetailComponent },
  { path: 'total-survey/:totalId/single-survey/:id/detail', component: SingleSurveyDetailComponent },
  { path: 'total-survey', component: TotalSurveysComponent },
  { path: 'total-survey/:id/detail', component: TotalSurveyDetailComponent },
  { path: 'total-survey/:id/single-survey-list', component: SinglesurveyListOfSurveyComponent },
  { path: 'total-survey/:id/properties', component: TotalSurveyPropertiesComponent },
  { path: 'branch-staffs/:branchId', component: BranchStaffsComponent },
  { path: 'branch-staffs/:branchId/add', component: CreateStaffComponent },
  { path: 'branch-staffs/:branchId/edit/:staffAccountId', component: EditStaffComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})

export class AppRoutingModule {
  static forRoot(): ModuleWithProviders {
    return {
        ngModule: AppRoutingModule,
        providers: []
    };
  }
 }
