import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TotalSurveysListComponent } from './total-surveys-list.component';

describe('TotalSurveysListComponent', () => {
  let component: TotalSurveysListComponent;
  let fixture: ComponentFixture<TotalSurveysListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TotalSurveysListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TotalSurveysListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
