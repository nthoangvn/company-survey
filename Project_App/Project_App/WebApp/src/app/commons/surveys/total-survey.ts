import { TotalSurveyStatus } from './total-survey-status';
import { BranchCompany } from '../objects/branch-company';
export interface TotalSurvey {
    Id: number;
    Status: TotalSurveyStatus;
    SurveyName: string;
    CreatedDate: Date;
    CloseDate: Date;
    DirectorId?: number;
    BranchCompanyIdList: number[];
    PublishDeadline: Date;
    SubmitDeadline: Date;
    ValidateDeadline: Date;
    SurveyYear: number;
    SingleSurveyQuantity: number;
    CanCompletedSurvey: boolean;
}

export interface TotalSurveyDetail extends TotalSurvey {
  Notes: TotalSurveyNotes[];
  BranchCompanyList: BranchCompany[];
}

export class TotalSurveyNotes {
  Id: number;
  TotalSurveyStatus: TotalSurveyStatus;
  Date: Date;
  Author: string;
  SurveyName: string;
}
