export class TotalSurveyDto {

  SurveyName: string = undefined;
  CreatedDate: Date = undefined;
  CloseDate: Date = undefined;
  DirectorId?: number = undefined;
  BranchCompanyIdList: number[] = undefined;
  PublishDeadline: Date = undefined;
  SubmitDeadline: Date = undefined;
  ValidateDeadline: Date = undefined;
  SurveyYear: number = undefined;
}
