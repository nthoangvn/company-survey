import { StaffAccount } from '../objects/staff-account';

export class AccountInSurvey {
  Id: number = undefined;
  Staff: StaffAccount = undefined;
  SingleSurveyId: number = undefined;
  AccountOfStaffId: number = undefined;
  SalaryValue: number = undefined;
}
