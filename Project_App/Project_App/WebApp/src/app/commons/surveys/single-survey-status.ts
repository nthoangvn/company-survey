import { ValueFieldColor } from '../objects/status-field-color';

export enum SingleSurveyStatus {
    Open,
    Appending,
    Correcting,
    Submitted,
    Closed
}


export const SingleSurveyStatusColor = {
  [ValueFieldColor.Gray]: [SingleSurveyStatus.Open],
  [ValueFieldColor.Blue]: [SingleSurveyStatus.Appending],
  [ValueFieldColor.Golden]: [SingleSurveyStatus.Submitted],
  [ValueFieldColor.Black]: [SingleSurveyStatus.Closed]
};
