import { ValueFieldColor } from '../objects/status-field-color';

export enum TotalSurveyStatus {
    Created,
    Published,
    Validated,
    Completed
}

export const TotalSurveyStatusColor = {
  [ValueFieldColor.Gray]: [TotalSurveyStatus.Created],
  [ValueFieldColor.Green]: [TotalSurveyStatus.Published],
  [ValueFieldColor.Golden]: [TotalSurveyStatus.Validated],
  [ValueFieldColor.Purple]: [TotalSurveyStatus.Completed]
};


