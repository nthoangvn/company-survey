import { AccountInSurvey } from './account-in-single-survey';
import { Survey } from './survey';
import { SingleSurveyStatus } from './single-survey-status';

export class SingleSurvey {
    Id: number = undefined;
    SurveyName: string = undefined;
    Status: SingleSurveyStatus = undefined;
    SubmitDeadline: Date = undefined;
    CreatedDate: Date = undefined;
    CloseDate: Date = undefined;
    DirectorId: number = undefined;
    TotalSurveyId: number = undefined;
    SurveyYear: number = undefined;
    BranchCompanyId: number = undefined;
    BranchCompany: string = undefined;
}

export class SingleSurveyDetail extends SingleSurvey {
  TotalSurveyCreatedDate: Date = undefined;
  PublishDeadline: Date = undefined;
  ValidateDeadline: Date = undefined;
  FillingTime: Date = undefined;
  SubmitTime: Date = undefined;
  DirectorName: string = undefined;
  BranchCompany: string = undefined;
  AccountInSurvey: AccountInSurvey[] = undefined;
  Notes: SingleSurveyNotes[] = undefined;
}

export class SingleSurveyNotes {
  Id: number;
  SingleSurveyStatus: SingleSurveyStatus;
  Date: Date;
  Author: string;
  SurveyName: string;
}
