import { TotalSurvey } from "./total-survey";
import { SingleSurvey } from "./single-survey";

export interface SingleSurveyListOfSurvey extends TotalSurvey {
    SingleSurveyList: SingleSurvey[];
}