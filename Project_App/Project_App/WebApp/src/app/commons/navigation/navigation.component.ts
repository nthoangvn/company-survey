import * as $ from 'jquery';
import { overlayConfigFactory } from 'ngx-modialog';
import { Modal } from 'ngx-modialog/plugins/bootstrap';
import {
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Component,
    ElementRef,
    NgZone,
    OnInit,
    ViewChild
} from '@angular/core';

import { MenuItem, MenuItemType } from './../sub-menu/sub-menu.component';

import { Router } from '@angular/router';

import { BSModalContext } from 'ngx-modialog/plugins/bootstrap';
import { ToastProvider } from '../services/toast.provider';
import { Utils } from '../helper/utils';
import { DirectorRole, Director } from '../objects/director';
import { User } from '../objects/user';
import { Cookie } from '../helper/cookie';
import { CommonConstants } from '../constants/common-constants';
import { AuthService } from '../authentications/auth.service';
import { UserDto } from '../objects/userDto';

@Component({
    selector: 'app-navigation',
    templateUrl: 'navigation.component.html',
    styleUrls: ['navigation.component.less'],
})
export class NavigationComponent implements OnInit {
    @ViewChild('navbar') navbar: ElementRef;
    user = <Director>{};
    menuItems = <MenuItem[]>[];
    settingItems = <MenuItem[]>[];
    fullName: string;
    isScroll = false;
    userRoles = DirectorRole;
    constructor(public router: Router,
        public modal: Modal, public userService: AuthService, private toastProvider: ToastProvider,
        private cdr: ChangeDetectorRef, private ngZone: NgZone) {
          if (this.userService.currentUser.value) {
            this.user = this.userService.currentUser.value;
          }
         }

    get userFullname() {
      if (this.userService.loggedInUser) {
        return this.userService.loggedInUser.FirstName + ' ' + this.userService.loggedInUser.LastName;
      }
      return '';
    }

    get countryName() {
      if (this.userService.loggedInUser) {
        return this.userService.loggedInUser.CountryName;
      }
      return '';
    }

    ngOnInit(): void {
        // TODO TVN Check user access right implementations in S2.
        // this.userService.userSubject.subscribe(user => this.user = user);
        if (this.userService.loggedInUser) {
          Object.assign(this.user, this.userService.loggedInUser);
          if (!this.user) { return; }
          this.fullName = this.user.FirstName + ' ' + this.user.LastName;
          this.initProfileMenu(this.user);
        }
        this.ngZone.runOutsideAngular(() => $(window).scroll(e => this.onScroll()));
    }

    initMenu(): void {
        const menu = <MenuItem[]>[];
        // this.userService.getMenu().then(response => {
        //     this.rawMenu = response;
        //     if (Utils.isDefinedAndNotNull(response._embedded)) {
        //         MenuConfig.forEach(element => {
        //             const api = response._embedded.api.find(x => Utils.isDefined(x._links[element.rel.toLowerCase()]));

        //             if (Utils.isDefined(api)) {

        //                 if (element.hasSubMenu) {
        //                     element.subMenus = [];
        //                     MenuConfig.forEach(subElement => {
        //                         const subApi = api._embedded.api.find(x => Utils.isDefined(x._links[subElement.rel.toLowerCase()]));
        //                         if (subApi) {
        //                             subElement.routerLink = subApi._links[subElement.rel.toLowerCase()].href;
        //                             element.subMenus.push(subElement);
        //                         }
        //                     });
        //                 }
        //                 element.routerLink = api._links[element.rel.toLowerCase()].href;
        //                 menu.push(element);
        //             }
        //         });
        //         this.buildCloseMenuUrl();
        //     }
        //     this.menuItems = menu;
        //     if (window.location.pathname === '/') {
        //         let link;
        //         if (Utils.isNullOrEmpty(this.user.Startseite) || this.user.Startseite === '/') {
        //             const routerLinks = Utils.getAllRouterLinkFromMenu(this.rawMenu, this.translate);
        //             link = !Utils.isNullOrEmpty(routerLinks) ? routerLinks[0].Id : '/';
        //         } else {
        //             link = this.user.Startseite;
        //         }
        //         this.router.navigate([link]);
        //         this.router.initialNavigation();
        //     }
        //     this.cdr.markForCheck();
        // })
    }

    initProfileMenu(user: Director) {
        if (Utils.isDefinedAndNotNull(user)) {
            this.settingItems.push(<MenuItem>{
                displayName: 'User Information',
                rel: 'user-setting',
                iconClass: 'icon-setting',
                itemType: MenuItemType.Action,
                index: 1
            });
            this.settingItems = [...this.settingItems];
            this.settingItems.push(<MenuItem>{
              displayName: 'Log out',
              rel: 'user-setting',
              iconClass: 'icon-close',
              itemType: MenuItemType.AbsoluteLink,
              routerLink: 'http://localhost:9000/login',
              index: 2
          });
        }
        this.settingItems = [...this.settingItems];
    }


    onScroll() {
        const scrollHeight = $(document).height() - $(window).height();
        if (document.documentElement.scrollTop !== 0 && document.documentElement.scrollTop !== scrollHeight) {
            if (!this.navbar.nativeElement.classList.contains('navbar-shadow')) {
                this.navbar.nativeElement.classList.add('navbar-shadow');
            }
        } else if (this.navbar.nativeElement.classList.contains('navbar-shadow')) {
            this.navbar.nativeElement.classList.remove('navbar-shadow');
        }
    }

    changeInstitut(institutId: number) {
        // this.user.Gruppe = this.user.InstitutDtoList.find(x => x.Id === institutId).Gruppe
        // this.user.GruppeName = CommonEnumMapper.GruppeNameMap(this.user.Gruppe);
        // Cookie.setCookie('eIAMuserInstitutIdEHP', institutId, CommonConstants.CookieExpiresValue);
        // location.href = '/';
    }
}
