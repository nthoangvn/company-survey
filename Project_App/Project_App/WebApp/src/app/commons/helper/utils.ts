import 'rxjs';

import { TreeNode } from 'primeng/api';
import * as isUndefined from 'lodash.isUndefined';
import * as isNull from 'lodash.isNull';
import * as isEmpty from 'lodash.isEmpty';
import * as isEqual from 'lodash.isEqual';
import * as pick from 'lodash.pick';
import * as keys from 'lodash.keys';
import * as omit from 'lodash.omit';
import * as assign from 'lodash.assign';
import * as omitBy from 'lodash.omitby';
import * as sortBy from 'lodash.sortBy';

import * as moment from 'moment';
import { CommonConstants, ValueFieldColor } from '../constants/common-constants';

export class Utils {
    static stringFormat(source: string, args: {}): string {
        if (!source || !args) {
            return source;
        }
        return source.replace(/{(\d+)}/g, (match, number) => {
            return (typeof args[number] !== 'undefined' && args[number] !== null) ? args[number] : match;
        });
    }
    static isDefined(value): boolean {
        return !isUndefined(value);
    }

    static isDefinedAndNotNull(value): boolean {
        return !isUndefined(value) && !isNull(value);
    }

    static isNull(value): boolean {
        return isNull(value);
    }

    static isNullOrUnDefined(value): boolean {
        return isNull(value) || isUndefined(value);
    }

    static isNullOrEmpty(value): boolean {
        return isNull(value) || isUndefined(value) || isEmpty(value);
    }

    static isEmptyArray(value): boolean {
        return Array.isArray(value) && isEmpty(value);
    }

    static dateTimeFormat(value, format?: string): string {
        const date = new Date(value);
        if (isNaN(date.getTime())) {
            return '';
        }

        return moment(date).format(format || CommonConstants.DefaultMomentDateTimeFormat);
    }

    static dateFromToFormat(fromDateValue: Date, toDateValue: Date,
        fromFormat?: string, toFormat?: string, separator?: string): string {
        const fromDate = new Date(fromDateValue);
        const toDate = new Date(toDateValue);

        if (isNaN(fromDate.getTime()) || isNaN(toDate.getTime())) {
            return '';
        }
        let from = '';
        if (fromDate.getFullYear() === toDate.getFullYear()) {
            from = moment(fromDate).format(fromFormat || CommonConstants.DefaultMomentOnlyDateFormat);
        } else {
            from = moment(fromDate).format(fromFormat || CommonConstants.DefaultMomentDateFormat);
        }

        const to = moment(toDate).format(toFormat || CommonConstants.DefaultMomentDateFormat);

        return `${from}${separator || '-'}${to}`;
    }

    static isEmptyObject(obj) {
        return (obj && (Object.keys(obj).length === 0));
    }
    static getUpperLangField(field: string, lang = 'DE'): string {
        const langLength = 2;
        return `${field}${lang.slice(0, langLength).toUpperCase()}`;
    }
    static getLangField(field: string, lang = 'De'): string {
        const langLength = 2;
        return `${field}${lang.slice(0, 1).toUpperCase()}${lang.slice(1, langLength).toLowerCase()}`;
    }

    static getFieldColorCss(option, value): string {
        if (Utils.isDefined(option[ValueFieldColor.Black]) &&
            option[ValueFieldColor.Black].indexOf(value) > -1) { return 'black-field'; }
        if (Utils.isDefined(option[ValueFieldColor.Green]) &&
            option[ValueFieldColor.Green].indexOf(value) > -1) { return 'green-field'; }
        if (Utils.isDefined(option[ValueFieldColor.Gray]) &&
            option[ValueFieldColor.Gray].indexOf(value) > -1) { return 'gray-field'; }
        if (Utils.isDefined(option[ValueFieldColor.Golden]) &&
            option[ValueFieldColor.Golden].indexOf(value) > -1) { return 'golden-field'; }
        if (Utils.isDefined(option[ValueFieldColor.Orange]) &&
            option[ValueFieldColor.Orange].indexOf(value) > -1) { return 'orange-field'; }
        if (Utils.isDefined(option[ValueFieldColor.Red]) &&
            option[ValueFieldColor.Red].indexOf(value) > -1) { return 'red-field'; }
        if (Utils.isDefined(option[ValueFieldColor.Purple]) &&
            option[ValueFieldColor.Purple].indexOf(value) > -1) { return 'purple-field'; }
        if (Utils.isDefined(option[ValueFieldColor.LightGreen]) &&
            option[ValueFieldColor.LightGreen].indexOf(value) > -1) { return 'light-green-field'; }
        if (Utils.isDefined(option[ValueFieldColor.LightBlue]) &&
            option[ValueFieldColor.LightBlue].indexOf(value) > -1) { return 'light-blue-field'; }
        return '';
    }

    // only apply null / undefined / empty check for first-level properties
    // we can improve by apply omit by recursively but it is not really useful until now
    static isEqual(obj1, obj2, pickFields?: string[], omitFields?: string[]): boolean {
        let clone2;
        let clone1;
        clone1 = omitBy(obj1, x => this.omitFunc(x));
        clone2 = omitBy(obj2, x => this.omitFunc(x));
        if (!pickFields && !omitFields) {
            return isEqual(clone1, clone2);
        } else {
            if (pickFields) {
                clone1 = pick(clone1, pickFields);
                clone2 = pick(clone2, pickFields);
                return isEqual(clone2, clone1);
            }
            if (omitFields) {
                clone1 = omit(clone1, pickFields);
                clone2 = omit(clone2, pickFields);
                return isEqual(clone2, clone1);
            }
        }
        return false;
    }

    static omitFunc(x) {
        return isNull(x) || isUndefined(x) || x === '' || (Array.isArray(x) && x.length === 0);
    }

    static assignByKeys(destinationObject, sourceObject) {
        return assign(destinationObject, pick(sourceObject, keys(destinationObject)));
    }

    static breakWord(targetString: string, maxLength: number, maxLine: number, showEllipsis = true): string {
        let trimmedString;
        if (targetString.length > maxLength) {
            // trim the string to the maximum length
            trimmedString = targetString.substr(0, maxLength);
            // re-trim if we are in the middle of a word
            return `${trimmedString.substr(0,
                Math.min(trimmedString.length, trimmedString.lastIndexOf(' ')))} ${showEllipsis ? '...' : ''}`;
        }
        if (targetString.split('\n').length > maxLine) {
            // trim the string to the maximum line
            trimmedString = targetString.slice(0, this.getLastWordOfLineIndex(targetString, maxLine));
            // re-trim if we are in the middle of a word
            return `${trimmedString.substr(0,
                Math.min(trimmedString.length, trimmedString.lastIndexOf('\n')))} ${showEllipsis ? '...' : ''}`;
        }
        return targetString;
    }

    static getLastWordOfLineIndex(text: string, maxLine: number): number {
        let index = 0;
        text.split('\n').forEach((line, i) => {
            if (i > maxLine) {
                return;
            } else {
                index += line.length;
            }
        });
        return index;
    }

    static arrayToObject(array: any[], keyFieldName: string) {
        return array.reduce((object, item) => {
            object[item[keyFieldName]] = item;
            return object;
        }, {});
    }

    static getParentUrl(url: string) {
        const the_arr = url.split('/');
        the_arr.pop();
        return (the_arr.join('/'));
    }

    static buildSpezialthemaDisplayText(zulassungstypSpezialthema: any, translateService: any) {
        // Extract lang from culture info
        const fromIndex = 0;
        const langInfoLength = 2;
        const lang = translateService.currentLang.substr(fromIndex, langInfoLength).toLowerCase();
        switch (lang) {
            case 'de':
                zulassungstypSpezialthema.forEach(x => {
                    x.Text = x.NameDe;
                });
                break;
            case 'fr':
                zulassungstypSpezialthema.forEach(x => {
                    x.Text = x.NameFr ? x.NameFr : x.NameDe;
                });
                break;
            case 'en':
                zulassungstypSpezialthema.forEach(x => {
                    x.Text = x.NameEn ? x.NameEn : x.NameDe;
                });
                break;
            case 'it':
                zulassungstypSpezialthema.forEach(x => {
                    x.Text = x.NameIt ? x.NameIt : x.NameFr ? x.NameFr : x.NameDe;
                });
                break;
        }

        return sortBy(zulassungstypSpezialthema, [function (o) {
            if (o.Text) { return (<string>o.Text).toLowerCase(); }
            return '';
        }]);
    }

    static buildDropdownDisplayText(dropdownList: any, translateService: any) {
        // Extract lang from culture info
        const fromIndex = 0;
        const langInfoLength = 2;
        const lang = translateService.currentLang.substr(fromIndex, langInfoLength).toLowerCase();
        switch (lang) {
            case 'de':
                dropdownList.forEach(x => {
                    x.Text = x.TextDe;
                });
                break;
            case 'fr':
                dropdownList.forEach(x => {
                    x.Text = x.TextFr ? x.TextFr : x.TextDe;
                });
                break;
            case 'en':
                dropdownList.forEach(x => {
                    x.Text = x.TextEn ? x.TextEn : x.TextDe;
                });
                break;
            case 'it':
                dropdownList.forEach(x => {
                    x.Text = x.TextIt ? x.TextIt : x.TextFr ? x.TextFr : x.TextDe;
                });
                break;
        }
        return dropdownList;
    }


    static flattenTreeNodes(treeNodes: TreeNode[], outResult: TreeNode[]) {
        treeNodes.forEach(node => {
            outResult.push(node);
            if (node.children && node.children.length > 0) {
                this.flattenTreeNodes(node.children, outResult);
            }
        });
    }

}
