
export class Cookie {
    static setCookie(cname, cvalue, exdays) {
        const milisecondOfDay = 86400000; // = 24 * 60 * 60 * 1000;
        const d = new Date();
        d.setTime(d.getTime() + (exdays * milisecondOfDay));
        const expires = 'expires=' + d.toUTCString();
        document.cookie = cname + '=' + cvalue + ';' + expires;
    }

    static getCookie(cname) {
        const name = cname + '=';
        const decodedCookie = decodeURIComponent(document.cookie);
        const ca = decodedCookie.split(';');
        for (let i = 0; i < ca.length; i++) {
            const c = ca[i].trim();
            if (c.indexOf(name) === 0) {
                return c.substring(name.length, c.length);
            }
        }
        return '';
    }
}
