import * as moment from 'moment';
import { CommonConstants } from '../constants/common-constants';


function formatNumerToHave2Digit(value: number) {
    return value < 10 ? '0' + value : value;
}

export interface Pair<T1, T2> {
    left: T1;
    right: T2;
}

export interface FormatFromTo {
    isDateTime?: boolean;
    from?: string | string[];
    to: string;
}

export interface DateOptions {
    formats?: Pair<string | string[], string | string[]>;
    truncate?: string;
}

moment.locale('de');

export class Dates {

    public static HOUR_MINUTE_DATE_OPTIONS: DateOptions = {
        formats: { left: 'HH:mm', right: 'HH:mm' },
        truncate: 'hour'
    };
    /**
     * Check if two date strings are on the same day
     * @param lhs first date string
     * @param rhs second date string
     *
     */
    public static isSame(lhs: string, rhs: string, options?: DateOptions): boolean {
        const { formats, truncate } = this.buildOptions(options);
        const left = this.parse(lhs, formats.left);
        const right = this.parse(rhs, formats.right);
        const truncateValue: moment.unitOfTime.StartOf = truncate as moment.unitOfTime.StartOf;

        return left.isValid() && right.isValid() && left.isSame(right, truncateValue);
    }

    /**
     * Check if first date string is after second date string
     * @param lhs first date string
     * @param rhs second date string
     *
     */
    public static isAfter(lhs: string, rhs: string, options?: DateOptions): boolean {
        const { formats, truncate } = this.buildOptions(options);
        const left = this.parse(lhs, formats.left);
        const right = this.parse(rhs, formats.right);
        const truncateValue: moment.unitOfTime.StartOf = truncate as moment.unitOfTime.StartOf;

        return left.isValid() && right.isValid() && left.isAfter(right, truncateValue);
    }

    /**
     * Check if first date string is before second date string
     * @param lhs first date string
     * @param rhs second date string
     *
     */
    public static isBefore(lhs: string, rhs: string, options?: DateOptions): boolean {
        // Swap formats before passing to isAfter for inverse case
        if (options && options.formats) {
            options.formats = {
                left: options.formats.right,
                right: options.formats.left
            };
        }

        return this.isAfter(rhs, lhs, options);
    }

    /**
     * Check if first date string is after or equal second date string
     * @param lhs first date string
     * @param rhs second date string
     *
     */
    public static isAfterOrEquals(lhs: string, rhs: string, options?: DateOptions): boolean {
        return this.isAfter(lhs, rhs, options) || this.isSame(lhs, rhs, options);
    }

    /**
     * Check if first date string is before or equal second date string
     * @param lhs first date string
     * @param rhs second date string
     *
     */
    public static isBeforeOrEquals(lhs: string, rhs: string, options?: DateOptions): boolean {
        return this.isBefore(lhs, rhs, options) || this.isSame(lhs, rhs, options);
    }

    /**
     * Transform date string to another format
     * @param str date
     *
     */
    public static format(str: string, formats?: FormatFromTo): string {
        formats = this.buildFormats(formats);
        return this.parse(str, formats.from).format(formats.to);
    }

    /**
     * Transform Date value to date string
     *
     */
    public static formatDate(date: Date, to?: string) {
        const value = moment(date);
        if (value.isValid()) {
            return value.format(to || CommonConstants.DefaultMomentDateFormat);
        }
        return '';
    }

    /**
     * Get current date time
     *
     */
    public static now(format?: string) {
        return moment().format(format || CommonConstants.TimeIncludeSecondFormat);
    }

    /**
     * Check if a string is a valid date
     * @param str date
     *
     */
    public static isValid(str: string, format?: string | string[]): boolean {
        return this.parse(str, format).isValid();
    }

    /**
     * Parse string with format and return moment object
     *
     */
    public static parse(str: string, format?: string | string[]) {
        return moment(str, format || CommonConstants.DefaultMomentDateFormat, /* strict parsing */ true);
    }

    private static buildOptions(options: DateOptions = {}): DateOptions {
        const leftFormat: string | string[] = options.formats && options.formats.left;
        const rightFormat: string | string[] = options.formats && options.formats.right;

        return {
            formats: { left: leftFormat, right: rightFormat },
            truncate: options.truncate || 'day'
        };
    }

    private static buildFormats(formats: FormatFromTo): FormatFromTo {
        return formats || {
            to: formats && formats.isDateTime ?
                CommonConstants.TimeIncludeSecondFormat : CommonConstants.DefaultMomentDateFormat
        };
    }

    public static getDiffDay(startDay: Date, endDay?: Date) {
        return moment(startDay).startOf('day').diff(moment(endDay || moment()).startOf('day'), 'days');
    }

    static dateFormat(value: any, format?: string): string {
        const date = new Date(value);
        if (isNaN(date.getTime())) {
            return '';
        }

        return moment(date).format(format || CommonConstants.DefaultMomentDateFormat);
    }

    public static addDay(date: Date, value: number) {
        return moment(date).add(value, 'days').toDate();
    }

    public static removeTimeFromDate(date: Date) {
        return moment(date).startOf('day').toDate();
    }

}
