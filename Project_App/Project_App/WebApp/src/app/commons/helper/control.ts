
import { FormControlName } from '@angular/forms';
import { NgZone } from '@angular/core';
import * as $ from 'jquery';
import { CommonConstants } from '../constants/common-constants';


export class ControlHelper {

    static getDisplayName(formControlName: FormControlName): string {
        return formControlName.control['displayName'] ? formControlName.control['displayName'] : formControlName.name;
    }

    static getSameFormDisplayName(formControlName: FormControlName, prop: string): string {
        return formControlName.control.parent.controls[prop]['displayName']
            ? formControlName.control.parent.controls[prop]['displayName']
            : prop;
    }

    static buildValidationError(messageKey: string, params?: {}[]): { validation: { messageKey: string, messageParams: {}[] } } {
        return {
            validation: { messageKey: messageKey, messageParams: params }
        };
    }

    static scrollAction(ngZone: NgZone) {
        ngZone.runOutsideAngular(() => {
            $('body,html').animate({
                scrollTop: 0
            }, CommonConstants.ScrollDuration);
        });
    }

}
