import { User } from "./user";

export class Staff extends User {
    StartDate: Date;
    Position: string;
    DepartmentId: number;
}
