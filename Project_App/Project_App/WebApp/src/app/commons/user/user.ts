export class User {
    Id: number;
    FirstName: string;
    LastName: string;
    StaffVisa: string;
    Phone: string;
    Activated: boolean;
}
