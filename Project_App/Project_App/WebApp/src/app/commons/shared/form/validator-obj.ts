import {ValidatorFn} from '@angular/forms';

export class ValidatorObj {
    [key: string]: ValidatorFn;
}
