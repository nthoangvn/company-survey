
import {of as observableOf,  Observable } from 'rxjs';
import { Modal } from 'ngx-modialog/plugins/bootstrap';
import { OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ToastProvider } from '../../services/toast.provider';




export interface IForm {
    isDirty(): boolean;
    submit(): void;
    reset(): void;
}

/**
 * Abstract form Component. A base class for form based component which support
 * dirty check on navigation via DirtyCheck guard, programmatic validation.
 */
export abstract class AbstractFormComponent<T> implements IForm, OnInit {
    originalData: T = <T>{};
    data: T = <T>{};
    /**
     * Dirty check message for the component
     * Can be a key value from i18n locale file or plain text message
     */
    dirtyCheckMessage: string;
    /**
     * the root form group of the form.
     */
    public formGroup: FormGroup;
    /**
     * Submitted flag, it is used to force to show validation error
     * when the form is submitted with validation error.
     */
    protected submitted: boolean;

    /**
     * The target entity which is bound to the form input controls
     */

    constructor(protected formBuilder: FormBuilder, protected modal: Modal,
        protected toastProvider: ToastProvider) { }

    public ngOnInit() {
        this.formGroup = this.configureFormGroup();
    }

    /**
     * Validate and submits the form.
     */
    public submit(): void {
        // Only perform valid check when formGroup is defined.
        this.formGroup.markAsTouched();
        if (this.checkFormNotValid()) {
            this.toastProvider.validationError();
            for (const item in this.formGroup.controls) {
                if (this.formGroup.controls[item]) {
                    this.formGroup.controls[item].markAsTouched();
                    this.formGroup.controls[item].markAsDirty();
                }
            }
            return;
        }

        this.validateForm().subscribe((res) => {
            if (!res || res.length === 0) {
                this.submitForm();
            } else {
                // UI feedback for specific form validation
                this.modal.alert()
                    .dialogClass('modal-dialog error-dialog')
                    .title('Error dialog')
                    .body('Save')
                    .open();
            }
        });
    }

    /**
     * disabled state is considered as NOT invalid ("technical valid")
     */
    public checkFormNotValid(): boolean {
        for (const item in this.formGroup.controls) {
            if (this.formGroup.controls[item]) {
                (<FormGroup>this.formGroup.controls[item]).updateValueAndValidity();
            }
        }
        return this.formGroup.invalid;
    }

    public markAsTouchedAndDirty() {
        this.formGroup.markAsTouched();
        this.formGroup.markAsDirty();
        for (const item in this.formGroup.controls) {
            if (this.formGroup.controls[item]) {
                this.formGroup.controls[item].markAsTouched();
                this.formGroup.controls[item].markAsDirty();
            }
        }
    }
    /**
     * Reset the form.
     */
    public reset(): void {
        this.submitted = false;
        this.formGroup.reset();
    }

    /**
     * dirty flag
     */
    public isDirty(): boolean {
        return this.formGroup.dirty;
    }

    /**
     * Reset state of formGroup back to pristine.
     */
    public resetDirty(): void {
        this.formGroup.markAsPristine();
    }

    /**
     * Determine whether or not input form object at invalid state.
     * @returns true if form is valid, otherwise false.
     */
    public checkFormStatus(): boolean {
        let valid = true;
        for (const path in this.formGroup.controls) {
            if (this.formGroup.controls[path].invalid) {
                // Turn on field dirty flag to display error message.
                this.formGroup.controls[path].markAsDirty();
                valid = false;
            }
        }
        if (!valid) {
            return true;
        }
        return valid;
    }

    /**
     * Get raw value from current formGroup.
     *
     */
    public formValue(): { [key: string]: any } {
        return this.formGroup.getRawValue();
    }

    protected validateForm(): Observable<string[]> {
        return observableOf([]);
    }

    /**
     * The actual submit logic of the child class.
     * This is only called when no validation errors found.
     */
    protected abstract submitForm(): void;

    /**
     * Configure the form group e.g. default value, Validators...etc.
     */
    protected abstract configureFormGroup(): FormGroup;
}
