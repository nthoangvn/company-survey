import {
    AfterViewInit,
    Component,
    ContentChild,
    ElementRef,
    Input,
    OnChanges,
    OnDestroy,
    SimpleChanges,
    ViewChild,
    ChangeDetectorRef,
} from '@angular/core';
import { FormControlName } from '@angular/forms';
import { Subscription } from 'rxjs';
import * as includes from 'lodash.includes';
import { Utils } from '../../../helper/utils';
import { ControlHelper } from '../../../helper/control';

@Component({
    selector: 'app-form-field',
    templateUrl: './form-field.view.html'
})
export class FormFieldComponent implements AfterViewInit, OnChanges {
    @ViewChild('inputGroup') inputGroup: ElementRef;
    @ContentChild(FormControlName) controlName: FormControlName;
    /* Define label of ng-content*/
    @Input() label: string;
    @Input() isShowLabel = true;
    @Input() inline: boolean;
    @Input() noSpacing = false;
    @Input() labelCol: number;
    @Input() controlCol: number;
    /* Define whether or not X button for clear value will be displayed with component. Default is true.*/
    @Input() clearable = true;
    @Input() hasValidationMessage = true;
    subscription: Subscription;
    disable = true;

    constructor(private cdr: ChangeDetectorRef) { }
    ngAfterViewInit(): void {
        this.buildControlDisplayName();
    }

    public ngOnChanges(changes: SimpleChanges): void {
        if (changes.hasOwnProperty('label') && !Utils.isNullOrEmpty(this.label)) {
            this.buildControlDisplayName();
        }
    }

    private buildControlDisplayName() {
        if (Utils.isNullOrUnDefined(this.controlName) || Utils.isNullOrUnDefined(this.controlName.control)) {
            return;
        }
        if (Utils.isNullOrEmpty(this.controlName.control['displayNameKey'])) {
            if (!Utils.isNullOrEmpty(this.label)) {
                this.controlName.control['displayName'] = this.label;
            }
        } else {
            this.controlName.control['displayName'] = this.controlName.control['displayNameKey'];
        }
    }

    clearValue(event: Event): void {
        if (this.controlName.control) {
            this.controlName.control.setValue(undefined);
        }
    }

    get showValidationError(): boolean {
        return this.controlName && !!this.controlName.control
            && this.controlName.control.dirty && this.controlName.control.invalid && this.controlName.control.touched;
    }

    get warning(): string {
        if (!this.controlName || !this.controlName.control) {
            return '';
        }
        for (const violation in this.controlName.control.errors) {
            if (violation === 'warning') {
                return this.controlName.control.getError(violation);
            }
        }
        return '';
    }

    // get errorMessage(): string {
    //     const errors = this.controlName.control.errors;
    //     const controlName = this.controlName;
    //     // tslint:disable-next-line:forin
    //     for (const violation in errors) {
    //         if (violation === 'required') {
    //             return errors.customMessageKey
    //                 ? errors.customMessageKey
    //                 : 'Required Field';
    //         }
    //     }
    // }

    get errorMessage(): string {
        const errors = this.controlName.control.errors;
        const controlName = this.controlName;
        // tslint:disable-next-line:forin
        for (const violation in errors) {
            if (violation === 'required') {
                return errors.customMessageKey
                    ? errors.customMessageKey
                    : 'Required Field';
            } else if (violation === 'maxlength' || violation === 'minlength') {
                return errors.customMessageKey
                    ? errors.customMessageKey.replace('{{requiredLength}}', errors[violation]['requiredLength'])
                    : violation;
            } else if (violation === 'dateInvalidAfter' || violation === 'dateInvalidBefore') {
                return errors.customMessageKey
                    ? errors.customMessageKey
                    : violation;
            } else if (violation === 'validation') {
                return errors[violation]['messageKey'] ?
                    errors[violation]['messageKey']
                    : violation;
            }
            return violation;
        }
    }
    get labelColCss() {
        return Utils.isNullOrUnDefined(this.labelCol) || !this.inline
        ? '' : `col-md-${this.labelCol} col-sm-${this.labelCol} col-lg-${this.labelCol}`;
    }
    get controlColCss() {
        return Utils.isNullOrUnDefined(this.controlCol) || !this.inline
        ? '' : `col-md-${this.controlCol} col-sm-${this.controlCol} col-lg-${this.controlCol}`;
    }
    get clearVisibility(): boolean {
        if (!this.controlName) {
            return this.clearable;
        }
        return this.clearable && !!this.controlName.control && this.controlName.control.value;
    }

}
