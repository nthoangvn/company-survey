
import { ControlValueAccessor, Validator, AbstractControl, ValidationErrors } from '@angular/forms';
import { BaseFormComponent } from '../baseForm.component';

import { Input, OnChanges, SimpleChange, OnInit, OnDestroy } from '@angular/core';

export abstract class BaseSubFormComponent<T> extends BaseFormComponent<T>
    implements ControlValueAccessor, Validator, OnChanges, OnInit, OnDestroy {
    @Input() originalData: T;
    @Input() data: T;

    @Input() disabled = false;

    protected onChangeFn = (_) => { };
    protected onTouchFn = () => { };

    protected onErrorFn = (_) => { };

    ngOnInit() {
        this.prepareForm();
    }

    ngOnChanges(changes: { [propName: string]: SimpleChange }) {
        if (changes.hasOwnProperty('data')) {
            this.onChangeFn(this.data);
        }
    }

    validate(c: AbstractControl): ValidationErrors {
        if (!this.formGroup.valid) {
            return { subforminvalid: 'subforminvalid' };
        }
        return null;
    }

    registerOnValidatorChange(fn: () => void): void {
        this.onErrorFn = fn;
    }

    writeValue(obj: T): void {
    }
    registerOnChange(fn): void {
        this.onChangeFn = fn;
    }
    registerOnTouched(fn): void {
        this.onTouchFn = fn;
    }
    setDisabledState(isDisabled: boolean): void {
        this.disabled = isDisabled;
    }

    doValidation() {
        this.checkFormNotValid();
        this.onChangeFn(this.data);
    }

    emitChange() {
        setTimeout(() => {
            this.onChangeFn(this.data);
        });
    }
}
