import { Component, OnInit, Input, SimpleChange, OnChanges, } from '@angular/core';
import { FormGroup } from '@angular/forms';

export class BaseSubFieldsComponent implements OnInit, OnChanges {
  @Input() data;
  @Input() fields: string[];
  @Input() group: FormGroup;
  @Input() disabled = false;
  constructor() {
  }

  ngOnInit() {
  }

  private initValue() {
    const result = {};
    if (this.group) {
      if (this.fields && this.fields.length > 0) {
        this.fields.forEach(x => result[x] = this.data[x]);
        this.group.patchValue(result);
      }
    }
  }
  ngOnChanges(changes: { [propName: string]: SimpleChange }) {
    if (changes.hasOwnProperty('group')) {
      this.initValue();
    }
  }
}
