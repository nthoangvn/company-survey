
import * as cloneDeep from 'lodash.clonedeep';
import { OnInit, OnDestroy, AfterViewInit, Input } from '@angular/core';
import { Location } from '@angular/common';
import { FormGroup, FormBuilder } from '@angular/forms';
import { BSModalContext, Modal } from 'ngx-modialog/plugins/bootstrap';
import { Subscription } from 'rxjs';
import { overlayConfigFactory } from 'ngx-modialog';

import { AbstractFormComponent } from './form.component';
import { Utils } from '../../../commons/helper/utils';
import { FormControlDecorator } from '../../../commons/shared/form/form-control-decorator';
import { ToastProvider } from '../../services/toast.provider';


export interface IDynamicRule {
    changeField: string;
    dependedField: string;
    validatorsTrue: any[];
    validatorsFalse: any[];
    trueValue: any;
    emitEvent?: boolean;
}
export interface IDynamicValidation {
    changeField: string;
    dependedField: string;
    emitEvent?: boolean;
    markAsDirty?: boolean;
}
export abstract class BaseFormComponent<T> extends AbstractFormComponent<T> implements OnInit, OnDestroy, AfterViewInit {
    rules: any = {};
    dynamicRules: IDynamicRule[] = [];
    dynamicValidations: IDynamicValidation[] = [];
    childrenForms: any[] = [];
    valueChangeSubs: Subscription[] = [];

    @Input() disabled = false;

    constructor(protected formBuilder: FormBuilder, protected modal: Modal,
        protected location: Location, protected toastProvider: ToastProvider) {
        super(formBuilder, modal, toastProvider);
    }

    get isDiff(): boolean {
        // if (this.submitted) {
        //     return false;
        // }
        return !Utils.isEqual(this.originalData, this.data);
    }

    public isDiffBy(pick?: string[], omit?: string[]): boolean {
        return !Utils.isEqual(this.originalData, this.data, pick, omit);
    }

    protected resetRequiredFields(rFields: Array<string>) {
        for (let i = 0; i < rFields.length; i++) {
            const fieldName = rFields[i];
            this.data[fieldName] = this.originalData[fieldName];
        }
    }

    public ngOnDestroy() {
        this.valueChangeSubs.forEach(x => x.unsubscribe());
    }
    public ngOnInit() {
        this.prepareData();
        this.prepareForm();
    }

    public ngAfterViewInit(): void {
        setTimeout(() => {
            this.dynamicValidations.forEach(x => {
                this.valueChangeSubs.push(this.formGroup.get(x.changeField).valueChanges.subscribe(value => {
                    if (x.markAsDirty) {
                        this.formGroup.get(x.dependedField).markAsDirty();
                        this.formGroup.get(x.dependedField).markAsTouched();
                    }
                    this.formGroup.get(x.dependedField).updateValueAndValidity({ emitEvent: x.emitEvent });
                    return;
                }));
            });
        });
    }

    public prepareForm() {
        this.formGroup = this.configureFormGroup();
        this.reset();
        this.resetDirty();
    }
    public prepareData(newData?: T) {
        if (Utils.isDefinedAndNotNull(newData)) {
            this.originalData = newData;
        }
        this.data = cloneDeep(this.originalData);
    }

    public new() {
        return <T>{};
    }

    public submit() {
        this.childrenForms.forEach(x => {
            if (!!x) { x.doValidation(); x.markAsTouchedAndDirty(); }
        });
        super.submit();
    }
    protected configureFormGroup(): FormGroup {
        const rules: any = {};
        Object.assign(rules, this.rules);
        const formGroup = this.formBuilder.group(rules);
        this.dynamicRules.forEach(x => {
            this.valueChangeSubs.push(formGroup.get(x.changeField).valueChanges.subscribe(value => {
                if (Utils.isDefinedAndNotNull(value) && Utils.isDefinedAndNotNull(formGroup.get(x.dependedField))) {
                    formGroup.get(x.dependedField).clearValidators();
                    // tslint:disable-next-line:triple-equals
                    const required = value === x.trueValue;
                    if (required) {
                        FormControlDecorator.setValidators(formGroup.get(x.dependedField), x.validatorsTrue);
                    } else {
                        FormControlDecorator.setValidators(formGroup.get(x.dependedField), x.validatorsFalse);
                    }
                    formGroup.get(x.dependedField).updateValueAndValidity({ emitEvent: x.emitEvent });
                    return;
                }
            }));
        });
        return formGroup;
    }
    protected submitForm() {
        this.submitted = true;
        this.resetDirty();
        this.submitSuccess();
    }
    protected submitSuccess() {
        this.toastProvider.showSuccessToast('common.saveSuccess');
    }

    async back() {
        this.goBack();
    }

    goBack() {
        this.location.back();
    }
}
