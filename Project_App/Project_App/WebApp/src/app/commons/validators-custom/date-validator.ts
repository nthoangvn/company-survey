
import { Injectable } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Dates } from '../helper/date';
import { ControlHelper } from '../helper/control';
import { CommonConstants } from '../constants/common-constants';


@Injectable()
export class DateValidator {
    static isDate(control: FormControl) {
        return (!control.value || Dates.isValid(control.value)) ? null :
            { validation: 'common.error.dateInvalid.invalid' };
    }

    static notInPast() {
        return (control: FormControl) => {
            if (control.value && Dates.isValid(control.value)
                && Dates.isBefore(control.value, Dates.now(CommonConstants.DefaultMomentDateFormat))) {
                return ControlHelper.buildValidationError('common.validation.notInPast');
            } else {
                return null;
            }
        };
    }

    static inFuture() {
        return (control: FormControl) => {
            if (control.value && Dates.isValid(control.value)
                && Dates.isBeforeOrEquals(control.value, Dates.now(CommonConstants.DefaultMomentDateFormat))) {
                return ControlHelper.buildValidationError('common.validation.inFuture');
            } else {
                return null;
            }
        };
    }

    static laterThanXDaysFromNow(x: number, customMessageKey = 'common.validation.laterThanXDaysFromNow') {
        return (control: FormControl) => {
            if (control.value && Dates.isValid(control.value)
                && Dates.getDiffDay(control.value) < x) {
                return ControlHelper.buildValidationError(customMessageKey, [x]);
            } else {
                return null;
            }
        };
    }

    static laterThanOrEqual(property: string, customMessageKey?: string) {
        return (control: FormControl) => {
            const compareControl = control.parent.controls[property];
            const valueOfProperty = compareControl.value;
            if (control.value && Dates.isValid(control.value)
                && valueOfProperty && Dates.isValid(valueOfProperty)
                && !Dates.isAfterOrEquals(control.value, valueOfProperty)) {
                return {
                    dateInvalidAfter: 'common.validation.dateInvalidAfter',
                    compareProperty: property,
                    customMessageKey: customMessageKey
                };
            } else {
                return null;
            }
        };
    }

    static laterThan(property: string, customMessageKey?: string) {
        return (control: FormControl) => {
            const compareControl = control.parent.controls[property];
            const valueOfProperty = compareControl.value;
            if (control.value && Dates.isValid(control.value)
                && valueOfProperty && Dates.isValid(valueOfProperty)
                && !Dates.isAfter(control.value, valueOfProperty)) {
                return {
                    dateInvalidAfter: 'common.validation.dateInvalidAfter',
                    compareProperty: property,
                    customMessageKey: customMessageKey
                };
            } else {
                return null;
            }
        };
    }
    static beforeThan(property: string, customMessageKey?: string) {
        return (control: FormControl) => {
            const compareControl = control.parent.controls[property];
            const valueOfProperty = compareControl.value;
            if (control.value && Dates.isValid(control.value)
                && valueOfProperty && Dates.isValid(valueOfProperty)
                && !Dates.isBefore(control.value, valueOfProperty)) {
                return {
                    dateInvalidBefore: 'common.validation.dateInvalidBefore',
                    compareProperty: property,
                    customMessageKey: customMessageKey
                };
            } else {
                return null;
            }
        };
    }
    static beforeThanOrEqual(property: string, customMessageKey?: string) {
        return (control: FormControl) => {
            const compareControl = control.parent.controls[property];
            const valueOfProperty = compareControl.value;
            if (control.value && Dates.isValid(control.value)
                && valueOfProperty && Dates.isValid(valueOfProperty)
                && !Dates.isBeforeOrEquals(control.value, valueOfProperty)) {
                return {
                    dateInvalidBefore: 'common.validation.dateInvalidBefore',
                    compareProperty: property,
                    customMessageKey: customMessageKey
                };
            } else {
                return null;
            }
        };
    }
}
