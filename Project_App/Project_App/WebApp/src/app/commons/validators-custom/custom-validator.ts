import { Injectable } from '@angular/core';
import { Validators, ValidatorFn, FormControl } from '@angular/forms';
import { Utils } from '../helper/utils';
import { ControlHelper } from '../helper/control';
import { isArray } from 'util';

@Injectable()
export class CustomValidator {
    static Required(messageKey: string): ValidatorFn {
        return (control: FormControl) => {
            const result = Validators.required(control);
            if (result && result.required) {
                result.customMessageKey = messageKey;
            }
            return result;
        };
    }
    static Email(messageKey = 'Email format is invalid'): ValidatorFn  {
        return (control: FormControl) => {
            const temp = <FormControl>{  ...control, value: control.value ? control.value.trim() : null  };
            const result = Validators.email(temp);
            // FINMAEHP-1854: create user with invalid email message
            // FINAMEHP-1945; correct regex expression
            // tslint:disable-next-line:max-line-length
            const regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if (result && result.email) {
                result.customMessageKey = messageKey;
            }
            if (!regex.test(temp.value)) {
                return ControlHelper.buildValidationError('Email is invalid');
            }

            return result;
        };
    }

    static minLength(minLength: number, messageKey: string): ValidatorFn {
        return (control: FormControl) => {
            const func = Validators.minLength(minLength).bind(this);
            const result = func(control);
            if (result && result['minlength']) {
                result.customMessageKey = messageKey;
            }
            return result;
        };
    }

    static maxLength(maxLength: number, messageKey: string): ValidatorFn {
        return (control: FormControl) => {
            const func = Validators.maxLength(maxLength).bind(this);
            const result = func(control);
            if (result && result['maxlength']) {
                result.customMessageKey = messageKey;
            }
            return result;
        };
    }

    static maxLengthText(maxLength: number, messageKey?: string): ValidatorFn {
        return (control: FormControl) => {
            const func = Validators.maxLength(maxLength).bind(this);
            const result = func(control);
            if (result && result['maxlength']) {
                result.customMessageKey = messageKey ? messageKey : 'The maximum number of characters is {{requiredLength}}.';
            }
            return result;
        };
    }

    static modelValidator(func: (model: any) => {}): ValidatorFn {
        return (control: FormControl) => {
            const model = {};
            for (const formControl in control.parent.controls) {
                if (control.parent.controls[formControl].value) {
                    model[formControl] = control.parent.controls[formControl].value;
                }
            }
            return func(model);
        };
    }

    static fileValidator(): ValidatorFn {
        return (control: FormControl) => {
            const value = control.value;
            if (Utils.isDefinedAndNotNull(value)
                && Utils.isDefinedAndNotNull(value[0].validationErrors) && value[0].validationErrors.length > 0) {
                return ControlHelper.buildValidationError(`common.validation.${value[0].validationErrors[0]}`);
            }
        };
    }

    static receiptValidator(): ValidatorFn {
        return (control: FormControl) => {
            const value = control.value;
            if (Utils.isDefinedAndNotNull(value)
                && Utils.isDefinedAndNotNull(value[0].validationErrors) && value[0].validationErrors.length > 0) {
                if (value[0].validationErrors[0] === 'invalidFileExtension') {
                    return ControlHelper.buildValidationError(`common.validation.invalidReceiptExtension`);
                }
                return ControlHelper.buildValidationError(`common.validation.${value[0].validationErrors[0]}`);
            }
        };
    }
    static customReceiptValidator(): ValidatorFn {
        return (control: FormControl) => {
            const value = control.value;
            if (Utils.isDefinedAndNotNull(value)
                && Utils.isDefinedAndNotNull(value[0].validationErrors) && value[0].validationErrors.length > 0) {
                if (value[0].validationErrors[0] === 'invalidFileExtension') {
                    return ControlHelper.buildValidationError(`common.validation.customInvalidReceiptExtension`);
                }
                return ControlHelper.buildValidationError(`common.validation.${value[0].validationErrors[0]}`);
            }
        };
    }

    static atLeastOneSelected(props: Array<string>, messageKey: string): ValidatorFn {
        return (control: FormControl) => {
            const value = control.value;
            let hasError = false;
            const validate = function (item) {
                if (!item) {
                    return;
                }
                let n = 0;
                props.forEach((p) => {
                    if (item[p] === true) {
                        n++;
                    }
                });
                if (n === 0) {
                    hasError = true;
                }
            };
            if (isArray(value)) {
                value.forEach((v) => {
                    if (!hasError) {
                        validate(v);
                    }
                });
            } else {
                validate(value);
            }
            if (hasError) {
                return ControlHelper.buildValidationError(messageKey);
            }
            return null;
        };
    }

}
