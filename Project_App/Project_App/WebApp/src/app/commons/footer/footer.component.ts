
import {Component, Input, OnDestroy, ChangeDetectionStrategy, ChangeDetectorRef} from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../authentications/auth.service';

@Component({
    selector: 'app-footer',
    templateUrl: 'footer.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class FooterComponent implements OnDestroy {
    langs: { Id: string, Text: string }[];
    currentLanguage: string;
    key: string;

    public value = 42;

    constructor(private userService: AuthService, private cdr: ChangeDetectorRef) {
        // window['EHPIntlService'] = this.intlService;
        // this.langs = translate.getLangs().map(i => {
        //     return {
        //         Id: i,
        //         Text: CommonEnumMapper.LanguagesMap(i)
        //     };
        // });
        // this.currentLanguage = translate.getDefaultLang();
        // this.sub = this.kendoLocaleService.onChangeLanguages.subscribe((lang) => {
        //     this.translate.use(lang);
        //     this.currentLanguage = lang;
        //     this.intlService['localeId'] = lang;
        //     this.kendoLocaleService.isChangeLanguages = false;
        //     setTimeout(() => {
        //         this.kendoLocaleService.isChangeLanguages = true;
        //     }, 100);
        //     this.cdr.markForCheck();
        // });
        // this.getUserSettingLanguage();
    }

    getUserSettingLanguage() {

    }

    ngOnDestroy() {

    }

    languageChanged(lang: string): void {

    }
}


