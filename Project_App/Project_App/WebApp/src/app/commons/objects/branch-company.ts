export interface BranchCompany {
  Id: number;
  CountryName: string;
  City: string;
  EstablishDate: Date;
  Address: string;
}
