export class SingleSurveyDetailVO {
  Id: number;
  SalaryValues: SalaryValueVO[];
}


export class SalaryValueVO {
  SalaryId: number;
  SalaryValue: number;
}
