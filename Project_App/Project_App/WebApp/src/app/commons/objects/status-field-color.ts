export enum ValueFieldColor {
  Gray,
  Green,
  Black,
  Golden,
  Purple,
  Orange,
  Red,
  Blue,
  LightGreen,
  LightBlue
}
