import { DirectorRole } from './director';
import { FormControlDecorator } from '../shared/form/form-control-decorator';

import { Validators } from '@angular/forms';

import { CustomValidator } from '../validators-custom/custom-validator';
import { BranchCompany } from './branch-company';

export class UserDto {
    Id: number = undefined;
    FirstName: string = undefined;
    LastName: string = undefined;
    Email: string = undefined;
    Password: string = undefined;
    IsLogined: boolean = undefined;
    LoginFailMessage: string = undefined;
    Token: string = undefined;
    Role: DirectorRole = undefined;
    CountryName: string = undefined;
    BranchCompany?: BranchCompany = undefined;
}


export const UserLoginRules = {
    EmailRule: FormControlDecorator.create('', [{email: CustomValidator.Email('Email format is invalid'), required: Validators.required },
        CustomValidator.maxLengthText(20)]),
    PasswordRule: FormControlDecorator.create('', [{ required: Validators.required }, CustomValidator.maxLengthText(9)])
};
