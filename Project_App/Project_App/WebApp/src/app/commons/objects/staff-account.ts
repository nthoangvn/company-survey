export class StaffAccount {
  Id: number = undefined;
  FirstName: number;
  LastName: number;
  StaffVisa: number;
  Phone: string;
  Activated: boolean;
  StartDate: Date;
  LeaveDate: Date;
  DepartmentName: string;
  Position: string;
  DepartmentId: number;
}
