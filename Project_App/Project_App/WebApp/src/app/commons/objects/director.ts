import { BranchCompany } from './branch-company';
export interface Director {
    Id: number;
    FirstName: string;
    LastName: string;
    Email: string;
    Phone: string;
    BranchCompany?: BranchCompany;
    Role: DirectorRole;
    IsActive: boolean;
}


export enum DirectorRole {
    GeneralDirector,
    BranchDirector
}
