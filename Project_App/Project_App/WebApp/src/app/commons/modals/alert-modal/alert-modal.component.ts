
import { OnInit } from '@angular/core';
import { ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { DialogRef, ModalComponent } from 'ngx-modialog';
import { Component, ChangeDetectionStrategy, HostListener } from '@angular/core';

import { BSModalContext } from 'ngx-modialog/plugins/bootstrap';
import { CommonConstants } from '../../constants/common-constants';
import { Utils } from '../../helper/utils';



export class AlertModalContext extends BSModalContext {

  constructor(public message, public splitErrorCode = false,
    public btnOk = 'common.popup.closeLabel', public title = 'common.popup.errorTitle',
    public useResource = false,
    public messageParams = []) {
    super();
    this.dialogClass = 'modal-dialog confirm-modal';
  }
}

@Component({
  selector: 'app-alert-modal',
  templateUrl: './alert-modal.component.html',
  styleUrls: ['./alert-modal.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AlertModalComponent implements AfterViewInit, OnInit, ModalComponent<AlertModalContext> {

  message: string;
  @ViewChild('ok') ok: ElementRef;

  context: AlertModalContext;

  constructor(public dialog: DialogRef<AlertModalContext>) {
    this.context = dialog.context;
  }

  ngOnInit() {
    if (this.context.useResource) {
      this.buildContent();
    } else if (this.context.splitErrorCode) {
      this.message = this.context.message.split(CommonConstants.LogMessageSeperator)[1];
    } else {
      this.message = this.context.message;
    }
  }

  ngAfterViewInit() {
    this.ok.nativeElement.focus();
  }
  buildContent(): void {
    if (this.context.splitErrorCode) {
      this.context.message = this.context.message.split(CommonConstants.LogMessageSeperator)[1];
    }
    this.message = Utils.stringFormat(this.context.message, this.context.messageParams);
  }

  btnOkClick() {
    this.dialog.close();
  }

  @HostListener('window:popstate', ['$event'])
  onPopState(event) {
    this.dialog.dismiss();
  }
}
