import { Injectable } from '@angular/core';
import { BackendHttpService } from '../services/http.service';
import { Observable } from 'rxjs/internal/Observable';
import { UserDto } from '../objects/userDto';
import { User } from '../objects/user';
import { BehaviorSubject } from 'rxjs';
import { RequestOptions } from '@angular/http';
import { Director } from '../objects/director';
import { BranchCompany } from '../objects/branch-company';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private user: Director = null;
  public _loginedUser: UserDto = JSON.parse(localStorage.getItem('currentUser'));
  public currentUserId = JSON.parse(localStorage.getItem('currentUserId'));
  public currentUser = new BehaviorSubject<Director>(this.user);
  constructor(private backendHttpService: BackendHttpService) {
    this.currentUser.asObservable();
    if (this.currentUserId) {
      // this.getUser(this.currentUserId).then((user) => {
      //   if (user) {
      //     this.user = user;
      //     this.currentUser.next(this.user);
      //   }
      // });
    }
  }

  get loggedInUser(): UserDto {
    return this._loginedUser;
  }
  set loggedInUser(user: UserDto) {
    if (user) {
      this._loginedUser = user;
    }
  }
  getUser(id: number): Promise<Director> {
    return this.backendHttpService.get<Director>(`/api/auth/${id}/me`);
  }

  LoginToAuthenticate(user: UserDto): Observable<any> {
    return this.backendHttpService.post<any>('api/auth/authenticate', user);
  }

  getAllBranchCompany(): Promise<BranchCompany[]> {
    return this.backendHttpService.get<BranchCompany[]>(`/api/auth/branches`);
  }

  getBranchCompanyInfo(branchId: number): Promise<BranchCompany> {
    return this.backendHttpService.get<BranchCompany>(`/api/auth/branches/${branchId}`);
  }

  getBranchCompanyByDirectorId(directorId: number): Promise<BranchCompany> {
    return this.backendHttpService.get<BranchCompany>(`/api/auth/branches/user/${directorId}`);
  }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser');
  }

  // private jwt() {
  //   // create authorization header with jwt token
  //   const currentUser = JSON.parse(localStorage.getItem('currentUser'));
  //   if (currentUser && currentUser.token) {
  //       const headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.token });
  //       return new RequestOptions({ headers: headers });
  //   }
}

  // getAnderungGesamtumfragen(mkpId: number, kontoIds: number[]): Observable<AnderungGesamtumfrage[]> {
  //   const paramsVO = { mkpId: mkpId, kontoIds: kontoIds };
  //   return this.backendHttpService.post(`api/gesamtumfragen/getAnderungGesamtumfragen`, paramsVO);
  // }

