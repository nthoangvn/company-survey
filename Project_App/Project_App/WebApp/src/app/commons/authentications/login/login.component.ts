import { DirectorRole } from './../../objects/director';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { BaseFormComponent } from '../../shared/form/baseForm.component';
import { UserDto, UserLoginRules } from '../../objects/userDto';
import { User } from '../../objects/user';
import { FormBuilder } from '@angular/forms';
import { Modal } from 'ngx-modialog/plugins/bootstrap';
import { ToastProvider } from '../../services/toast.provider';
import { Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertModalComponent, AlertModalContext } from '../../modals/alert-modal/alert-modal.component';
import { overlayConfigFactory } from 'ngx-modialog';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginComponent extends BaseFormComponent<UserDto> implements OnInit {
  userName = '12312';
  password = '122143213';
  userLogin: UserDto = <UserDto>{};
  busy: any;
  constructor(protected service: AuthService, protected formBuilder: FormBuilder, public modal: Modal,
    protected location: Location, protected toastProvider: ToastProvider, public router: Router, private route: ActivatedRoute) {
      super(formBuilder, modal, location, toastProvider);
     }

  ngOnInit() {
    localStorage.removeItem('currentUser');
    localStorage.removeItem('currentUserId');
    localStorage.removeItem('loggedInUser');
    this.rules = UserLoginRules;
    this.originalData = this.userLogin;
    super.ngOnInit();
  }

  protected submitSuccess() {
    const userDto: UserDto = <UserDto>{};
    userDto.Email = this.data.Email;
    userDto.Password = this.data.Password;
    userDto.IsLogined = false;
    this.busy = this.service.LoginToAuthenticate(userDto).subscribe((response) => {
      this.busy = true;
      const user = response;
      if (user && user.Token) {
        localStorage.setItem('currentUser', JSON.stringify(user));
        this.service.loggedInUser = user;
      }

      if (user.Role === DirectorRole.GeneralDirector) {
        this.router.navigate(['../total-survey-list'], { relativeTo: this.route });
      } else {
        this.router.navigate(['../single-survey-list'], { relativeTo: this.route });
      }
    }, error => {
      console.log(error);
      this.modal.open(AlertModalComponent, overlayConfigFactory(new AlertModalContext(
        'Login Failed'), AlertModalContext)
      );

    });
  }

}
