export class CommonConstants {

    static Comma = ',';

    static DefaultDateTimeFormat = 'dd.MM.yyyy HH:mm';
    static DefaultDateFormat = 'dd.MM.yyyy';

    // Since format of KendoUI and format of Moment is difference

    static DefaultMomentDateFormat = 'DD.MM.YYYY';
    static DefaultMomentDateTimeFormat = 'DD.MM.YYYY HH:mm';
    static DefaultMomentOnlyDateFormat = 'DD.MM';

    static TimeIncludeSecondFormat = 'DD.MM.YYYY HH:mm:ss';

    static ScrollDuration = 1000;
    static LogMessageSeperator = '#';

    static CookieExpiresValue = 1; // 1 day
}

export enum ValueFieldColor {
    Gray,
    Green,
    Black,
    Golden,
    Purple,
    Orange,
    Red,
    Blue,
    LightGreen,
    LightBlue
}

export enum IKPViewOption {
    Standard,
    MCV,
    AVO
}
