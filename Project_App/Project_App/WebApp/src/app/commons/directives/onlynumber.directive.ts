import { Input, HostListener, Directive } from '@angular/core';
@Directive({
    // tslint:disable-next-line:directive-selector
    selector: '[onlyNumber]'
})
export class OnlyNumberDirective {
    @Input('hasNegative') hasNegative: boolean;
    @Input('hasDecimal') hasDecimal: boolean;
    @HostListener('keydown', ['$event']) onKeyDown(event) {
        const e = <KeyboardEvent>event;

        const key = e.key;
        if (this.checkNumber(key)
            || key === 'Backspace'
            || key === 'Tab'
            || key === 'Enter'
            || key === 'ArrowLeft'
            || key === 'ArrowRight'
            || key === 'Home'
            || key === 'End'
            || key === 'Insert'
            || key === 'Delete'
            || (this.hasNegative && key === '-')
            || (this.hasDecimal && (key === '.' || key === ','))) {
        } else {
            e.returnValue = false;
            if (e.preventDefault) {
                e.preventDefault();
            }
        }
    }

    checkNumber(key) {
        return key === '0' ||
            key === '1' ||
            key === '2' ||
            key === '3' ||
            key === '4' ||
            key === '5' ||
            key === '6' ||
            key === '7' ||
            key === '8' ||
            key === '9';
    }
}
