import { ActivatedRoute, Router } from '@angular/router';
import * as sortBy from 'lodash.sortBy';
import {
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Component,
    EventEmitter,
    Input,
    OnChanges,
    Output,
    SimpleChanges,
    NgZone,
    OnInit,

} from '@angular/core';

@Component({
    selector: 'app-sub-menu',
    templateUrl: 'sub-menu.component.html',
})
export class SubMenuComponent implements OnChanges {
    @Input() menuItems: MenuItem[];
    @Output() openUserSettingModal: EventEmitter<any> = new EventEmitter<any>();

    menuItemType = MenuItemType;

    constructor(protected route: ActivatedRoute, protected router: Router,
        private cdr: ChangeDetectorRef, private ngZone: NgZone) {
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (this.menuItems.length > 0) {
            this.menuItems = sortBy(this.menuItems, ['index']);
        }
    }

    onItemClick(item: MenuItem) {
        switch (item.itemType) {
            case MenuItemType.Router:
                this.router.navigate([item.routerLink], { relativeTo: this.route });
                break;
            case MenuItemType.AbsoluteLink:
                location.href = item.routerLink;
                break;
            case MenuItemType.Action:
                this.actionHandler(item);
                break;
        }
    }

    actionHandler(item: MenuItem) {
        switch (item.rel) {
            case 'user-setting':
                this.openUserSetting();
                break;
        }
    }

    openUserSetting(): void {
        this.openUserSettingModal.emit();
    }


}

export interface MenuItem {
    rel?: string;
    hasSubMenu: boolean;
    displayName: string;
    routerLink: string;
    subMenus?: MenuItem[];
    iconClass: string;
    itemType: MenuItemType;
    children: RadioItem[];
    index?: number;
}

export interface RadioItem {
    checked: boolean;
    displayName: string;
    Id: number | string;
}

export enum MenuItemType {
    Router,
    AbsoluteLink,
    Action
}

