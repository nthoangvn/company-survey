
import {take} from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { Component, AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, OnInit } from '@angular/core';
import { Utils } from '../helper/utils';
import { CommonConstants } from '../constants/common-constants';

@Component({
    selector: 'app-not-found',
    templateUrl: 'notFound.component.html',
    styleUrls: ['./notFound.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class NotFoundComponent implements OnInit {
    mess: string;
    logId: string;
    errorCode: number;
    lang: string;
    closeUrl: string;
    constructor(protected route: ActivatedRoute, private cdr: ChangeDetectorRef) { }

    ngOnInit() {

    }
}
