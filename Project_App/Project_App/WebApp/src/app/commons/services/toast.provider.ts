import { Injectable } from '@angular/core';
import { ToastrService  } from 'ngx-toastr';
import { CommonConstants } from '../constants/common-constants';


@Injectable()
export class ToastProvider {
    constructor(public toastr: ToastrService) {
    }

    showSuccessToast(message: string, parameter?) {
        return this.toastr.success(message);
    }

    showErrorToast(message: string, splitErrorCode = false) {
        message = splitErrorCode ? message.split(CommonConstants.LogMessageSeperator)[1] : message;
        return this.toastr.error(message);
    }

    validationError(message: string = 'Error Form. You Should Fill correctly') {
        const errorMessageParts = message.split(CommonConstants.LogMessageSeperator);
        if (errorMessageParts.length > 1) {
            message = errorMessageParts[1];
        }
        return this.toastr.error(message);
    }

    showManualDismisSuccessToast(message: string, parameter?) {
        return this.toastr.success(message, undefined, { tapToDismiss: true, closeButton: true });
    }
}
