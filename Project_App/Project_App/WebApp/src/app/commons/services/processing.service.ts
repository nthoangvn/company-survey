import { Injectable, EventEmitter } from '@angular/core';

@Injectable()
export class ProcessingService {
    private pendingRequestNum = 0;

    get hasPendingRequest() {
        return this.pendingRequestNum > 0;
    }

    requestStarted() {
        this.pendingRequestNum++;
    }

    requestFinished() {
        this.pendingRequestNum--;
    }
}
