
import {of as observableOf,  Observable ,  Subject } from 'rxjs';

import {mergeMap} from 'rxjs/operators';
import { Router } from '@angular/router';



import { Injectable, Optional } from '@angular/core';
import { Http, Headers, RequestOptionsArgs } from '@angular/http';
declare var kendo: any;

import { UUID } from 'angular2-uuid';

import * as includes from 'lodash.includes';
import { Modal } from 'ngx-modialog/plugins/bootstrap';
import { ProcessingService } from './processing.service';

/**
 * RequestError interface. This information is send to the caller when there are exception from the backend.
 * The generic error handler will only handle the error if the caller does not handle the error (set handled = true).
 */
export interface IRequestError {
    handled?: boolean;
    status: number;
    message: string;
}

/**
 * BackendHttpService interface, defines common operation for a backend service.
 */
export declare interface IBackendHttpService {
    get<T>(url: string | Observable<string>, body?: any): Promise<T>;
    post<T>(url: string | Observable<string>, body: any): Observable<T>;
    put<T>(url: string | Observable<string>, body: any): Observable<T>;
    patch<T>(url: string | Observable<string>, body: any): Observable<T>;
    delete<T>(url: string | Observable<string>): Observable<T>;
}

@Injectable()
/**
 * The generic back http service with default error handler for failed request.
 * Default value for request header can be set via defaultRequestOption property.
 *
 * Usage example : see contact component in demo module.
 */
export class BackendHttpService implements IBackendHttpService {
    constructor(protected processingSvc: ProcessingService,
        protected http: Http, @Optional()
        protected modal: Modal, public router: Router) {

    }

    public delete<T>(url: string | Observable<string>): Observable<T> {
        return this.genericPost<T>('delete', url);
    }

    public put<T>(url: string | Observable<string>, body: any): Observable<T> {
        return this.genericPost<T>('put', url, body);
    }

    public post<T>(url: string | Observable<string>, body: any): Observable<T> {
        return this.genericPost<T>('post', url, body);
    }

    public patch<T>(url: string | Observable<string>, body: any): Observable<T> {
        return this.genericPost<T>('patch', url, body);
    }

    public get<T>(url: string | Observable<string>, body?: any): Promise<T> {
        return (typeof url === 'string' ? observableOf(url) : url).pipe(mergeMap(link => this.getQuery<T>(link, body, false)))
            .toPromise()
            .then(response => response as T);
    }

    public getJson<T>(url: string | Observable<string>, body?: any): Promise<T> {
        return (typeof url === 'string' ? observableOf(url) : url).pipe(mergeMap(link => this.getQuery<T>(link, body, true)))
            .toPromise()
            .then(response => response as T);
    }

    protected get defaultRequestOption(): RequestOptionsArgs {
        const uuid = UUID.UUID();
        document.cookie = 'XSRF-TOKEN=' + uuid + '; path=/';

        return {
            headers: new Headers({
                'Access-Control-Allow-Origin': '*',
                'Content-Type': 'application/json; charset=utf-8',
                'Accept': 'application/hal+json',

            }),
            // uncomment the following to send the authentication cookie with request
            // withCredentials: true
        };
    }

    protected get jsonRequestOption(): RequestOptionsArgs {
        const uuid = UUID.UUID();
        document.cookie = 'XSRF-TOKEN=' + uuid + '; path=/';
        return {
            headers: new Headers({
                'Access-Control-Allow-Origin': '*',
                'Content-Type': 'application/json; charset=utf-8',
            }),
        };
    }

    private dateReviver(key: string, value: any) {
        if (typeof value !== 'string') {
            return value;
        }
        // tslint:disable-next-line:max-line-length
        if (value.match(/(\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d\.\d+([+-][0-2]\d:[0-5]\d|Z))|(\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d([+-][0-2]\d:[0-5]\d|Z))|(\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d([+-][0-2]\d:[0-5]\d|Z))/)) {
            // parse input value as UTC date then convert to local date
            return new Date(value);
        }
        return value;
    }

    private genericPost<E>(targetMethod: 'post' | 'put' | 'delete' | 'patch', url: string | Observable<string>, body?: any): Observable<E> {
        this.processingSvc.requestStarted();
        const subject = new Subject<E>();
        (typeof url === 'string' ? observableOf(url) : url).subscribe(link => {
            (body ? this.http[targetMethod].apply(this.http, [link, body, this.defaultRequestOption])
                : this.http[targetMethod].apply(this.http, [link, this.defaultRequestOption]))
                .subscribe(
                    (response: any) => {
                        if (this.checkResponseStatus(response, this.defaultRequestOption)) {
                            return;
                        }
                        let result: any;
                        if (!!response._body) {
                            result = JSON.parse(response.text(), this.dateReviver);
                        }
                        subject.next(result);
                        subject.complete();
                    },
                    (error: any) => {
                        this.processingSvc.requestFinished();
                    },
                    this.onHttpComplete.bind(this)
                );
        });
        return subject.asObservable();
    }

    private getQuery<E>(url: string, body?: any, getJson?: Boolean): Promise<E> {
        this.processingSvc.requestStarted();
        const queryString = !!body ? Object.keys(body).reduce(
            (previousValue, currentValue, currentIndex) => {
                return currentIndex === 1 ? '?' + previousValue + '=' + body[previousValue] + '&' + currentValue + '=' + body[currentValue]
                    : previousValue + '&' + currentValue + '=' + body[currentValue];
            }) : '';
        return this.http.get(url + queryString, getJson ? this.jsonRequestOption : this.defaultRequestOption)
            .toPromise()
            .then((response: any) => {
                if (this.checkResponseStatus(response, getJson ? this.jsonRequestOption : this.defaultRequestOption)) {
                    return;
                }
                this.processingSvc.requestFinished();
                if (!response._body) {
                    return response;
                }

                return JSON.parse(response.text(), this.dateReviver) as E;
            }, (resaon) => {
                this.processingSvc.requestFinished();
            });
    }

    private onHttpComplete() {
        this.processingSvc.requestFinished();
    }
    checkResponseStatus(response: any, opt: RequestOptionsArgs) {
        if (response.headers && includes(response.headers.get('content-type'), 'text/html;')
            && (includes(opt.headers.get('content-type'), 'application/json;')
                || includes(opt.headers.get('content-type'), 'application/hal+json;'))) {
            this.router.navigate(['/error-page/303']);
            return true;
        }
        return false;
    }
}


export function parseError(error: any): IRequestError {
    try {
        const se: IRequestError = {
            status: error.status,
            handled: false,
            message: error._body
        };
        return se;
    } catch (ex) {
        // provide UI feedback here (e.g dialog...etc)
        return { handled: false, status: error.status, message: error._body };
    }
}

export function handleError(serverError: IRequestError, modal: Modal, router: Router) {
    if (!serverError.handled) {
        if (serverError.status >= 500 && serverError.status !== 502) { // Unrecoverable error
            // if (modal) {
            //     modal.alert()
            //         .dialogClass('modal-dialog error-dialog')
            //         .title('Error dialog')
            //         .body(`An unexpected error occured on the server.\n
            //         You should refresh the page and contact support if the error persists.`)
            //         .open();
            // }
            if (window.location.pathname !== '/error-page') {
                router.navigate(['/error-page']);
            }
            return;
        }

        switch (serverError.status) {
            case 401: // Unauthorized
                // This should only happen when session timeout occured. Hence, we will treat it this way.
                if (window.location.pathname !== '/error-page') {
                    router.navigate(['/error-page']);
                }
                break;
            case 403: // Forbidden
                // This should only happen when session timeout occured. Hence, we will treat it this way.
                if (window.location.pathname !== '/error-page') {
                    router.navigate(['/error-page']);
                }
                break;

            case 406:
            case 502:
                // exception will handle in each component
                break;
            case 400: // Bad request
                if (modal) {
                    modal.alert()
                        .dialogClass('modal-dialog error-dialog')
                        .title('Error dialog')
                        .body('The data could not be saved (Http 400).')
                        .open();
                }
                break;
            case 409: // Conflict
                if (modal) {
                    modal.alert()
                        .dialogClass('modal-dialog error-dialog')
                        .title('Error dialog')
                        .body('Error')
                        .open()
                        .result
                        .then(() => {
                            window.location.reload();
                        });
                }
                break;
            default:
                if (window.location.pathname !== '/error-page') {
                    router.navigate(['/error-page']);
                }
                break;
        }
    } else {
        // Caller already handle the error, just log it here.
        if (window.location.pathname !== '/error-page') {
            router.navigate(['/error-page']);
        }
    }
}
