import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { TotalSurveyNotes } from '../commons/surveys/total-survey';
import { TotalSurveyStatus } from '../commons/surveys/total-survey-status';
import { Utils } from '../commons/helper/utils';

@Component({
  selector: 'app-survey-status-notification',
  templateUrl: './survey-status-notification.component.html',
  styleUrls: ['./survey-status-notification.component.less']
})
export class SurveyStatusNotificationComponent implements OnChanges {
  @Input() Notes: TotalSurveyNotes[] = [];
  TotalSurveyStatus: typeof TotalSurveyStatus = TotalSurveyStatus;
  status = {
    Created: TotalSurveyStatus.Created,
    Published: TotalSurveyStatus.Published,
    Validated: TotalSurveyStatus.Validated,
    Completed: TotalSurveyStatus.Completed
  };
  constructor() { }

  ngOnChanges(changes: SimpleChanges) {
    console.log(changes);
    if (!Utils.isNullOrUnDefined(this.Notes)) {
      console.log(changes.currentValue);
    }
  }
}
