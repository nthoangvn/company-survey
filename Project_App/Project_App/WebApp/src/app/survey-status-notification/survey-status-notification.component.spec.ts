import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SurveyStatusNotificationComponent } from './survey-status-notification.component';

describe('SurveyStatusNotificationComponent', () => {
  let component: SurveyStatusNotificationComponent;
  let fixture: ComponentFixture<SurveyStatusNotificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SurveyStatusNotificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SurveyStatusNotificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
