import { Location } from '@angular/common';
import { Department } from './../../commons/user/department';
import { Component, OnInit } from '@angular/core';
import { Staff } from 'src/app/commons/user/staff';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from 'src/app/user.service';
import { AuthService } from 'src/app/commons/authentications/auth.service';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { ToastProvider } from 'src/app/commons/services/toast.provider';
import { Modal, overlayConfigFactory } from 'ngx-modialog';
import { AlertModalComponent, AlertModalContext } from 'src/app/commons/modals/alert-modal/alert-modal.component';

@Component({
  selector: 'app-create-staff',
  templateUrl: './create-staff.component.html',
  styleUrls: ['./create-staff.component.css']
})
export class CreateStaffComponent implements OnInit {

  createStaffAccountForm: FormGroup;
  staffAccount: Staff = new Staff();
  departments: Department[];
  selectedDepartment: Department;

  constructor(private route: ActivatedRoute,
    public router: Router,
    private userService: UserService,
    private authService: AuthService,
    private formBuilder: FormBuilder,
    protected toastProvider: ToastProvider,
    public modal: Modal,
    private _location: Location) { }

  ngOnInit() {
    const branchId = +this.route.snapshot.paramMap.get('branchId');
    this.staffAccount.DepartmentId = branchId;
    this.createStaffAccountForm = this.formBuilder.group({
      firstName: [''],
      lastName: [''],
      staffVisa: [''],
      phone: [''],
      position: [''],
      department: [''],
    });
    this.userService.getDepartmentByBranchId(branchId).then(
      res => {
        this.departments = res;
        console.log('Get departments res: ', this.departments);
      }
    );
  }

  onSubmit() {
    this.staffAccount.DepartmentId = this.selectedDepartment.Id;
    this.staffAccount.Activated = true;
    this.staffAccount.StartDate = new Date();
    console.log('On add staff account submit', this.staffAccount);
    this.userService.createStaffAccount(this.staffAccount).subscribe(
      (response) => {
        console.log('Add Staff Account res: ', response);
        if (response) {
          this.toastProvider.showSuccessToast('Add Staff Account Successfully!');
          this._location.back();
        } else {
          this.toastProvider.showErrorToast('Add Staff Account Failed!');
        }
      }, error => {
        console.log('Add Staff Account error: ', error);
        this.toastProvider.showErrorToast('Add Staff Account Failed!');
      });
  }
}
