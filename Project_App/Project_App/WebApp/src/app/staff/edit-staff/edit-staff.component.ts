import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from 'src/app/user.service';
import { AuthService } from 'src/app/commons/authentications/auth.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ToastProvider } from 'src/app/commons/services/toast.provider';
import { Modal } from 'ngx-modialog';
import { Staff } from 'src/app/commons/user/staff';
import { Department } from 'src/app/commons/user/department';

@Component({
  selector: 'app-edit-staff',
  templateUrl: './edit-staff.component.html',
  styleUrls: ['./edit-staff.component.css']
})
export class EditStaffComponent implements OnInit {

  editStaffAccountForm: FormGroup;
  staffAccount: Staff = new Staff();
  departments: Department[];
  selectedDepartment: Department;

  constructor(private route: ActivatedRoute,
    public router: Router,
    private userService: UserService,
    private authService: AuthService,
    private formBuilder: FormBuilder,
    protected toastProvider: ToastProvider,
    public modal: Modal,
    private _location: Location) { }

  ngOnInit() {
    const branchId = +this.route.snapshot.paramMap.get('branchId');
    const staffAccountId = +this.route.snapshot.paramMap.get('staffAccountId');
    this.editStaffAccountForm = this.formBuilder.group({
      firstName: [''],
      lastName: [''],
      staffVisa: [''],
      phone: [''],
      position: [''],
      department: [''],
    });

    this.userService.getStaffAccount(staffAccountId).then(
      res => {
        this.staffAccount = res;
        console.log('Get staff account res: ', this.staffAccount);
        this.getDepartment(branchId);
      }
    );
  }

  getDepartment(branchId: number) {
    this.userService.getDepartmentByBranchId(branchId).then(
      res => {
        this.departments = res;
        console.log('Get departments res: ', this.departments);
        for (let i = 0; i < this.departments.length; i++) {
          var department = this.departments[i];
          if (department.Id == this.staffAccount.DepartmentId) {
            this.selectedDepartment = department;
            break;
          }
        }
      }
    );
  }

  onSubmit() {
    this.staffAccount.DepartmentId = this.selectedDepartment.Id;
    // this.staffAccount.Activated = true;
    // this.staffAccount.StartDate = new Date();
    console.log('On update staff account submit', this.staffAccount);
    this.userService.updateStaffAccount(this.staffAccount).subscribe(
      (response) => {
        console.log('Update Staff Account res: ', response);
        if (response) {
          this.toastProvider.showSuccessToast('Update Staff Account Successfully!');
          this._location.back();
        } else {
          this.toastProvider.showErrorToast('Update Staff Account Failed!');
        }
      }, error => {
        console.log('Update Staff Account error: ', error);
        this.toastProvider.showErrorToast('Update Staff Account Failed!');
      });
  }
}
