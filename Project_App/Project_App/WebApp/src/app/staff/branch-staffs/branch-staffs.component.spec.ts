import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BranchStaffsComponent } from './branch-staffs.component';

describe('BranchStaffsComponent', () => {
  let component: BranchStaffsComponent;
  let fixture: ComponentFixture<BranchStaffsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BranchStaffsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BranchStaffsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
