import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from 'src/app/user.service';
import { AuthService } from 'src/app/commons/authentications/auth.service';
import { BranchCompany } from 'src/app/commons/objects/branch-company';
import { Staff } from 'src/app/commons/user/staff';
import { ConfirmationService, Message } from 'primeng/api';
import { ToastProvider } from 'src/app/commons/services/toast.provider';

@Component({
  selector: 'app-branch-staffs',
  templateUrl: './branch-staffs.component.html',
  styleUrls: ['./branch-staffs.component.css']
})
export class BranchStaffsComponent implements OnInit {

  constructor(private router: Router,
    private route: ActivatedRoute,
    private toastProvider: ToastProvider,
    private userService: UserService,
    private authService: AuthService,
    private confirmationService: ConfirmationService) 
    { }

    msgs: Message[] = [];
    branchCompany: BranchCompany;
    users: Staff[];
    selectedStaff: Staff;

  ngOnInit() {
    this.getBranchCompanyInfo();
    this.getBranchStaffs();
  }

  getBranchCompanyInfo(): any {
    const branchId = +this.route.snapshot.paramMap.get('branchId');
    this.authService.getBranchCompanyInfo(branchId).then(
      res => {
        this.branchCompany = res;
        console.log('Get branch company detail res: ', this.branchCompany);
      }
    );
  }

  getBranchStaffs(): any {
    const branchId = +this.route.snapshot.paramMap.get('branchId');
    this.userService.getStaffsByBranchCompanyId(branchId).then(
      res => {
        this.users = res;
        console.log('Get user detail res: ', this.users);
      }
    );
  }

  onSort() {
  }

  createStaff() {
    console.log('Create staff clicked');
    const branchId = +this.route.snapshot.paramMap.get('branchId');
    var addStaffsUrl = '/branch-staffs/' + branchId + '/add';
        console.log('Add branch staffs URL: ', addStaffsUrl);
        this.router.navigateByUrl(addStaffsUrl);
  }

  deleteStaff(staffId: number) {
    this.userService.deleteStaffAccount(staffId)
      .subscribe(
      () => { // Success
          console.log('Delete Staff Account success');
          this.toastProvider.showSuccessToast('Delete Staff Account Successfully!');
          // Reload staff account list
          this.getBranchStaffs();
        }
    );
  }

  displayDialog: boolean;
  showDeleteConfirm(event: Event, staff: Staff) {
    this.selectedStaff = staff;
    this.displayDialog = true;
    event.preventDefault();
    this.confirm();
  }

  confirm() {
    this.confirmationService.confirm({
      message: 'Do you want to delete this staff account?',
      header: 'Delete Confirmation',
      icon: 'pi pi-info-circle',
      accept: () => {
          this.msgs = [{severity:'info', summary:'Confirmed', detail:'Staff account deleted'}];
          this.deleteStaff(this.selectedStaff.Id);
      },
      reject: () => {
          this.msgs = [{severity:'info', summary:'Rejected', detail:'You have rejected'}];
      }
  });
  }

  editStaff(event: Event, staff: Staff) {
    const branchId = +this.route.snapshot.paramMap.get('branchId');
    console.log('Edit Staff Account: ', staff);
    var editStaffsUrl = '/branch-staffs/' + branchId + '/edit/' + staff.Id;
        console.log('Edit branch staffs URL: ', editStaffsUrl);
        this.router.navigateByUrl(editStaffsUrl);
  }
}
