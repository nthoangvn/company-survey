import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastProvider } from 'src/app/commons/services/toast.provider';
import { TotalSurveysService } from '../total-surveys.service';
import { ConfirmationService } from 'primeng/api';
import { SingleSurveyListOfSurvey } from 'src/app/commons/surveys/singlesurvey-of-survey';
import { SingleSurvey } from 'src/app/commons/surveys/single-survey';
import { SingleSurveyStatus } from 'src/app/commons/surveys/single-survey-status';
import { TotalSurveyStatus } from 'src/app/commons/surveys/total-survey-status';
import { overlayConfigFactory , Modal} from 'ngx-modialog';
import { AlertModalComponent, AlertModalContext } from 'src/app/commons/modals/alert-modal/alert-modal.component';

@Component({
  selector: 'app-singlesurvey-list-of-survey',
  templateUrl: './singlesurvey-list-of-survey.component.html',
  styleUrls: ['./singlesurvey-list-of-survey.component.less']
})
export class SinglesurveyListOfSurveyComponent implements OnInit {
  totalSurveyId: number;
  busy: any;
  cols: any[];
  displayDialog: boolean;
  data: SingleSurveyListOfSurvey = <SingleSurveyListOfSurvey>{};
  singleSurveys: SingleSurvey[];
  rowGroupMetadata: any;
  SingleSurveyStatus: typeof SingleSurveyStatus = SingleSurveyStatus;

  TotalSurveyStatus: typeof TotalSurveyStatus = TotalSurveyStatus;

  singleStatus = {
    Open: SingleSurveyStatus.Open,
    Appending: SingleSurveyStatus.Appending,
    Submitted: SingleSurveyStatus.Submitted,
    Correcting: SingleSurveyStatus.Correcting,
    Closed: SingleSurveyStatus.Closed
  };

  totalStatus = {
    Created: TotalSurveyStatus.Created,
    Published: TotalSurveyStatus.Published,
    Validated: TotalSurveyStatus.Validated,
    Completed: TotalSurveyStatus.Completed
  };
  constructor(private router: Router,
    private route: ActivatedRoute,
    private toastProvider: ToastProvider,
    private totalSurveysService: TotalSurveysService,
    public modal: Modal,
    private confirmationService: ConfirmationService) { }

  ngOnInit() {
    this.cols = [
      { field: 'FirstName', header: 'First Name'},
      { field: 'LastName', header: 'Last Name'},
      { field: 'StaffVisa', header: 'Visa' },
      { field: 'StartDate', header: 'Start Date' },
      { field: 'Position', header: 'Role'},
      { field: 'SalaryValue', header: 'Salary Value'}
    ];
    this.route.params.subscribe( param => {
      this.totalSurveyId = +param['id'];
      this.busy = this.totalSurveysService.getSingleSurveyListOfSurvey(this.totalSurveyId).then(response => {
        this.data = response;
        this.singleSurveys = response.SingleSurveyList;
        this.data.CreatedDate = new Date(this.data.CreatedDate);
        this.data.PublishDeadline = new Date(this.data.PublishDeadline);
        this.data.SubmitDeadline = new Date(this.data.SubmitDeadline);
        this.data.ValidateDeadline = new Date(this.data.ValidateDeadline);
        this.data.CloseDate = new Date(this.data.ValidateDeadline);
        this.updateRowGroupMetaData();
        // this.notes = this.data.Notes;
      });
    });
  }

  updateRowGroupMetaData() {
    this.rowGroupMetadata = {};
    if (this.singleSurveys) {
        for (let i = 0; i < this.singleSurveys.length; i++) {
            const rowData = this.singleSurveys[i];
            const year = rowData.SurveyYear;
            console.log('Survey year: ', year);
            if (i === 0) {
                this.rowGroupMetadata[rowData.SurveyYear] = { index: 0, size: 1 };
            } else {
                const previousRowData = this.singleSurveys[i - 1];
                const previousYear = previousRowData.SurveyYear;
                if (year === previousYear) {
                  this.rowGroupMetadata[rowData.SurveyYear].size++;
                } else {
                  this.rowGroupMetadata[rowData.SurveyYear] = { index: i, size: 1 };
                }
            }
        }
    }
  }

  onRowSelect(event, singleSurvey) {
    // tslint:disable-next-line:max-line-length
    if (singleSurvey.Status === SingleSurveyStatus.Open || singleSurvey.Status === SingleSurveyStatus.Correcting || singleSurvey.Status === SingleSurveyStatus.Appending) {
      this.displayDialog = true;
      event.preventDefault();
      this.confirmationService.confirm({
        message: 'Please wait for the Single Survey has status Submiited or Closed',
        header: 'Warning',
        accept: () => {
        }
      });
    } else {
      this.router.navigate([`../single-survey/${singleSurvey.Id}/detail`], { relativeTo: this.route });
    }
  }

  goBack() {
    this.router.navigate([`../../../total-survey-list`], { relativeTo: this.route });
  }

}
