import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SinglesurveyListOfSurveyComponent } from './singlesurvey-list-of-survey.component';

describe('SinglesurveyListOfSurveyComponent', () => {
  let component: SinglesurveyListOfSurveyComponent;
  let fixture: ComponentFixture<SinglesurveyListOfSurveyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SinglesurveyListOfSurveyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SinglesurveyListOfSurveyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
