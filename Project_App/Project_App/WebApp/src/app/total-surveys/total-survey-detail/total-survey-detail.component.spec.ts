import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TotalSurveyDetailComponent } from './total-survey-detail.component';

describe('TotalSurveyDetailComponent', () => {
  let component: TotalSurveyDetailComponent;
  let fixture: ComponentFixture<TotalSurveyDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TotalSurveyDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TotalSurveyDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
