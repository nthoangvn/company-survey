import { TestBed } from '@angular/core/testing';

import { TotalSurveysService } from './total-surveys.service';

describe('TotalSurveysService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TotalSurveysService = TestBed.get(TotalSurveysService);
    expect(service).toBeTruthy();
  });
});
