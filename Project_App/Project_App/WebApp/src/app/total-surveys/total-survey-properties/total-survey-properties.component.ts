import { Component, OnInit } from '@angular/core';
import { TotalSurveyDetail, TotalSurveyNotes } from 'src/app/commons/surveys/total-survey';
import { BranchCompany } from 'src/app/commons/objects/branch-company';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SurveyYear } from 'src/app/create-survey/create-a-survey/create-a-survey.component';
import { AuthService } from 'src/app/commons/authentications/auth.service';
import { ToastProvider } from 'src/app/commons/services/toast.provider';
import { TotalSurveysService } from '../total-surveys.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Modal, overlayConfigFactory } from 'ngx-modialog';
import { differenceWith, isEqual } from 'lodash';
import { Utils } from 'src/app/commons/helper/utils';
import { TotalSurveyDto } from 'src/app/commons/surveys/total-survey-dto';
import { AlertModalComponent, AlertModalContext } from 'src/app/commons/modals/alert-modal/alert-modal.component';

@Component({
  selector: 'app-total-survey-properties',
  templateUrl: './total-survey-properties.component.html',
  styleUrls: ['./total-survey-properties.component.less']
})
export class TotalSurveyPropertiesComponent implements OnInit {
  busy: any;
  isLinear = false;
  totalSurveyId: number;
  data: TotalSurveyDetail = <TotalSurveyDetail>{};
  branchSourceList: BranchCompany[] = [];
  branchTargetList: BranchCompany[] = [];
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  notes: TotalSurveyNotes[];
  readOnlyList: {Left: string, Right: string}[] = [];
  surveyYearList: SurveyYear[] = [
    {value: 2017, viewValue: 2017},
    {value: 2018, viewValue: 2018},
    {value: 2019, viewValue: 2019}
  ];
  constructor(private _formBuilder: FormBuilder, protected userService: AuthService,
    protected toastProvider: ToastProvider, public totalSurveysService: TotalSurveysService,
    public router: Router, private route: ActivatedRoute, public modal: Modal) {}

  ngOnInit() {
    this.route.params.subscribe( param => {
      this.totalSurveyId = +param['id'];
      this.busy = this.totalSurveysService.getTotalSurveyDetail(this.totalSurveyId).then(response => {
        this.data = response;
        this.branchTargetList = this.data.BranchCompanyList;
        this.data.PublishDeadline = new Date(this.data.PublishDeadline);
        this.data.SubmitDeadline = new Date(this.data.SubmitDeadline);
        this.data.ValidateDeadline = new Date(this.data.ValidateDeadline);
        this.notes = this.data.Notes;
        this.buildReadOnly();
      });
    });
  }

  buildReadOnly() {

  }

  save() {
    if (this.branchTargetList.length === 0) {
      this.toastProvider.showErrorToast('Please choose Branch Company for Survey');
      return;
    } else if (Utils.isNullOrUnDefined(this.data.PublishDeadline)
          || Utils.isNullOrUnDefined(this.data.SubmitDeadline)
          || Utils.isNullOrUnDefined(this.data.ValidateDeadline)) {
      this.toastProvider.showErrorToast('Please select full the deadline');
      return;
    } else {
      const currentUserId = JSON.parse(localStorage.getItem('currentUser')).Id;
      this.data.DirectorId = currentUserId;
      const branchCompanyIdList = this.branchTargetList.map(b => b.Id);
      this.data.BranchCompanyIdList = branchCompanyIdList;
      const totalSurveyDto = <TotalSurveyDto>{};
      Object.assign(totalSurveyDto, this.data);
      this.busy = this.totalSurveysService.updateTotalSurvey(this.data.Id, totalSurveyDto).subscribe(
        (response) => {
          if (response) {
            this.toastProvider.showSuccessToast('Create Total Survey Successfully!');
            this.goBack();
          }
        }, error => {
          console.log(error);
          this.modal.open(AlertModalComponent, overlayConfigFactory(new AlertModalContext(
            'Create Survey Failed'), AlertModalContext)
          );
        });
    }
  }

  goBack() {
    this.router.navigate([`../../../total-survey-list`], { relativeTo: this.route });
  }

  moveToTarget(event) {
    console.log(event);
    console.log('target', this.branchTargetList);
  }

  moveAllToTarget(event) {
    console.log(event);
  }

  moveAllToSource(event) {
    console.log(event);
  }

  moveToSource(event) {
    console.log(event);
    console.log('target', this.branchTargetList);
  }
}
