import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TotalSurveyPropertiesComponent } from './total-survey-properties.component';

describe('TotalSurveyPropertiesComponent', () => {
  let component: TotalSurveyPropertiesComponent;
  let fixture: ComponentFixture<TotalSurveyPropertiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TotalSurveyPropertiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TotalSurveyPropertiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
