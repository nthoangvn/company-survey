import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TotalSurveysComponent } from './total-surveys.component';

describe('TotalSurveysComponent', () => {
  let component: TotalSurveysComponent;
  let fixture: ComponentFixture<TotalSurveysComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TotalSurveysComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TotalSurveysComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
