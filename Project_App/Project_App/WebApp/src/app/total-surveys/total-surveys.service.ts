import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { BackendHttpService } from '../commons/services/http.service';
import { TotalSurvey, TotalSurveyDetail } from '../commons/surveys/total-survey';
import { TotalSurveyDto } from '../commons/surveys/total-survey-dto';
import { SingleSurveyListOfSurvey } from '../commons/surveys/singlesurvey-of-survey';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class TotalSurveysService {
  private prefixUrl = 'api/totalsurvey';  // URL to web api
  constructor(
    private backendHttpService: BackendHttpService) { }

  /** GET total surveys from the server */
  getTotalSurveys (generalDirectorId: number): Promise<TotalSurvey[]> {
    const fullUrl = this.prefixUrl + '/' + generalDirectorId;
    console.log('Get total survey detail url: ' + fullUrl);
    return this.backendHttpService.get<TotalSurvey[]>(fullUrl);
  }

  getTotalSurveyDetail(id: number): Promise<TotalSurveyDetail> {
    return this.backendHttpService.get<TotalSurveyDetail>(`api/totalsurvey/${id}/detail`);
  }

  createTotalSurvey(data: TotalSurvey): Observable<TotalSurvey> {
    return this.backendHttpService.post<TotalSurvey>('api/totalsurvey/create', data);
  }

  updateTotalSurvey(id: number, data: TotalSurveyDto): Observable<TotalSurvey> {
    return this.backendHttpService.post<TotalSurvey>(`api/totalsurvey/${id}/update`, data);
  }

  createSingleSurveys(totalSurveyId: number): Observable<TotalSurvey> {
    return this.backendHttpService.post<TotalSurvey>(`api/totalsurvey/${totalSurveyId}/create-singlesurvey`, {});
  }

  completeSurvey(totalSurveyId: number): Observable<TotalSurvey> {
    return this.backendHttpService.post<TotalSurvey>(`api/totalsurvey/${totalSurveyId}/complete`, {});
  }

  getSingleSurveyListOfSurvey(totalSurveyId: number): Promise<SingleSurveyListOfSurvey> {
    return this.backendHttpService.get<SingleSurveyListOfSurvey>(`api/totalsurvey/${totalSurveyId}/singlesurveys`);
  }

  deleteTotalSurvey(generalDirectorId: number, surveyId: number): Observable<TotalSurvey> {
    const fullUrl = this.prefixUrl + '/' + generalDirectorId + '/' + surveyId;
    console.log('Delete total survey detail url: ' + fullUrl);
    return this.backendHttpService.delete<TotalSurvey>(fullUrl);
  }
}
