import { TotalSurveyStatusColor } from './../commons/surveys/total-survey-status';
import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TotalSurvey } from '../commons/surveys/total-survey';
import { TotalSurveysService } from './total-surveys.service';
import { TotalSurveyStatus} from '../commons/surveys/total-survey-status';
import { DataTableModule, SelectItem } from 'primeng/primeng';
import {ButtonModule} from 'primeng/button';
import {DataViewModule} from 'primeng/dataview';
import {DialogModule} from 'primeng/dialog';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {ConfirmationService, Message} from 'primeng/api';
import {MessagesModule} from 'primeng/messages';
import {MessageModule} from 'primeng/message';
import { ToastProvider } from '../commons/services/toast.provider';
import { Utils } from '../commons/helper/utils';
import * as cloneDeep from 'lodash.clonedeep';

@Component({
  selector: 'app-total-surveys',
  templateUrl: './total-surveys.component.html',
  styleUrls: ['./total-surveys.component.css']
})
export class TotalSurveysComponent implements OnInit {

  constructor(private router: Router,
    private route: ActivatedRoute,
    private toastProvider: ToastProvider,
    private totalSurveysService: TotalSurveysService,
    private confirmationService: ConfirmationService) { }
  // totalSurveys = TOTAL_SURVEYS
  // @Input() directorId: number;
  totalSurveys: TotalSurvey[];
  allSurvey: TotalSurvey[];
  hideCompleteSurvey: TotalSurvey[];
  rowGroupMetadata: any;
  TotalSurveyStatus: typeof TotalSurveyStatus = TotalSurveyStatus;
  busy: any;
  selectedCar: TotalSurvey;
  displayDialog: boolean;
  isShowCompleteSurvey = false;
  status = {
    Created: TotalSurveyStatus.Created,
    Published: TotalSurveyStatus.Published,
    Validated: TotalSurveyStatus.Validated,
    Completed: TotalSurveyStatus.Completed
  };
  cols: any[];
  msgs: Message[] = [];


  ngOnInit() {
    console.log('TotalSurveys component on init', localStorage);
    this.cols = [
      { field: 'Status', header: 'Status' },
      {field: 'Survey Name', header: 'Name' },
      { field: 'Created Date', header: 'CreatedDate' },
      { field: 'Single Surveys', header: 'SingleSurveyQuantity' },
      { field: '_links', header: 'Action' },
    ];
    this.getTotalSurveys();
  }

  // get statusCss() {
  //   if (this.totalSurveys.length > 0) {

  //   }
  //   return Utils.getFieldColorCss(TotalSurveyStatusColor, this.data.Status);
  // }

  createItem() {
    this.router.navigate(['./create-survey'], { relativeTo: this.route });
  }

  getTotalSurveys(): void {
    // const directorId = +this.route.snapshot.paramMap.get('directorId');
    const user = JSON.parse(localStorage.getItem('currentUser'));
    this.busy = this.totalSurveysService.getTotalSurveys(user.Id).then(
      res => { // Success
        console.log('Get TotalSurveys response, size: ', res);
        this.allSurvey = res;
        this.hideCompleteSurvey = res.filter(x => x.Status !== TotalSurveyStatus.Completed);
        this.totalSurveys = cloneDeep(this.hideCompleteSurvey);
        console.log('Get TotalSurveys mapping: ', this.totalSurveys);
        this.updateRowGroupMetaData();
        }
    );
  }

  deleteTotalSurvey(surveyId: number) {
    const user = JSON.parse(localStorage.getItem('currentUser'));
    this.totalSurveysService.deleteTotalSurvey(user.Id, surveyId)
      .subscribe(
      () => { // Success
        console.log('Delete Total Survey success');
        this.toastProvider.showSuccessToast('Delete Total Survey Successfully!');
        // Reload survey list
        this.getTotalSurveys();
        }
    );
  }

  onSort() {
    this.updateRowGroupMetaData();
  }

  updateRowGroupMetaData() {
    this.rowGroupMetadata = {};
    if (this.totalSurveys) {
        for (let i = 0; i < this.totalSurveys.length; i++) {
            const rowData = this.totalSurveys[i];
            const year = rowData.SurveyYear;
            console.log('Survey year: ', year);
            if (i === 0) {
                this.rowGroupMetadata[rowData.SurveyYear] = { index: 0, size: 1 };
            } else {
                const previousRowData = this.totalSurveys[i - 1];
                const previousYear = previousRowData.SurveyYear;
                if (year === previousYear) {
                  this.rowGroupMetadata[rowData.SurveyYear].size++;
                } else {
                  this.rowGroupMetadata[rowData.SurveyYear] = { index: i, size: 1 };
                }
            }
        }
    }
  }

  createSingle(event, totalSurvey) {
    this.busy = this.totalSurveysService.createSingleSurveys(totalSurvey.Id).subscribe((res) => {
      let updatedTotalSurvey = this.totalSurveys.find(x => x.Id === totalSurvey.Id);
      updatedTotalSurvey = res;
      Object.assign(this.totalSurveys.find(x => x.Id === totalSurvey.Id), res);
      Object.assign(this.allSurvey.find(x => x.Id === totalSurvey.Id), res);
        Object.assign(this.hideCompleteSurvey.find(x => x.Id === totalSurvey.Id), res);
        if (this.isShowCompleteSurvey) {
          this.totalSurveys = cloneDeep(this.allSurvey);
        } else {
          this.totalSurveys = cloneDeep(this.hideCompleteSurvey);
        }
    });
  }

  openSingleSurveyList(totalSurvey) {
    this.router.navigate([`../total-survey/${totalSurvey.Id}/single-survey-list`], { relativeTo: this.route });
  }

  editSurvey(event, totalSurvey) {
    if (totalSurvey.Status === TotalSurveyStatus.Created) {
      this.router.navigate([`../total-survey/${totalSurvey.Id}/detail`], { relativeTo: this.route });
    }
    if (totalSurvey.Status === TotalSurveyStatus.Published) {
      this.router.navigate([`../total-survey/${totalSurvey.Id}/properties`], { relativeTo: this.route });
    }
  }

  completeSurvey(event, totalSurvey) {
    if (!totalSurvey.CanCompletedSurvey) {
      this.displayDialog = true;
      event.preventDefault();
      this.confirmationService.confirm({
        message: 'Please wait for all Single Survey of this Survey has status Closed',
        header: 'Warning'
      });
      event.preventDefault();
    } else {
      this.busy = this.totalSurveysService.completeSurvey(totalSurvey.Id).subscribe((res) => {
        let updatedTotalSurvey = this.totalSurveys.find(x => x.Id === totalSurvey.Id);
        updatedTotalSurvey = res;
        Object.assign(this.totalSurveys.find(x => x.Id === totalSurvey.Id), res);
        Object.assign(this.allSurvey.find(x => x.Id === totalSurvey.Id), res);
        Object.assign(this.hideCompleteSurvey.find(x => x.Id === totalSurvey.Id), res);
        if (this.isShowCompleteSurvey) {
          this.totalSurveys = cloneDeep(this.allSurvey);
        } else {
          this.totalSurveys = cloneDeep(this.hideCompleteSurvey);
        }
      });
    }
  }

  showDeleteConfirm(event: Event, car: TotalSurvey) {
    this.selectedCar = car;
    this.displayDialog = true;
    event.preventDefault();
    this.confirm();
  }

  onDialogHide() {
    this.selectedCar = null;
  }

  handleChange(e) {
    this.isShowCompleteSurvey = e.checked;
    if (this.isShowCompleteSurvey) {
      this.totalSurveys = cloneDeep(this.allSurvey);
    } else {
      this.totalSurveys = cloneDeep(this.hideCompleteSurvey);
    }
    this.updateRowGroupMetaData();
  }

  confirm() {
    this.confirmationService.confirm({
      message: 'Do you want to delete this survey?',
      header: 'Delete Confirmation',
      icon: 'pi pi-info-circle',
      accept: () => {
          this.msgs = [{severity: 'info', summary: 'Confirmed', detail: 'Survey deleted'}];
          this.deleteTotalSurvey(this.selectedCar.Id);
      },
      reject: () => {
          this.msgs = [{severity: 'info', summary: 'Rejected', detail: 'You have rejected'}];
      }
  });
  }
}
