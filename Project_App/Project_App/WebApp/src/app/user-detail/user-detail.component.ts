import { Component, OnInit, Input } from '@angular/core';
import { Location } from '@angular/common';
import { User } from '../commons/user/user';
import { ActivatedRoute } from '@angular/router';
import { USERS } from '../users/mock-users';
import { UserService } from '../user.service';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent implements OnInit {

  @Input() user: User;

  constructor(
    private route: ActivatedRoute,
    private location: Location,
    private userService: UserService
  ) { }

  ngOnInit() {
    this.getUser();
  }

  getUser(): void {
    const id = +this.route.snapshot.paramMap.get('id'); // Plus sign means: convert string to int
    this.userService.getUser(id).then(
      res => {
        this.user = res;
        console.log('Get user detail res: ', this.user);
      }
    );
  }

  goBack(): void {
    this.location.back();
  }

  save(): void {
    this.goBack();
  }
}
