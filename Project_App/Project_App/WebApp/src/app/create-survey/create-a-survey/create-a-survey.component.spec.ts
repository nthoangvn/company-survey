import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateASurveyComponent } from './create-a-survey.component';

describe('CreateASurveyComponent', () => {
  let component: CreateASurveyComponent;
  let fixture: ComponentFixture<CreateASurveyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateASurveyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateASurveyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
