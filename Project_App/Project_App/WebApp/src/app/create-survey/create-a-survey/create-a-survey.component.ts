import { TotalSurveysService } from './../../total-surveys/total-surveys.service';
import { ToastProvider } from './../../commons/services/toast.provider';
import { TotalSurvey } from './../../commons/surveys/total-survey';
import { BranchCompany } from './../../commons/objects/branch-company';
import { AuthService } from './../../commons/authentications/auth.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Utils } from 'src/app/commons/helper/utils';
import { Router, ActivatedRoute } from '@angular/router';
import { Modal, overlayConfigFactory } from 'ngx-modialog';
import { AlertModalComponent, AlertModalContext } from 'src/app/commons/modals/alert-modal/alert-modal.component';

export interface SurveyYear {
  value: number;
  viewValue: number;
}

@Component({
  selector: 'app-create-a-survey',
  templateUrl: './create-a-survey.component.html',
  styleUrls: ['./create-a-survey.component.less']
})

export class CreateASurveyComponent implements OnInit {
  busy: any;
  isLinear = false;
  data: TotalSurvey = <TotalSurvey>{};
  branchSourceList: BranchCompany[] = [];
  branchTargetList: BranchCompany[] = [];
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  surveyYearList: SurveyYear[] = [
    {value: 2017, viewValue: 2017},
    {value: 2018, viewValue: 2018},
    {value: 2019, viewValue: 2019}
  ];
  constructor(private _formBuilder: FormBuilder, protected userService: AuthService,
    protected toastProvider: ToastProvider, public totalSurveysService: TotalSurveysService,
    public router: Router, private route: ActivatedRoute, public modal: Modal) {}

  ngOnInit() {
    this.firstFormGroup = this._formBuilder.group({
      surveyName: ['', Validators.required],
      surveyYear: ['', Validators.required]
    });
    this.secondFormGroup = this._formBuilder.group({
      publishDeadline: ['', Validators.required],
      submitDeadline: ['', Validators.required],
      validateDeadline: ['', Validators.required]
    });
    this.busy = this.userService.getAllBranchCompany().then(
      (response) => {
        this.branchSourceList = response;
      });
  }

  save() {
    if (this.branchTargetList.length === 0) {
      this.toastProvider.showErrorToast('Please choose Branch Company for Survey');
      return;
    } else if (Utils.isNullOrUnDefined(this.data.PublishDeadline)
          || Utils.isNullOrUnDefined(this.data.SubmitDeadline)
          || Utils.isNullOrUnDefined(this.data.ValidateDeadline)) {
      this.toastProvider.showErrorToast('Please select full the deadline');
      return;
    } else {
      const currentUserId = JSON.parse(localStorage.getItem('currentUser')).Id;
      this.data.DirectorId = currentUserId;
      const branchCompanyIdList = this.branchTargetList.map(b => b.Id);
      this.data.BranchCompanyIdList = branchCompanyIdList;
      this.busy = this.totalSurveysService.createTotalSurvey(this.data).subscribe(
        (response) => {
          if (response) {
            this.toastProvider.showSuccessToast('Create Total Survey Successfully!');
            this.router.navigate(['../../total-survey-list'], { relativeTo: this.route });
          }
        }, error => {
          console.log(error);
          this.modal.open(AlertModalComponent, overlayConfigFactory(new AlertModalContext(
            'Create Survey Failed'), AlertModalContext)
          );
        });
    }
  }

  moveToTarget(event) {
    console.log(event);
    console.log('target', this.branchTargetList);
  }

  moveAllToTarget(event) {
    console.log(event);
  }

  moveAllToSource(event) {
    console.log(event);
  }

  moveToSource(event) {
    console.log(event);
    console.log('target', this.branchTargetList);
  }
}
