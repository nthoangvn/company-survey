import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleSurveyNotificationComponent } from './single-survey-notification.component';

describe('SingleSurveyNotificationComponent', () => {
  let component: SingleSurveyNotificationComponent;
  let fixture: ComponentFixture<SingleSurveyNotificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SingleSurveyNotificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleSurveyNotificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
