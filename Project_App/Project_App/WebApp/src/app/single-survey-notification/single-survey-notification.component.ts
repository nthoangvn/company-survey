import { Component, OnInit, Input, SimpleChanges, OnChanges } from '@angular/core';
import { SingleSurveyNotes } from '../commons/surveys/single-survey';
import { SingleSurveyStatus } from '../commons/surveys/single-survey-status';
import { Utils } from '../commons/helper/utils';

@Component({
  selector: 'app-single-survey-notification',
  templateUrl: './single-survey-notification.component.html',
  styleUrls: ['./single-survey-notification.component.less']
})
export class SingleSurveyNotificationComponent implements OnChanges {
  @Input() Notes: SingleSurveyNotes[] = [];
  SingleSurveyStatus: typeof SingleSurveyStatus = SingleSurveyStatus;
  status = {
    Open: SingleSurveyStatus.Open,
    Appending: SingleSurveyStatus.Appending,
    Submitted: SingleSurveyStatus.Submitted,
    Correcting: SingleSurveyStatus.Correcting,
    Closed: SingleSurveyStatus.Closed
  };
  constructor() { }

  ngOnChanges(changes: SimpleChanges) {
    console.log(changes);
    if (!Utils.isNullOrUnDefined(this.Notes)) {
      console.log(changes.currentValue);
    }
  }
}
