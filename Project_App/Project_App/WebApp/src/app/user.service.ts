import { Department } from './commons/user/department';
import { StaffAccount } from './commons/objects/staff-account';
import { Staff } from 'src/app/commons/user/staff';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from './commons/user/user';
import { USERS } from './users/mock-users';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { BackendHttpService } from './commons/services/http.service';
import { BranchCompany } from './commons/objects/branch-company';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private usersUrl = 'api/staffaccounts';  // URL to web api
  private branchStaffsUrl = 'api/staffaccounts/branch';  // URL to web api
  private branchCompanyUrl = 'api/staffaccounts';  // URL to web api
  private departmentUrl = 'api/department';  // URL to web api
  constructor(private backendHttpService: BackendHttpService) { }

  // getUsers(): User[] {
  //   return USERS;
  // }

  // getUser(id: number): User {
  //   var result = null;
  //   for (let i=0; i<USERS.length; i++) {
  //     var user = USERS[i];
  //     if (user.id == id) {
  //       result = user;
  //       break;
  //     }
  //   }
  //   return result;
  // }

  /** GET users from the server */
  getUsers (): Promise<User[]> {
    return this.backendHttpService.get<User[]>(this.usersUrl);
  }

  /** GET users from the server */
  getStaffsByBranchCompanyId (branchCompanyId: number): Promise<Staff[]> {
    var getBranchStaffsUrl = this.branchStaffsUrl + '/' + branchCompanyId;
    return this.backendHttpService.get<Staff[]>(getBranchStaffsUrl);
  }

  getUser (id: number): Promise<User> {
    var userDetailUrl = this.usersUrl + '/' + id;
    console.log('Get user detail url: ' + userDetailUrl);
    return this.backendHttpService.get<User>(userDetailUrl);
  }

  getBranchCompanyByDirectorId(directorId: number): Promise<BranchCompany> {
    const userDetailUrl = this.usersUrl + '/' + directorId;
    console.log('Get user detail url: ' + userDetailUrl);
    return this.backendHttpService.get<BranchCompany>(userDetailUrl);
  }

  getDepartmentByBranchId(branchId: number): Promise<Department[]> {
    const getDepartmentsUrl = this.departmentUrl + '/' + branchId;
    console.log('Get department url: ', getDepartmentsUrl);
    return this.backendHttpService.get<Department[]>(getDepartmentsUrl);
  }

  getStaffAccount(staffAccountId: number): Promise<Staff> {
    var getStaffsUrl = this.usersUrl + '/' + staffAccountId;
    console.log('Get staff account url: ' + getStaffsUrl);
    return this.backendHttpService.get<Staff>(getStaffsUrl);
  }

  createStaffAccount(staffAccount: Staff): Observable<Staff> {
    var createStaffsUrl = this.usersUrl;
    console.log('Create staff account url: ' + createStaffsUrl);
    return this.backendHttpService.post<Staff>(createStaffsUrl, staffAccount);
  }

  updateStaffAccount(staffAccount: Staff): Observable<Staff> {
    var updateStaffsUrl = this.usersUrl  + '/' + staffAccount.Id;
    console.log('Edit staff account url: ' + updateStaffsUrl);
    return this.backendHttpService.put<Staff>(updateStaffsUrl, staffAccount);
  }

  deleteStaffAccount(staffId: number): Observable<void> {
    var deleteStaffsUrl = this.branchCompanyUrl + '/' + staffId;
    console.log('Delete staff account url: ' + deleteStaffsUrl);
    return this.backendHttpService.delete<void>(deleteStaffsUrl);
  }

  /** GET user by id. Return `undefined` when id not found */
  // getUserNo404<Data>(id: number): Observable<User> {
  //   const url = `${this.usersUrl}/?id=${id}`;
  //   return this.http.get<User[]>(url)
  //     .pipe(
  //       map(users => users[0]), // returns a {0|1} element array
  //       tap(h => {
  //         const outcome = h ? `fetched` : `did not find`;
  //         this.log(`${outcome} user id=${id}`);
  //       }),
  //       catchError(this.handleError<User>(`getUser id=${id}`))
  //     );
  // }

  /** GET user by id. Will 404 if id not found */
  // getUser(id: number): Observable<User> {
  //   const url = `${this.usersUrl}/${id}`;
  //   return this.http.get<User>(url).pipe(
  //     tap(_ => this.log(`fetched user id=${id}`)),
  //     catchError(this.handleError<User>(`getUser id=${id}`))
  //   );
  // }

  /* GET users whose name contains search term */
  // searchUsers(term: string): Observable<User[]> {
  //   if (!term.trim()) {
  //     // if not search term, return empty user array.
  //     return of([]);
  //   }
  //   return this.http.get<User[]>(`${this.usersUrl}/?name=${term}`).pipe(
  //     tap(_ => this.log(`found users matching "${term}"`)),
  //     catchError(this.handleError<User[]>('searchUsers', []))
  //   );
  // }

  //////// Save methods //////////

  /** POST: add a new user to the server */
  // addUser (user: User): Observable<User> {
  //   return this.http.post<User>(this.usersUrl, user, httpOptions).pipe(
  //     tap((user: User) => this.log(`added user w/ id=${user.id}`)),
  //     catchError(this.handleError<User>('addUser'))
  //   );
  // }

  /** DELETE: delete the user from the server */
  // deleteUser (user: User | number): Observable<User> {
  //   const id = typeof user === 'number' ? user : user.id;
  //   const url = `${this.usersUrl}/${id}`;

  //   return this.http.delete<User>(url, httpOptions).pipe(
  //     tap(_ => this.log(`deleted user id=${id}`)),
  //     catchError(this.handleError<User>('deleteUser'))
  //   );
  // }

  /** PUT: update the user on the server */
  // updateUser (user: User): Observable<any> {
  //   return this.http.put(this.usersUrl, user, httpOptions).pipe(
  //     tap(_ => this.log(`updated user id=${user.id}`)),
  //     catchError(this.handleError<any>('updateUser'))
  //   );
  // }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** Log a UserService message with the MessageService */
  private log(message: string) {
    // this.messageService.add(`UserService: ${message}`);
  }
}
