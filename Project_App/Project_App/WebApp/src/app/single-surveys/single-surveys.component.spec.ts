import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleSurveysComponent } from './single-surveys.component';

describe('SingleSurveysComponent', () => {
  let component: SingleSurveysComponent;
  let fixture: ComponentFixture<SingleSurveysComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SingleSurveysComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleSurveysComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
