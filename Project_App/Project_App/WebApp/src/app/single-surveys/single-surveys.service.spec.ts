import { TestBed } from '@angular/core/testing';

import { SingleSurveysService } from './single-surveys.service';

describe('SingleSurveysService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SingleSurveysService = TestBed.get(SingleSurveysService);
    expect(service).toBeTruthy();
  });
});
