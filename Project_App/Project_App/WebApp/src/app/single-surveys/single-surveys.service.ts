import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { BackendHttpService } from '../commons/services/http.service';
import { SingleSurvey, SingleSurveyDetail } from '../commons/surveys/single-survey';
import { SingleSurveyDetailVO } from '../commons/objects/single-survey-detail-VO';
import { ReportDataVO } from '../commons/objects/report-data-VO';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class SingleSurveysService {
  private prefixUrl = 'api/singlesurvey';  // URL to web api
  constructor(private backendHttpService: BackendHttpService) { }

  /** GET single surveys from the server */
  getSingleSurveys (branchDirectorId: number): Promise<SingleSurvey[]> {
    const fullUrl = this.prefixUrl + '/' + branchDirectorId;
    console.log('Get single survey detail url: ' + fullUrl);
    return this.backendHttpService.get<SingleSurvey[]>(fullUrl);
  }

  getSingleSurveyDetail(id: number): Promise<SingleSurveyDetail> {
    return this.backendHttpService.get<SingleSurveyDetail>(`api/singlesurvey/${id}/detail`);
  }

  updateSingleSurveyDetail(id: number, singleSurveyDetailVO: SingleSurveyDetailVO): Observable<SingleSurveyDetail> {
    return this.backendHttpService.post<SingleSurveyDetail>(`api/singlesurvey/${id}/updatesalary`, singleSurveyDetailVO);
  }

  correctingSingleSurvey(id: number): Observable<SingleSurveyDetail> {
    return this.backendHttpService.post<SingleSurveyDetail>(`api/singlesurvey/${id}/correcting`, {});
  }

  submitSingleSurvey(id: number): Observable<SingleSurveyDetail> {
    return this.backendHttpService.post<SingleSurveyDetail>(`api/singlesurvey/${id}/submit`, {});
  }

  closeSingleSurvey(id: number): Observable<SingleSurveyDetail> {
    return this.backendHttpService.post<SingleSurveyDetail>(`api/singlesurvey/${id}/close`, {});
  }

  generateReport(id: number, reportData: ReportDataVO) {
    const queryString = !!reportData ? Object.keys(reportData).reduce(
      (previousValue, currentValue, currentIndex) => {
        return currentIndex === 1
          ? `?${previousValue}=${reportData[previousValue]}&${currentValue}=${reportData[currentValue]}`
          : `${previousValue}&${currentValue}=${reportData[currentValue]}`;
      }) : '';
    window.open(`api/singlesurvey/${id}/generatereport` + queryString, '_blank');
  }

  deleteSingleSurvey(generalDirectorId: number, surveyId: number): Observable<SingleSurvey> {
    const fullUrl = this.prefixUrl + '/' + generalDirectorId + '/' + surveyId;
    console.log('Delete single survey detail url: ' + fullUrl);
    return this.backendHttpService.delete<SingleSurvey>(fullUrl);
  }
}
