import { Component, OnInit, Input } from '@angular/core';
import { SingleSurvey } from '../commons/surveys/single-survey';

import { SingleSurveysService } from './single-surveys.service';
import { ActivatedRoute, Router } from '@angular/router';
import { SingleSurveyStatus} from '../commons/surveys/single-survey-status';
import { DataTableModule, SelectItem } from 'primeng/primeng';
import {ButtonModule} from 'primeng/button';
import {DataViewModule} from 'primeng/dataview';
import {DialogModule} from 'primeng/dialog';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {ConfirmationService, Message} from 'primeng/api';
import {MessagesModule} from 'primeng/messages';
import {MessageModule} from 'primeng/message';
import { ToastProvider } from '../commons/services/toast.provider';
import {ToastModule} from 'primeng/toast';
import { User } from '../commons/user/user';
import { UserService } from '../user.service';
import { BranchCompany } from '../commons/objects/branch-company';
import { AuthService } from '../commons/authentications/auth.service';

@Component({
  selector: 'app-single-surveys',
  templateUrl: './single-surveys.component.html',
  styleUrls: ['./single-surveys.component.css']
})
export class SingleSurveysComponent implements OnInit {

  // @Input() directorId: number;
  singleSurveys: SingleSurvey[];
  rowGroupMetadata: any;
  SingleSurveyStatus: typeof SingleSurveyStatus = SingleSurveyStatus;

  status = {
    Open: SingleSurveyStatus.Open,
    Appending: SingleSurveyStatus.Appending,
    Submitted: SingleSurveyStatus.Submitted,
    Correcting: SingleSurveyStatus.Correcting,
    Closed: SingleSurveyStatus.Closed
  };
  selectedSingleSurvey: SingleSurvey;
  cols: any[];
  msgs: Message[] = [];
  user: User;
  branchCompany: BranchCompany;

  constructor(private router: Router,
    private route: ActivatedRoute,
    private toastProvider: ToastProvider,
    private authService: AuthService,
    private singleSurveysService: SingleSurveysService,
    private confirmationService: ConfirmationService) { }

  ngOnInit() {
    console.log('SingleSurveys component on init', localStorage);
    this.user = JSON.parse(localStorage.getItem('currentUser'));
    this.cols = [
      { field: 'SurveyName', header: 'Survey Name'},
      { field: 'Status', header: 'Status' },
      { field: 'CreatedDate', header: 'Created Date' },
      { field: 'SubmitDeadline', header: 'Submit Deadline'},
      { field: '_links', header: 'Action'}


    ];
    this.getBranchCompany();
    this.getSingleSurveys();
  }

  getBranchCompany(): any {
    this.authService.getBranchCompanyByDirectorId(this.user.Id).then(
      res => { // Success
        this.branchCompany = res;
        console.log('Get branch company response: ', this.branchCompany);
        }
    );
  }

  getSingleSurveys(): void {
    this.singleSurveysService.getSingleSurveys(this.user.Id).then(
      res => { // Success
        console.log('Get SingleSurveys response, size: ', res);
        this.singleSurveys = res;
        console.log('Get SingleSurveys mapping: ', this.singleSurveys);
        this.updateRowGroupMetaData();
        }
    );
  }

  deleteSigleSurvey(surveyId: number) {
    this.singleSurveysService.deleteSingleSurvey(this.user.Id, surveyId)
      .subscribe(
      () => { // Success
          console.log('Delete Single Survey success');
          this.toastProvider.showSuccessToast('Delete Single Survey Successfully!');
          // Reload survey list
          this.getSingleSurveys();
        }
    );
  }

  onSort() {
    this.updateRowGroupMetaData();
  }

  updateRowGroupMetaData() {
    this.rowGroupMetadata = {};
    if (this.singleSurveys) {
        for (let i = 0; i < this.singleSurveys.length; i++) {
            const rowData = this.singleSurveys[i];
            const year = rowData.SurveyYear;
            console.log('Survey year: ', year);
            if (i === 0) {
                this.rowGroupMetadata[rowData.SurveyYear] = { index: 0, size: 1 };
            } else {
                const previousRowData = this.singleSurveys[i - 1];
                const previousYear = previousRowData.SurveyYear;
                if (year === previousYear) {
                  this.rowGroupMetadata[rowData.SurveyYear].size++;
                } else {
                  this.rowGroupMetadata[rowData.SurveyYear] = { index: i, size: 1 };
                }
            }
        }
    }
  }

  displayDialog: boolean;
  showDeleteConfirm(event: Event, car: SingleSurvey) {
    this.selectedSingleSurvey = car;
    this.displayDialog = true;
    event.preventDefault();
    this.confirm();
  }

  showBranchStaffs(event: Event) {
    this.displayDialog = true;
    event.preventDefault();
    var branchStaffsUrl = '/branch-staffs/' + this.branchCompany.Id;
        console.log('Branch staffs URL: ', branchStaffsUrl);
        this.router.navigateByUrl(branchStaffsUrl);
  }

  onDialogHide() {
    this.selectedSingleSurvey = null;
  }

  onRowSelect(event) {
    console.log(event);
    this.router.navigate([`../single-survey/${event.data.Id}/detail`], { relativeTo: this.route });
  }

  onRowUnselect(event) {
    console.log(event);
  }

  confirm() {
    this.confirmationService.confirm({
      message: 'Do you want to delete this survey?',
      header: 'Delete Confirmation',
      icon: 'pi pi-info-circle',
      accept: () => {
          this.msgs = [{severity:'info', summary:'Confirmed', detail:'Survey deleted'}];
          this.deleteSigleSurvey(this.selectedSingleSurvey.Id);
      },
      reject: () => {
          this.msgs = [{severity:'info', summary:'Rejected', detail:'You have rejected'}];
      }
  });
  }
}
