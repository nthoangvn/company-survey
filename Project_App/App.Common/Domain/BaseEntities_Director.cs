using App.Common.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;
using App.Common.Enum;

namespace App.Common.Domain
{
    

    public partial class BaseEntities_Director : BaseEntity
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public BaseEntities_Director()
        {
            BaseEntities_TotalSurvey = new HashSet<BaseEntities_TotalSurvey>();
        }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        public string Phone { get; set; }

        [NotMapped]
        public Enum.DirectorRole Role { get; set; }

        [StringLength(200)]
        [Column("Role")]
        public string TypeString
        {
            get { return Role.ToString(); }
            private set { Role = value.ParseEnum<Enum.DirectorRole>(); }
        }

        public bool IsActive { get; set; }

        [ForeignKey("BaseEntities_IdentityUser")]
        public int IdentityUserId { get; set; }

        public virtual BaseEntities_IdentityUser BaseEntities_IdentityUser { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BaseEntities_TotalSurvey> BaseEntities_TotalSurvey { get; set; }

        public string GetFullName()
        {
            var fullname = $"{FirstName} {LastName}";
            return fullname;
        }
    }
}
