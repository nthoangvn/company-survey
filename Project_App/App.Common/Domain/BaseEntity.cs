﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace App.Common.Domain
{
    /// <summary>
    /// Add row version column and Id column (long: identity)
    /// </summary>
    public class BaseEntity
    {
        [Timestamp]
        public byte[] RowVersion { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public override string ToString()
        {
            return $"{GetType().Name}(Id = {Id})";
        }
    }
}