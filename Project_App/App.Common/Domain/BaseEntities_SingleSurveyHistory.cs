﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Common.Enum;
using App.Common.Helper;

namespace App.Common.Domain
{
    public partial class BaseEntities_SingleSurveyHistory : BaseEntities_SurveyHistory
    {
        // [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]

        [NotMapped]
        [Required]
        public Enum.SingleSurveyStatus SingleSurveyStatus
        {
            get { return SurveyStatus.ParseEnum<Common.Enum.SingleSurveyStatus>(); }
            set { SurveyStatus = value.ToString(); }
        }

        [ForeignKey("BaseEntities_SingleSurvey")]
        public int SingleSurveyId { get; set; }

        public BaseEntities_SingleSurvey BaseEntities_SingleSurvey { get; set; }

    }
}
