﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Common.Domain
{
    public partial class BaseEntities_SurveyHistory : BaseEntity
    {
        public string SurveyStatus { get; set; }
        public string SurveyName { get; set; }
        public string Author { get; set; }
        public DateTime Date { get; set; }
    }
}
