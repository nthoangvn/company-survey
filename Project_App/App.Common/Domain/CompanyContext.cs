using System.Data.Entity.Validation;

namespace App.Common.Domain
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public class CompanyContext : DbContext
    {
        public const string ConnectionStringName = "CompanyContext";

        public CompanyContext() : this(ConnectionStringName)
        {
            Configuration.ProxyCreationEnabled = false;
        }

        public CompanyContext(string dbName)
            : base($"name={dbName}")
        {
        }

        protected virtual string Schema => "App";
        public string GetSchema()
        {
            return Schema;
        }

        public virtual DbSet<BaseEntities_AccountOfStaff> BaseEntities_AccountOfStaff { get; set; }
        public virtual DbSet<BaseEntities_AccountsInSingleSurvey> BaseEntities_AccountsInSingleSurvey { get; set; }
        public virtual DbSet<BaseEntities_BranchCompany> BaseEntities_BranchCompany { get; set; }
        public virtual DbSet<BaseEntities_Department> BaseEntities_Department { get; set; }
        public virtual DbSet<BaseEntities_Director> BaseEntities_Director { get; set; }
        public virtual DbSet<BaseEntities_IdentityUser> BaseEntities_IdentityUser { get; set; }
        public virtual DbSet<BaseEntities_SingleSurvey> BaseEntities_SingleSurvey { get; set; }
        public virtual DbSet<BaseEntities_TotalSurvey> BaseEntities_TotalSurvey { get; set; }
        public virtual DbSet<BaseEntities_SurveyHistory> BaseEntities_SurveyHistory { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            modelBuilder.Entity<BaseEntities_AccountOfStaff>()
                .HasMany(e => e.BaseEntities_AccountsInSingleSurvey)
                .WithRequired(e => e.BaseEntities_AccountOfStaff)
                .HasForeignKey(e => e.AccountOfStaffId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BaseEntities_BranchCompany>()
                .HasMany(e => e.BaseEntities_Department)
                .WithRequired(e => e.BaseEntities_BranchCompany)
                .HasForeignKey(e => e.BranchCompanyId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BaseEntities_Department>()
                .HasMany(e => e.BaseEntities_AccountOfStaff)
                .WithRequired(e => e.BaseEntities_Department)
                .HasForeignKey(e => e.DepartmentId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BaseEntities_BranchCompany>()
                .HasOptional(e => e.BaseEntities_Director);

            modelBuilder.Entity<BaseEntities_Director>()
                .HasMany(e => e.BaseEntities_TotalSurvey)
                .WithOptional(e => e.BaseEntities_Director)
                .HasForeignKey(e => e.DirectorId);

            modelBuilder.Entity<BaseEntities_SingleSurvey>()
                .HasMany(e => e.BaseEntities_AccountsInSingleSurvey)
                .WithRequired(e => e.BaseEntities_SingleSurvey)
                .HasForeignKey(e => e.SingleSurveyId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BaseEntities_TotalSurvey>()
                .HasMany(e => e.BaseEntities_SingleSurvey)
                .WithRequired(e => e.BaseEntities_TotalSurvey)
                .HasForeignKey(e => e.TotalSurveyId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BaseEntities_Director>()
                .HasOptional(e => e.BaseEntities_IdentityUser)
                .WithRequired(i => i.BaseEntities_Director);
            modelBuilder.Entity<BaseEntities_SurveyHistory>().ToTable(nameof(Domain.BaseEntities_SurveyHistory));

            base.OnModelCreating(modelBuilder);
        }

        public override int SaveChanges()
        {
            var modifiedEntries = ChangeTracker.Entries()
                .Where(x => x.State == EntityState.Added || x.State == EntityState.Modified);


            foreach (var entry in modifiedEntries)
            {
                var entity = entry.Entity as BaseEntity;

                if (entity != null)
                {


                }
            }

            var modifiedOrDeletedEntries = ChangeTracker.Entries()
                .Where(x => x.State == EntityState.Deleted);


            try
            {
                return base.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors, ex);
            }
        }
    }
}
