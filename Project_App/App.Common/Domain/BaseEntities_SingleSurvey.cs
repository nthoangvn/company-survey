using App.Common.Helper;
using App.Common.Enum;
namespace App.Common.Domain
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class BaseEntities_SingleSurvey : BaseEntity
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public BaseEntities_SingleSurvey()
        {
            BaseEntities_AccountsInSingleSurvey = new HashSet<BaseEntities_AccountsInSingleSurvey>();
            BaseEntities_SingleSurveyHistory = new List<BaseEntities_SingleSurveyHistory>();
        }

        [NotMapped]
        public Common.Enum.SingleSurveyStatus Status { get; set; }

        [StringLength(200)]
        [Column("Status")]
        public string TypeString
        {
            get { return Status.ToString(); }
            set { Status = value.ParseEnum<Common.Enum.SingleSurveyStatus>(); }
        }

        public DateTime CreatedDate { get; set; }

        public DateTime? CloseDate { get; set; }

        public DateTime? FillingTime { get; set; }

        public DateTime? SubmitTime { get; set; }

        [ForeignKey("BaseEntities_BranchCompany")]
        public int BranchCompanyId { get; set; }

        public List<BaseEntities_SingleSurveyHistory> BaseEntities_SingleSurveyHistory { get; set; }

        public int TotalSurveyId { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BaseEntities_AccountsInSingleSurvey> BaseEntities_AccountsInSingleSurvey { get; set; }

        public virtual BaseEntities_BranchCompany BaseEntities_BranchCompany { get; set; }

        public virtual BaseEntities_TotalSurvey BaseEntities_TotalSurvey { get; set; }
    }
}
