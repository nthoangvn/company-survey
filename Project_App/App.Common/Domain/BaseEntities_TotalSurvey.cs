﻿using App.Common.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace App.Common.Domain
{
    

    public partial class BaseEntities_TotalSurvey : BaseEntity
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public BaseEntities_TotalSurvey()
        {
            BaseEntities_SingleSurvey = new HashSet<BaseEntities_SingleSurvey>();
            BaseEntities_TotalSurveyHistory = new List<BaseEntities_TotalSurveyHistory>();
        }

        [NotMapped]
        public Enum.TotalSurveyStatus Status { get; set; }

        [StringLength(200)]
        [Column("Status")]
        public string TypeString
        {
            get { return Status.ToString(); }
            private set { Status = value.ParseEnum<Enum.TotalSurveyStatus>(); }
        }

        public string SurveyName { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime? CloseDate { get; set; }

        public int? DirectorId { get; set; }

        public List<BaseEntities_TotalSurveyHistory> BaseEntities_TotalSurveyHistory { get; set; }

        public string BranchCompanyIdList { get; set; }

        public DateTime PublishDeadline { get; set; }

        public DateTime SubmitDeadline { get; set; }

        public DateTime ValidateDeadline { get; set; }

        public int SurveyYear { get; set; }

        public int SingleSurveyQuantity { get; set; }

        public virtual BaseEntities_Director BaseEntities_Director { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BaseEntities_SingleSurvey> BaseEntities_SingleSurvey { get; set; }
    }
}

