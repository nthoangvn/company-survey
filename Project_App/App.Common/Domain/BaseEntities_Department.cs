namespace App.Common.Domain
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class BaseEntities_Department : BaseEntity
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public BaseEntities_Department()
        {
            BaseEntities_AccountOfStaff = new HashSet<BaseEntities_AccountOfStaff>();
        }

        [Required]
        public string Name { get; set; }

        public int BranchCompanyId { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BaseEntities_AccountOfStaff> BaseEntities_AccountOfStaff { get; set; }

        public virtual BaseEntities_BranchCompany BaseEntities_BranchCompany { get; set; }
    }
}
