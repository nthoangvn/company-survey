namespace App.Common.Domain
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class BaseEntities_AccountOfStaff : BaseEntity
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public BaseEntities_AccountOfStaff()
        {
            BaseEntities_AccountsInSingleSurvey = new HashSet<BaseEntities_AccountsInSingleSurvey>();
        }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        public string StaffVisa { get; set; }

        [Required]
        public string Phone { get; set; }

        public bool Activated { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime? LeaveDate { get; set; }

        [Required]
        public string Position { get; set; }

        public int DepartmentId { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BaseEntities_AccountsInSingleSurvey> BaseEntities_AccountsInSingleSurvey { get; set; }

        public virtual BaseEntities_Department BaseEntities_Department { get; set; }
    }
}
