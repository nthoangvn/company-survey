namespace App.Common.Domain
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class BaseEntities_BranchCompany : BaseEntity
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public BaseEntities_BranchCompany()
        {
            BaseEntities_Department = new HashSet<BaseEntities_Department>();
            BaseEntities_SingleSurvey = new HashSet<BaseEntities_SingleSurvey>();
        }

        [Required]
        public string CountryName { get; set; }

        public string City { get; set; }

        public DateTime EstablishDate { get; set; }

        [Required]
        public string Address { get; set; }

        [ForeignKey("BaseEntities_Director")]
        public int? Director_Id { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BaseEntities_Department> BaseEntities_Department { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BaseEntities_SingleSurvey> BaseEntities_SingleSurvey { get; set; }

        public virtual BaseEntities_Director BaseEntities_Director { get; set; }

        public string GetFullAddress()
        {
            var fullAddress = $"{Address}, {City}, {CountryName}";
            return fullAddress;
        }
    }
}
