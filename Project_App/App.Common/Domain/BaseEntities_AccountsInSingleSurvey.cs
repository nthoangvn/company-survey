namespace App.Common.Domain
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class BaseEntities_AccountsInSingleSurvey : BaseEntity
    {
        public int AccountOfStaffId { get; set; }

        public int SingleSurveyId { get; set; }

        public decimal? SalaryValue { get; set; }

        public virtual BaseEntities_AccountOfStaff BaseEntities_AccountOfStaff { get; set; }

        public virtual BaseEntities_SingleSurvey BaseEntities_SingleSurvey { get; set; }
    }
}
