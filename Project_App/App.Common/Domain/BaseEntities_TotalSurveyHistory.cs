﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Common.Helper;

namespace App.Common.Domain
{
    public partial class BaseEntities_TotalSurveyHistory : BaseEntities_SurveyHistory
    {
        [NotMapped]
        [Required]
        public Enum.TotalSurveyStatus TotalSurveyStatus
        {
            get { return SurveyStatus.ParseEnum<Common.Enum.TotalSurveyStatus>(); }
            set { SurveyStatus = value.ToString(); }
        }

        [ForeignKey("BaseEntities_TotalSurvey")]
        public int TotalSurveyId { get; set; }

        public BaseEntities_TotalSurvey BaseEntities_TotalSurvey { get; set; }
    }
}
