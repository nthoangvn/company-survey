﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using App.Common.Domain;

namespace App.Common.Repositories
{
    public interface IGenericRepository<T>
    {
        #region IMethods

        /// <summary>
        /// Insert a new entity to DB
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="isCommitted">enable commit to db</param>
        T AddOrUpdate(T entity, bool isCommitted = false);

        /// <summary>
        /// Insert new entities to DB
        /// </summary>
        /// <returns></returns>
        IEnumerable<T> AddOrUpdate(IEnumerable<T> entities);

        /// <summary>
        /// Remove entity from DB context. If necessary, set a flag
        /// isCommitted to true to enable to commit to db.
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="isCommitted">enable commit to db</param>
        bool Remove(T entity, bool isCommitted = false);

        /// <summary>
        /// Remove all of entities
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        IEnumerable<T> RemoveRange(IEnumerable<T> entities);

        /// <summary>
        /// Get all entries
        /// </summary>
        IList<T> GetAll();

        /// <summary>
        /// Get entry by Id
        /// </summary>
        T Get(int id);

        /// <summary>
        /// Get entry by Id with include expressions
        /// </summary>
        T Get(int id, params Expression<Func<T, object>>[] includeExpressions);

        /// <summary>
        /// Force a reload of an entity from the database
        /// </summary>
        /// <param name="entity">The entity to reload</param>
        void Reload(T entity);

        /// <summary>
        /// Change entity state to Detached 
        /// </summary>
        /// <param name="entity">The entity to reload</param>
        void Detach(T entity);

        /// <summary>
        /// Get entry by Id
        /// </summary>
        IList<T> FindAllByIdList(IList<int> idList, params Expression<Func<T, object>>[] includeExpressions);

        /// <summary>
        /// Find first or default with include expressions
        /// </summary>
        T Find(Expression<Func<T, bool>> match, params Expression<Func<T, object>>[] includeExpressions);
        /// <summary>
        /// Find all entries by expression
        /// </summary>
        IList<T> FindAll();

        /// <summary>
        /// Find all entries by expression with include expressions
        /// </summary>
        IList<T> FindAll(Expression<Func<T, bool>> match, params Expression<Func<T, object>>[] includeExpressions);

        /// <summary>
        /// Find all with include expressions, force load from DB
        /// </summary>
        IList<T> FindAllAsNoTrackChange(Expression<Func<T, bool>> match,
            params Expression<Func<T, object>>[] includeExpressions);

        /// <summary>
        /// Find all entries by many expressions with include expressions
        /// </summary>
        /// <param name="matches"></param>
        /// <param name="includeExpressions"></param>
        /// <returns></returns>
        IList<T> FindAllWithConditions(IEnumerable<Expression<Func<T, bool>>> matches,
            params Expression<Func<T, object>>[] includeExpressions);

        /// <summary>
        /// Find all entries by expression
        /// </summary>
        IList<T> FindAll(Expression<Func<T, bool>> match);

        /// <summary>
        /// Query pagination
        /// </summary>
        /// <param name="match"></param>
        /// <param name="orderBy"></param>
        /// <param name="skipItems"></param>
        /// <param name="itemsPerPage"></param>
        /// <returns></returns>
        IList<T> Find(Expression<Func<T, bool>> match, Expression<Func<T, string>> orderBy, int skipItems,
            int itemsPerPage);

        /// <summary>
        /// Count the number of entity
        /// </summary>
        /// <param name="match"></param>
        /// <returns></returns>
        int Count(Expression<Func<T, bool>> match);

        /// <summary>
        /// Delete one row
        /// </summary>
        void Delete(T t);

        /// <summary>
        /// Save change
        /// </summary>
        void Commit();


        #endregion IMethods
    }
}
