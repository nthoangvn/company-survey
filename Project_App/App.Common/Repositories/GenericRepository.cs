﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using App.Common.Domain;
using App.Common.Factory;
using log4net;

namespace App.Common.Repositories
{
    // ReSharper disable once InconsistentNaming
    public class GenericRepository<T, S> : IGenericRepository<T>
        where T : Domain.BaseEntity
        where S : CompanyContext, new()
    {
        //public S DbContext;
        public virtual S DbContext => DbContextFactory.GetContextPerRequest<S>();

//        public GenericRepository(S dbContext)
//        {
//            DbContext = dbContext;
//        }

//        private readonly TimeSpan _waitingInterval = new TimeSpan(0, 0, 5);
//        private readonly TimeSpan _pollIntervalMs = new TimeSpan(0, 0, 10);
//        private const int JobFinished = 4;
//        private const int JobSuccessful = 1;
//        private const int MaxPollCount = 180; // = 30 min (value from ADD) * 60 / _pollIntervalMs
//        private const int MaxWaitingCount = 1440; // 2 hours * 60 * 60 / _waitingInterval

        public T AddOrUpdate(T entity, bool isCommitted = false)
        {
            DbContext.Set<T>().AddOrUpdate(entity);
            if (isCommitted)
            {
                Commit();
            }
            return entity;
        }

        public IEnumerable<T> AddOrUpdate(IEnumerable<T> entities)
        {
            var list = entities as T[] ?? entities.ToArray();
            DbContext.Set<T>().AddOrUpdate(list);
            return list;
        }

        public bool Remove(T entity, bool isCommitted = false)
        {
            DbContext.Set<T>().Remove(entity);
            if (isCommitted)
            {
                try
                {
                    Commit();
                    return true;
                }
                catch (System.Exception e)
                {
                    return false;
                }
            }
            return true;
        }

        public IEnumerable<T> RemoveRange(IEnumerable<T> entities)
        {
            var list = entities as T[] ?? entities.ToArray();
            DbContext.Set<T>().RemoveRange(list);
            return list;
        }

        /// <summary>
        /// Get all entries
        /// </summary>
        public virtual IList<T> GetAll()
        {
            return DbContext.Set<T>().ToList();
        }

        /// <summary>
        /// Get entry by Id
        /// </summary>
        public virtual T Get(int id)
        {
            T t = GetQueryableId(id);
            return t;
        }

        public virtual T Get(int id, params Expression<Func<T, object>>[] includeExpressions)
        {
            return Find(t => t.Id == id, includeExpressions);
        }

        /// <summary>
        /// Force a reload of an entity from the database
        /// </summary>
        /// <param name="entity">The entity to reload</param>
        public void Reload(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }
            DbContext.Entry(entity).Reload();
        }

        /// <summary>
        /// Change entity state to Detached 
        /// </summary>
        /// <param name="entity">The entity to reload</param>
        public void Detach(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }
            DbContext.Entry(entity).State = EntityState.Detached;
        }

        public virtual IList<T> FindAllByIdList(IList<int> idList, params Expression<Func<T, object>>[] includeExpressions)
        {
            return FindAll(t => idList.Contains(t.Id), includeExpressions);
        }

        /// <summary>
        /// Get entry queryable by Id
        /// </summary>
        public virtual T GetQueryableId(int id)
        {
            var queryable = DbContext.Set<T>();
            T t = queryable.FirstOrDefault(i => i.Id == id);
            return t;
        }

        /// <summary>
        /// Find first or default with include expressions
        /// </summary>
        public virtual T Find(Expression<Func<T, bool>> match, params Expression<Func<T, object>>[] includeExpressions)
        {
            IQueryable<T> queryable = DbContext.Set<T>();
            if (includeExpressions.Any())
            {
                queryable = includeExpressions
                    .Aggregate(queryable, (current, expression) => current.Include(expression));
            }
            return queryable.FirstOrDefault(match);
        }


        /// <summary>
        /// Find all entries
        /// </summary>
        public virtual IList<T> FindAll()
        {
            return FindAll(t => true);
        }

        /// <summary>
        /// Find all entries by expression
        /// </summary>
        public virtual IList<T> FindAll(Expression<Func<T, bool>> match)
        {
            var queryable = DbContext.Set<T>();
            var result = queryable.Where(match);
            if (result != null)
            {
                return result.ToList();
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Find all with include expressions
        /// </summary>
        public virtual IList<T> FindAll(Expression<Func<T, bool>> match, params Expression<Func<T, object>>[] includeExpressions)
        {
            var queryable = CreateFindAllQueryable(match, includeExpressions);
            if (queryable == null)
            {
                return null;
            }
            else
            {
                return queryable.ToList();
            }

        }

        /// <summary>
        /// Find all with include expressions, force load from DB
        /// </summary>
        public virtual IList<T> FindAllAsNoTrackChange(Expression<Func<T, bool>> match, params Expression<Func<T, object>>[] includeExpressions)
        {
            return CreateFindAllQueryable(match, includeExpressions).AsNoTracking().Where(match).ToList();
        }

        /// <summary>
        /// Find all with include expressions
        /// </summary>
        public virtual IList<T> FindAllWithConditions(IEnumerable<Expression<Func<T, bool>>> matches, params Expression<Func<T, object>>[] includeExpressions)
        {
            IQueryable<T> queryable = DbContext.Set<T>();
            if (includeExpressions.Any())
            {
                queryable = includeExpressions
                    .Aggregate(queryable, (current, expression) => current.Include(expression));
            }
            return matches.Aggregate(queryable, (current, expression) => current.Where(expression)).ToList();
        }

        public virtual IList<T> Find(Expression<Func<T, bool>> match, Expression<Func<T, string>> orderBy, int skipItems, int itemsPerPage)
        {
            var queryable = DbContext.Set<T>();
            return queryable.Where(match).OrderBy(orderBy).Skip(skipItems).Take(itemsPerPage).ToList();
        }

        public int Count(Expression<Func<T, bool>> match)
        {
            return DbContext.Set<T>().Where(match).Count();
        }

        /// <summary>
        /// Delete one row
        /// </summary>
        public virtual void Delete(T t)
        {
            if (t == null)
            {
                return;
            }
            DbContext.Set<T>().Remove(t);
        }

        public void Commit()
        {
            bool saveFailed;
            do
            {
                saveFailed = false;

                try
                {
                    DbContext.SaveChanges();
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    saveFailed = true;
                    // Update the values of the entity that failed to save from the store 
                    ex.Entries.Single().Reload();
                }
            } while (saveFailed);
        }

        public bool Any(Expression<Func<T, bool>> match)
        {
            return DbContext.Set<T>().Any(match);
        }

        private IQueryable<T> CreateFindAllQueryable(Expression<Func<T, bool>> match, Expression<Func<T, object>>[] includeExpressions)
        {
            IQueryable<T> queryable = DbContext.Set<T>();
            if (includeExpressions.Any())
            {
                queryable = includeExpressions
                    .Aggregate(queryable, (current, expression) => current.Include(expression));
            }

            queryable = queryable.Where(match);
            return queryable;
        }
    }
}
