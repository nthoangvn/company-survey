﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using AutoMapper;
using Ninject;

namespace App.Common.Controllers
{
    public class BaseApiController : ApiController
    {
        [Inject]
        public IMapper Mapper { get; set; }


        protected void ThrowHttpResponseException(HttpStatusCode statusCode, string reason)
        {
            var message = new HttpResponseMessage
            {
                StatusCode = statusCode,
                ReasonPhrase = reason
            };
            throw new HttpResponseException(message);
        }
    }
}
