﻿using WebApi.Hal;

namespace App.Common.Controllers
{
    public static class LinkTemplates
    {

        public static class NotifikationsvorlageLink
        {
            public static Link Notifikationsvorlage => new Link("notifikationsvorlagen", "api/notifikationsvorlagen");
            public static Link View => new Link("view", "api/notifikationsvorlagen/{id}");
            public static Link Edit => new Link("edit", "api/notifikationsvorlagen/{id}");
        }
    }
}
