﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using App.Common.Constants;

namespace App.Common.Helper
{
    public static class ContextHelper
    {

        public static object Get(string contextKey)
        {

            ConcurrentDictionary<string, IDisposable> contexts = (HttpContext.Current != null
                ? HttpContext.Current.Items[CommonConstants.ContextsKey]
                : CallContext.LogicalGetData(CommonConstants.ContextsKey)) as ConcurrentDictionary<string, IDisposable>;

            IDisposable value = null;
            if (contexts != null)
            {
                contexts.TryGetValue(contextKey, out value);
            }
            return value;
        }

        public static ConcurrentDictionary<string, IDisposable> GetAll()
        {

            ConcurrentDictionary<string, IDisposable> contexts = (HttpContext.Current != null
                ? HttpContext.Current.Items[CommonConstants.ContextsKey]
                : CallContext.LogicalGetData(CommonConstants.ContextsKey)) as ConcurrentDictionary<string, IDisposable>;

            return contexts ?? new ConcurrentDictionary<string, IDisposable>();
        }

        public static void Set(string contextKey, IDisposable value)
        {
            ConcurrentDictionary<string, IDisposable> contexts;
            if (HttpContext.Current != null)
            {
                contexts = HttpContext.Current.Items[CommonConstants.ContextsKey] as ConcurrentDictionary<string, IDisposable> ??
                           new ConcurrentDictionary<string, IDisposable>();
                contexts[contextKey] = value;
                HttpContext.Current.Items[CommonConstants.ContextsKey] = contexts;
            }
            else
            {
                contexts = CallContext.LogicalGetData(CommonConstants.ContextsKey) as ConcurrentDictionary<string, IDisposable> ??
                           new ConcurrentDictionary<string, IDisposable>();
                contexts[contextKey] = value;
                CallContext.LogicalSetData(CommonConstants.ContextsKey, contexts);
            }
        }

        public static void Delete(string contextKey)
        {
            ConcurrentDictionary<string, IDisposable> contexts;
            IDisposable value;
            if (HttpContext.Current != null)
            {
                contexts = HttpContext.Current.Items[CommonConstants.ContextsKey] as ConcurrentDictionary<string, IDisposable>;
                if (contexts != null)
                {
                    contexts.TryRemove(contextKey, out value);
                    if (contexts.Keys.Count == 0)
                    {
                        HttpContext.Current.Items.Remove(CommonConstants.ContextsKey);
                    }
                }
            }
            else
            {
                contexts = CallContext.LogicalGetData(CommonConstants.ContextsKey) as ConcurrentDictionary<string, IDisposable>;
                if (contexts != null)
                {
                    contexts.TryRemove(contextKey, out value);
                    if (contexts.Keys.Count > 0)
                    {
                        CallContext.LogicalSetData(CommonConstants.ContextsKey, contexts);
                    }
                    else
                    {
                        CallContext.FreeNamedDataSlot(CommonConstants.ContextsKey);
                    }
                }
            }
        } 

    }
}
