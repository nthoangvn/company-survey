﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Common.Helper
{
    public static class StringExtensions
    {
        public static T ParseEnum<T>(this string value)
        {

            return (T)System.Enum.Parse(typeof(T), value, true);
        }
    }
}
