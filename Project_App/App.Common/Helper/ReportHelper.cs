﻿using App.Common.Constants;
// using Microsoft.Reporting.WebForms.Internal.Soap.ReportingServices2005.Execution;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Services.Protocols;
using App.Common.ReportExecutionService;
using App.Common.Dto;

namespace App.Common.Helper
{
    public static class ReportHelper
    {
        public static HttpResponseMessage CreateDownloadPDFResponse(byte[] data, string fileName)
        {
            var result = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new ByteArrayContent(data)
            };
            result.Content.Headers.ContentType =
                new MediaTypeHeaderValue(CommonConstants.FileStreamMediaTypeHeader);
//            result.Content.Headers.ContentType =
//                new MediaTypeHeaderValue("application/octet-stream");
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue(CommonConstants.AttachmentContentDispositionHeader)
            {
                FileName = fileName
            };
            return result;
        }

        public static byte[] GenerateReport(int singleId, ReportDataVO reportDataVO)
        {
            // ReportExecutionService.ReportExecutionService reportSerive = new ReportExecutionService.ReportExecutionService();
            var reportSerive = new ReportExecutionService.ReportExecutionService
            {
                Credentials = System.Net.CredentialCache.DefaultNetworkCredentials,
                ExecutionHeaderValue = new ExecutionHeader()
            };
            if (reportDataVO.format == "Excel")
            {
                reportDataVO.format = "EXCELOPENXML";
            }
//            reportSerive.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
//            reportSerive.Url = "http://desktop-hddm4va:80/ReportServer";
            byte[] result = null;
            string reportPath = "/Salary_Report/SalaryReport";
            string format = reportDataVO.format.ToUpper();
            string historyID = null;
            string devInfo = null;
            //            ParameterValue[] parameters = new ParameterValue[3];
            //            parameters[0] = new ParameterValue();
            var parameterValues = new List<ParameterValue>();
            var singleIdParam = new ParameterValue
            {
                Value = singleId.ToString(),
                Name = "SingleID"
            };
            parameterValues.Add(singleIdParam);
            DataSourceCredentials[] credentials = null;
            string showHideToggle = null;


            ExecutionInfo execInfo = new ExecutionInfo();
            ExecutionHeader execHeader = new ExecutionHeader();

            reportSerive.ExecutionHeaderValue = execHeader;
            execInfo = reportSerive.LoadReport(reportPath, historyID);

            reportSerive.SetExecutionParameters(parameterValues.ToArray(), "en-us");
            // String SessionId = rs.ExecutionHeaderValue.ExecutionID;
            string encoding;
            string mimeType;
            string extension;
            Warning[] warnings = null;
            //ParameterValue[] reportHistoryParameters = null;
            string[] streamIDs = null;

            result = reportSerive.Render(format, devInfo, out extension, out encoding, out mimeType, out warnings, out streamIDs);

            execInfo = reportSerive.GetExecutionInfo();
            return result;
        }


    }
}
