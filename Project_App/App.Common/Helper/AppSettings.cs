﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Web.Configuration;
using log4net;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace App.Common.Helper
{
    public static class AppSettings
    {
        private const string FullDateTimeFormat = "yyyy-MM-ddTHH:mm:ssZ";

        public static T Get<T>(string key, T defaultValue = default(T))
        {
            var appSetting = WebConfigurationManager.AppSettings[key];
            if (String.IsNullOrEmpty(appSetting))
            {
                return defaultValue;
            }

            var converter = TypeDescriptor.GetConverter(typeof(T));
            try
            {
                return (T)(converter.ConvertFromInvariantString(appSetting));
            }
            catch (System.Exception ex)
            {
                return defaultValue;
            }
        }

        public static JsonSerializerSettings CreateJsonSerializerSettings()
        {
            var settings = new JsonSerializerSettings();
            settings.Converters.Add(new IsoDateTimeConverter
            {
                DateTimeFormat = FullDateTimeFormat,
                DateTimeStyles = DateTimeStyles.AdjustToUniversal
            });
            // Remove null value in json string
            settings.NullValueHandling = NullValueHandling.Ignore;
            return settings;
        }
    }
}
