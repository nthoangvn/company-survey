﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Common.Constants
{
    public static class CommonConstants
    {
        public const string ContextsKey = "App.Contexts";
        public const string FileStreamMediaTypeHeader = "application/octet-stream";
        public const string AttachmentContentDispositionHeader = "attachment";
    }
}
