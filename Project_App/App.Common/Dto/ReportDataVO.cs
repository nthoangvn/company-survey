﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Common.Dto
{
    public class ReportDataVO
    {
        public string fileType { get; set; }
        public string format { get; set; }
    }
}
