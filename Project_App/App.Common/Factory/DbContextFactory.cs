﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Common.Domain;
using App.Common.Helper;

namespace App.Common.Factory
{
    public static class DbContextFactory
    {
        #region SFields

        private static readonly int _dbTimeout = AppSettings.Get("DatabaseTimeoutInSeconds", 180);

        #endregion

        #region SMethods

        /// <summary>
        /// create a new context and set the relevant attributes
        /// use it for jobs and internal tasks
        /// dispose it yourself
        /// </summary>
        /// <returns></returns>
        public static T InitializeDBContext<T>() where T : CompanyContext, new()
        {
            var dbContext = new T();
            dbContext.Database.CommandTimeout = _dbTimeout;
            dbContext.Configuration.AutoDetectChangesEnabled = true;
            dbContext.Configuration.LazyLoadingEnabled = false;
            return dbContext;
        }

        /// <summary>
        /// get context per request
        /// don't use it for jobs or internal tasks, only for requests
        /// disposed at the end of the request
        /// </summary>
        /// <returns></returns>
        public static T GetContextPerRequest<T>() where T : CompanyContext, new()
        {
            var contextKey = typeof(T).ToString();
            var dbContext = ContextHelper.Get(contextKey) as T;
            if (dbContext == null)
            {
                dbContext = InitializeDBContext<T>();
                ContextHelper.Set(contextKey, dbContext);
            }

            /*var log = EHPLogManager.GetLogger("DbContextFactory");
            dbContext.Database.Log = s => log.Info(s);*/
            return dbContext;
        }


        /// <returns></returns>
        public static void SetContextPerRequest<T>(T t) where T : CompanyContext, new()
        {
            var contextKey = typeof(T).ToString();
            ContextHelper.Set(contextKey, t);
        }


        /// <summary>
        /// Dispose all Db context in context
        /// </summary>
        public static void Close()
        {
            var dbContexts = ContextHelper.GetAll();
            var keys = dbContexts.Keys.ToList();
            foreach (var key in keys)
            {
                if (dbContexts[key] is CompanyContext)
                {
                    dbContexts[key].Dispose();
                    ContextHelper.Delete(key);
                }
            }
        }

        #endregion SMethods
    }
}
