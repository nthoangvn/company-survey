//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace App.Common
{
    using System;
    using System.Collections.Generic;
    
    public partial class BaseEntities_AccountsInSingleSurvey
    {
        public int Id { get; set; }
        public int AccountOfStaffId { get; set; }
        public int SingleSurveyId { get; set; }
        public decimal SalaryValue { get; set; }
        public byte[] RowVersion { get; set; }
    
        public virtual BaseEntities_AccountOfStaff BaseEntities_AccountOfStaff { get; set; }
        public virtual BaseEntities_SingleSurvey BaseEntities_SingleSurvey { get; set; }
    }
}
