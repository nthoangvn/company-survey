namespace App.Common.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_survey_year : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BaseEntities_TotalSurvey", "SurveyYear", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.BaseEntities_TotalSurvey", "SurveyYear");
        }
    }
}
