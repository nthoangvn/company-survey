namespace App.Common.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_Field_City : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BaseEntities_BranchCompany", "City", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.BaseEntities_BranchCompany", "City");
        }
    }
}
