namespace App.Common.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_Survey_History : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BaseEntities_SurveyHistory",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SurveyStatus = c.String(),
                        Date = c.DateTime(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        TotalSurveyId = c.Int(),
                        SingleSurveyId = c.Int(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.BaseEntities_TotalSurvey", t => t.TotalSurveyId, cascadeDelete: true)
                .ForeignKey("dbo.BaseEntities_SingleSurvey", t => t.SingleSurveyId, cascadeDelete: true)
                .Index(t => t.TotalSurveyId)
                .Index(t => t.SingleSurveyId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.BaseEntities_SurveyHistory", "SingleSurveyId", "dbo.BaseEntities_SingleSurvey");
            DropForeignKey("dbo.BaseEntities_SurveyHistory", "TotalSurveyId", "dbo.BaseEntities_TotalSurvey");
            DropIndex("dbo.BaseEntities_SurveyHistory", new[] { "SingleSurveyId" });
            DropIndex("dbo.BaseEntities_SurveyHistory", new[] { "TotalSurveyId" });
            DropTable("dbo.BaseEntities_SurveyHistory");
        }
    }
}
