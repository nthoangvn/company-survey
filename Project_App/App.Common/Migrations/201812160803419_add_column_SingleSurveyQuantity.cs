namespace App.Common.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_column_SingleSurveyQuantity : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BaseEntities_TotalSurvey", "SingleSurveyQuantity", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.BaseEntities_TotalSurvey", "SingleSurveyQuantity");
        }
    }
}
