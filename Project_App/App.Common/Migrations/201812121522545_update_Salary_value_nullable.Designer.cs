// <auto-generated />
namespace App.Common.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class update_Salary_value_nullable : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(update_Salary_value_nullable));
        
        string IMigrationMetadata.Id
        {
            get { return "201812121522545_update_Salary_value_nullable"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
