namespace App.Common.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_SurveyName_To_SurveyHistory : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BaseEntities_SurveyHistory", "SurveyName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.BaseEntities_SurveyHistory", "SurveyName");
        }
    }
}
