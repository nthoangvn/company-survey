namespace App.Common.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_TimeLine_For_SingleSurvey : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BaseEntities_SingleSurvey", "FillingTime", c => c.DateTime());
            AddColumn("dbo.BaseEntities_SingleSurvey", "SubmitTime", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.BaseEntities_SingleSurvey", "SubmitTime");
            DropColumn("dbo.BaseEntities_SingleSurvey", "FillingTime");
        }
    }
}
