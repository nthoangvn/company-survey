namespace App.Common.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class update_Salary_value_nullable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.BaseEntities_AccountsInSingleSurvey", "SalaryValue", c => c.Decimal(precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.BaseEntities_AccountsInSingleSurvey", "SalaryValue", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
    }
}
