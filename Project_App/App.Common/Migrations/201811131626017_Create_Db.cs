namespace App.Common.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Create_Db : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BaseEntities_AccountOfStaff",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(nullable: false),
                        LastName = c.String(nullable: false),
                        StaffVisa = c.String(nullable: false),
                        Phone = c.String(nullable: false),
                        Activated = c.Boolean(nullable: false),
                        StartDate = c.DateTime(nullable: false),
                        LeaveDate = c.DateTime(),
                        Position = c.String(nullable: false),
                        DepartmentId = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.BaseEntities_Department", t => t.DepartmentId)
                .Index(t => t.DepartmentId);
            
            CreateTable(
                "dbo.BaseEntities_AccountsInSingleSurvey",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AccountOfStaffId = c.Int(nullable: false),
                        SingleSurveyId = c.Int(nullable: false),
                        SalaryValue = c.Decimal(nullable: false, precision: 18, scale: 2),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.BaseEntities_SingleSurvey", t => t.SingleSurveyId)
                .ForeignKey("dbo.BaseEntities_AccountOfStaff", t => t.AccountOfStaffId)
                .Index(t => t.AccountOfStaffId)
                .Index(t => t.SingleSurveyId);
            
            CreateTable(
                "dbo.BaseEntities_SingleSurvey",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Status = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(nullable: false),
                        CloseDate = c.DateTime(),
                        DirectorId = c.Int(),
                        TotalSurveyId = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.BaseEntities_Director", t => t.DirectorId)
                .ForeignKey("dbo.BaseEntities_TotalSurvey", t => t.TotalSurveyId)
                .Index(t => t.DirectorId)
                .Index(t => t.TotalSurveyId);
            
            CreateTable(
                "dbo.BaseEntities_Director",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(nullable: false),
                        LastName = c.String(nullable: false),
                        Email = c.String(nullable: false),
                        Phone = c.String(nullable: false),
                        Role = c.String(maxLength: 200),
                        IsActive = c.Boolean(nullable: false),
                        IdentityUserId = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.BaseEntities_BranchCompany",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CountryName = c.String(nullable: false),
                        EstablishDate = c.DateTime(nullable: false),
                        Address = c.String(nullable: false),
                        Director_Id = c.Int(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.BaseEntities_Director", t => t.Director_Id)
                .Index(t => t.Director_Id);
            
            CreateTable(
                "dbo.BaseEntities_Department",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        BranchCompanyId = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.BaseEntities_BranchCompany", t => t.BranchCompanyId)
                .Index(t => t.BranchCompanyId);
            
            CreateTable(
                "dbo.BaseEntities_IdentityUser",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Password = c.String(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.BaseEntities_Director", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.BaseEntities_TotalSurvey",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Status = c.String(maxLength: 200),
                        SurveyName = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        CloseDate = c.DateTime(),
                        DirectorId = c.Int(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.BaseEntities_Director", t => t.DirectorId)
                .Index(t => t.DirectorId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.BaseEntities_AccountsInSingleSurvey", "AccountOfStaffId", "dbo.BaseEntities_AccountOfStaff");
            DropForeignKey("dbo.BaseEntities_TotalSurvey", "DirectorId", "dbo.BaseEntities_Director");
            DropForeignKey("dbo.BaseEntities_SingleSurvey", "TotalSurveyId", "dbo.BaseEntities_TotalSurvey");
            DropForeignKey("dbo.BaseEntities_SingleSurvey", "DirectorId", "dbo.BaseEntities_Director");
            DropForeignKey("dbo.BaseEntities_IdentityUser", "Id", "dbo.BaseEntities_Director");
            DropForeignKey("dbo.BaseEntities_BranchCompany", "Director_Id", "dbo.BaseEntities_Director");
            DropForeignKey("dbo.BaseEntities_Department", "BranchCompanyId", "dbo.BaseEntities_BranchCompany");
            DropForeignKey("dbo.BaseEntities_AccountOfStaff", "DepartmentId", "dbo.BaseEntities_Department");
            DropForeignKey("dbo.BaseEntities_AccountsInSingleSurvey", "SingleSurveyId", "dbo.BaseEntities_SingleSurvey");
            DropIndex("dbo.BaseEntities_TotalSurvey", new[] { "DirectorId" });
            DropIndex("dbo.BaseEntities_IdentityUser", new[] { "Id" });
            DropIndex("dbo.BaseEntities_Department", new[] { "BranchCompanyId" });
            DropIndex("dbo.BaseEntities_BranchCompany", new[] { "Director_Id" });
            DropIndex("dbo.BaseEntities_SingleSurvey", new[] { "TotalSurveyId" });
            DropIndex("dbo.BaseEntities_SingleSurvey", new[] { "DirectorId" });
            DropIndex("dbo.BaseEntities_AccountsInSingleSurvey", new[] { "SingleSurveyId" });
            DropIndex("dbo.BaseEntities_AccountsInSingleSurvey", new[] { "AccountOfStaffId" });
            DropIndex("dbo.BaseEntities_AccountOfStaff", new[] { "DepartmentId" });
            DropTable("dbo.BaseEntities_TotalSurvey");
            DropTable("dbo.BaseEntities_IdentityUser");
            DropTable("dbo.BaseEntities_Department");
            DropTable("dbo.BaseEntities_BranchCompany");
            DropTable("dbo.BaseEntities_Director");
            DropTable("dbo.BaseEntities_SingleSurvey");
            DropTable("dbo.BaseEntities_AccountsInSingleSurvey");
            DropTable("dbo.BaseEntities_AccountOfStaff");
        }
    }
}
