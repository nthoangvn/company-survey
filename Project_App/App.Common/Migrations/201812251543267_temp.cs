namespace App.Common.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class temp : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.BaseEntities_SingleSurvey", "BranchCompanyId");
            RenameColumn(table: "dbo.BaseEntities_SingleSurvey", name: "BaseEntities_BranchCompany_Id", newName: "BranchCompanyId");
            RenameIndex(table: "dbo.BaseEntities_SingleSurvey", name: "IX_BaseEntities_BranchCompany_Id", newName: "IX_BranchCompanyId");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.BaseEntities_SingleSurvey", name: "IX_BranchCompanyId", newName: "IX_BaseEntities_BranchCompany_Id");
            RenameColumn(table: "dbo.BaseEntities_SingleSurvey", name: "BranchCompanyId", newName: "BaseEntities_BranchCompany_Id");
            AddColumn("dbo.BaseEntities_SingleSurvey", "BranchCompanyId", c => c.Int(nullable: false));
        }
    }
}
