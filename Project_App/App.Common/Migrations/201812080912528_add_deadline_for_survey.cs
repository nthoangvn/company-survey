namespace App.Common.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_deadline_for_survey : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BaseEntities_TotalSurvey", "BranchCompanyIdList", c => c.String());
            AddColumn("dbo.BaseEntities_TotalSurvey", "PublishDeadline", c => c.DateTime(nullable: false));
            AddColumn("dbo.BaseEntities_TotalSurvey", "SubmitDeadline", c => c.DateTime(nullable: false));
            AddColumn("dbo.BaseEntities_TotalSurvey", "ValidateDeadline", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.BaseEntities_TotalSurvey", "ValidateDeadline");
            DropColumn("dbo.BaseEntities_TotalSurvey", "SubmitDeadline");
            DropColumn("dbo.BaseEntities_TotalSurvey", "PublishDeadline");
            DropColumn("dbo.BaseEntities_TotalSurvey", "BranchCompanyIdList");
        }
    }
}
