namespace App.Common.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class change_relationship_singleSurvey : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.BaseEntities_SingleSurvey", "DirectorId", "dbo.BaseEntities_Director");
            DropIndex("dbo.BaseEntities_SingleSurvey", new[] { "DirectorId" });
            AddColumn("dbo.BaseEntities_SingleSurvey", "BranchCompanyId", c => c.Int(nullable: false));
            AddColumn("dbo.BaseEntities_SingleSurvey", "BaseEntities_BranchCompany_Id", c => c.Int(nullable: false));
            CreateIndex("dbo.BaseEntities_SingleSurvey", "BaseEntities_BranchCompany_Id");
            AddForeignKey("dbo.BaseEntities_SingleSurvey", "BaseEntities_BranchCompany_Id", "dbo.BaseEntities_BranchCompany", "Id", cascadeDelete: true);
            DropColumn("dbo.BaseEntities_SingleSurvey", "DirectorId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.BaseEntities_SingleSurvey", "DirectorId", c => c.Int());
            DropForeignKey("dbo.BaseEntities_SingleSurvey", "BaseEntities_BranchCompany_Id", "dbo.BaseEntities_BranchCompany");
            DropIndex("dbo.BaseEntities_SingleSurvey", new[] { "BaseEntities_BranchCompany_Id" });
            DropColumn("dbo.BaseEntities_SingleSurvey", "BaseEntities_BranchCompany_Id");
            DropColumn("dbo.BaseEntities_SingleSurvey", "BranchCompanyId");
            CreateIndex("dbo.BaseEntities_SingleSurvey", "DirectorId");
            AddForeignKey("dbo.BaseEntities_SingleSurvey", "DirectorId", "dbo.BaseEntities_Director", "Id");
        }
    }
}
