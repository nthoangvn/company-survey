
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 11/13/2018 20:20:13
-- Generated from EDMX file: C:\Users\ASUS\Documents\company-survey\Project_App\App.Common\MultiNationalModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [MultiNationalCompanyDb];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------


-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'BaseEntities'
CREATE TABLE [dbo].[BaseEntities] (
    [Id] int IDENTITY(1,1) NOT NULL
);
GO

-- Creating table 'BaseEntities_AccountOfStaff'
CREATE TABLE [dbo].[BaseEntities_AccountOfStaff] (
    [FirstName] nvarchar(max)  NOT NULL,
    [LastName] nvarchar(max)  NOT NULL,
    [StaffVisa] nvarchar(max)  NOT NULL,
    [Phone] nvarchar(max)  NOT NULL,
    [Activated] bit  NOT NULL,
    [StartDate] datetime  NOT NULL,
    [LeaveDate] datetime  NULL,
    [Position] nvarchar(max)  NOT NULL,
    [DepartmentId] int  NOT NULL,
    [Id] int  NOT NULL
);
GO

-- Creating table 'BaseEntities_AccountsInSingleSurvey'
CREATE TABLE [dbo].[BaseEntities_AccountsInSingleSurvey] (
    [AccountOfStaffId] int  NOT NULL,
    [SingleSurveyId] int  NOT NULL,
    [SalaryValue] nvarchar(max)  NOT NULL,
    [Id] int  NOT NULL
);
GO

-- Creating table 'BaseEntities_Survey'
CREATE TABLE [dbo].[BaseEntities_Survey] (
    [SurveyName] nvarchar(max)  NOT NULL,
    [Id] int  NOT NULL
);
GO

-- Creating table 'BaseEntities_SingleSurvey'
CREATE TABLE [dbo].[BaseEntities_SingleSurvey] (
    [Status] int  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [CloseDate] datetime  NULL,
    [DirectorId] int  NULL,
    [TotalSurveyId] int  NOT NULL,
    [Id] int  NOT NULL
);
GO

-- Creating table 'BaseEntities_Department'
CREATE TABLE [dbo].[BaseEntities_Department] (
    [Name] nvarchar(max)  NOT NULL,
    [BranchCompanyId] int  NOT NULL,
    [Id] int  NOT NULL
);
GO

-- Creating table 'BaseEntities_BranchCompany'
CREATE TABLE [dbo].[BaseEntities_BranchCompany] (
    [CountryName] nvarchar(max)  NOT NULL,
    [EstablishDate] datetime  NOT NULL,
    [Address] nvarchar(max)  NOT NULL,
    [Id] int  NOT NULL,
    [Director_Id] int  NULL
);
GO

-- Creating table 'BaseEntities_Director'
CREATE TABLE [dbo].[BaseEntities_Director] (
    [FirstName] nvarchar(max)  NOT NULL,
    [LastName] nvarchar(max)  NOT NULL,
    [Email] nvarchar(max)  NOT NULL,
    [Phone] nvarchar(max)  NOT NULL,
    [Role] int  NOT NULL,
    [IsActive] bit  NOT NULL,
    [Id] int  NOT NULL,
    [IdentityUser_Id] int  NOT NULL
);
GO

-- Creating table 'BaseEntities_IdentityUser'
CREATE TABLE [dbo].[BaseEntities_IdentityUser] (
    [Password] nvarchar(max)  NOT NULL,
    [Id] int  NOT NULL
);
GO

-- Creating table 'BaseEntities_TotalSurvey'
CREATE TABLE [dbo].[BaseEntities_TotalSurvey] (
    [Status] int  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [CloseDate] datetime  NULL,
    [DirectorId] int  NULL,
    [Id] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'BaseEntities'
ALTER TABLE [dbo].[BaseEntities]
ADD CONSTRAINT [PK_BaseEntities]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'BaseEntities_AccountOfStaff'
ALTER TABLE [dbo].[BaseEntities_AccountOfStaff]
ADD CONSTRAINT [PK_BaseEntities_AccountOfStaff]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'BaseEntities_AccountsInSingleSurvey'
ALTER TABLE [dbo].[BaseEntities_AccountsInSingleSurvey]
ADD CONSTRAINT [PK_BaseEntities_AccountsInSingleSurvey]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'BaseEntities_Survey'
ALTER TABLE [dbo].[BaseEntities_Survey]
ADD CONSTRAINT [PK_BaseEntities_Survey]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'BaseEntities_SingleSurvey'
ALTER TABLE [dbo].[BaseEntities_SingleSurvey]
ADD CONSTRAINT [PK_BaseEntities_SingleSurvey]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'BaseEntities_Department'
ALTER TABLE [dbo].[BaseEntities_Department]
ADD CONSTRAINT [PK_BaseEntities_Department]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'BaseEntities_BranchCompany'
ALTER TABLE [dbo].[BaseEntities_BranchCompany]
ADD CONSTRAINT [PK_BaseEntities_BranchCompany]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'BaseEntities_Director'
ALTER TABLE [dbo].[BaseEntities_Director]
ADD CONSTRAINT [PK_BaseEntities_Director]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'BaseEntities_IdentityUser'
ALTER TABLE [dbo].[BaseEntities_IdentityUser]
ADD CONSTRAINT [PK_BaseEntities_IdentityUser]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'BaseEntities_TotalSurvey'
ALTER TABLE [dbo].[BaseEntities_TotalSurvey]
ADD CONSTRAINT [PK_BaseEntities_TotalSurvey]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [AccountOfStaffId] in table 'BaseEntities_AccountsInSingleSurvey'
ALTER TABLE [dbo].[BaseEntities_AccountsInSingleSurvey]
ADD CONSTRAINT [FK_AccountOfStaffAccountsInSingleSurvey]
    FOREIGN KEY ([AccountOfStaffId])
    REFERENCES [dbo].[BaseEntities_AccountOfStaff]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_AccountOfStaffAccountsInSingleSurvey'
CREATE INDEX [IX_FK_AccountOfStaffAccountsInSingleSurvey]
ON [dbo].[BaseEntities_AccountsInSingleSurvey]
    ([AccountOfStaffId]);
GO

-- Creating foreign key on [SingleSurveyId] in table 'BaseEntities_AccountsInSingleSurvey'
ALTER TABLE [dbo].[BaseEntities_AccountsInSingleSurvey]
ADD CONSTRAINT [FK_SingleSurveyAccountsInSingleSurvey]
    FOREIGN KEY ([SingleSurveyId])
    REFERENCES [dbo].[BaseEntities_SingleSurvey]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_SingleSurveyAccountsInSingleSurvey'
CREATE INDEX [IX_FK_SingleSurveyAccountsInSingleSurvey]
ON [dbo].[BaseEntities_AccountsInSingleSurvey]
    ([SingleSurveyId]);
GO

-- Creating foreign key on [DepartmentId] in table 'BaseEntities_AccountOfStaff'
ALTER TABLE [dbo].[BaseEntities_AccountOfStaff]
ADD CONSTRAINT [FK_DepartmentAccountOfStaff]
    FOREIGN KEY ([DepartmentId])
    REFERENCES [dbo].[BaseEntities_Department]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_DepartmentAccountOfStaff'
CREATE INDEX [IX_FK_DepartmentAccountOfStaff]
ON [dbo].[BaseEntities_AccountOfStaff]
    ([DepartmentId]);
GO

-- Creating foreign key on [BranchCompanyId] in table 'BaseEntities_Department'
ALTER TABLE [dbo].[BaseEntities_Department]
ADD CONSTRAINT [FK_BranchCompanyDepartment]
    FOREIGN KEY ([BranchCompanyId])
    REFERENCES [dbo].[BaseEntities_BranchCompany]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_BranchCompanyDepartment'
CREATE INDEX [IX_FK_BranchCompanyDepartment]
ON [dbo].[BaseEntities_Department]
    ([BranchCompanyId]);
GO

-- Creating foreign key on [Director_Id] in table 'BaseEntities_BranchCompany'
ALTER TABLE [dbo].[BaseEntities_BranchCompany]
ADD CONSTRAINT [FK_DirectorBranchCompany]
    FOREIGN KEY ([Director_Id])
    REFERENCES [dbo].[BaseEntities_Director]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_DirectorBranchCompany'
CREATE INDEX [IX_FK_DirectorBranchCompany]
ON [dbo].[BaseEntities_BranchCompany]
    ([Director_Id]);
GO

-- Creating foreign key on [IdentityUser_Id] in table 'BaseEntities_Director'
ALTER TABLE [dbo].[BaseEntities_Director]
ADD CONSTRAINT [FK_DirectorIdentityUser]
    FOREIGN KEY ([IdentityUser_Id])
    REFERENCES [dbo].[BaseEntities_IdentityUser]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_DirectorIdentityUser'
CREATE INDEX [IX_FK_DirectorIdentityUser]
ON [dbo].[BaseEntities_Director]
    ([IdentityUser_Id]);
GO

-- Creating foreign key on [DirectorId] in table 'BaseEntities_TotalSurvey'
ALTER TABLE [dbo].[BaseEntities_TotalSurvey]
ADD CONSTRAINT [FK_DirectorTotalSurvey]
    FOREIGN KEY ([DirectorId])
    REFERENCES [dbo].[BaseEntities_Director]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_DirectorTotalSurvey'
CREATE INDEX [IX_FK_DirectorTotalSurvey]
ON [dbo].[BaseEntities_TotalSurvey]
    ([DirectorId]);
GO

-- Creating foreign key on [DirectorId] in table 'BaseEntities_SingleSurvey'
ALTER TABLE [dbo].[BaseEntities_SingleSurvey]
ADD CONSTRAINT [FK_DirectorSingleSurvey]
    FOREIGN KEY ([DirectorId])
    REFERENCES [dbo].[BaseEntities_Director]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_DirectorSingleSurvey'
CREATE INDEX [IX_FK_DirectorSingleSurvey]
ON [dbo].[BaseEntities_SingleSurvey]
    ([DirectorId]);
GO

-- Creating foreign key on [TotalSurveyId] in table 'BaseEntities_SingleSurvey'
ALTER TABLE [dbo].[BaseEntities_SingleSurvey]
ADD CONSTRAINT [FK_TotalSurveySingleSurvey]
    FOREIGN KEY ([TotalSurveyId])
    REFERENCES [dbo].[BaseEntities_TotalSurvey]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_TotalSurveySingleSurvey'
CREATE INDEX [IX_FK_TotalSurveySingleSurvey]
ON [dbo].[BaseEntities_SingleSurvey]
    ([TotalSurveyId]);
GO

-- Creating foreign key on [Id] in table 'BaseEntities_AccountOfStaff'
ALTER TABLE [dbo].[BaseEntities_AccountOfStaff]
ADD CONSTRAINT [FK_AccountOfStaff_inherits_BaseEntity]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[BaseEntities]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Id] in table 'BaseEntities_AccountsInSingleSurvey'
ALTER TABLE [dbo].[BaseEntities_AccountsInSingleSurvey]
ADD CONSTRAINT [FK_AccountsInSingleSurvey_inherits_BaseEntity]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[BaseEntities]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Id] in table 'BaseEntities_Survey'
ALTER TABLE [dbo].[BaseEntities_Survey]
ADD CONSTRAINT [FK_Survey_inherits_BaseEntity]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[BaseEntities]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Id] in table 'BaseEntities_SingleSurvey'
ALTER TABLE [dbo].[BaseEntities_SingleSurvey]
ADD CONSTRAINT [FK_SingleSurvey_inherits_Survey]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[BaseEntities_Survey]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Id] in table 'BaseEntities_Department'
ALTER TABLE [dbo].[BaseEntities_Department]
ADD CONSTRAINT [FK_Department_inherits_BaseEntity]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[BaseEntities]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Id] in table 'BaseEntities_BranchCompany'
ALTER TABLE [dbo].[BaseEntities_BranchCompany]
ADD CONSTRAINT [FK_BranchCompany_inherits_BaseEntity]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[BaseEntities]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Id] in table 'BaseEntities_Director'
ALTER TABLE [dbo].[BaseEntities_Director]
ADD CONSTRAINT [FK_Director_inherits_BaseEntity]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[BaseEntities]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Id] in table 'BaseEntities_IdentityUser'
ALTER TABLE [dbo].[BaseEntities_IdentityUser]
ADD CONSTRAINT [FK_IdentityUser_inherits_BaseEntity]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[BaseEntities]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Id] in table 'BaseEntities_TotalSurvey'
ALTER TABLE [dbo].[BaseEntities_TotalSurvey]
ADD CONSTRAINT [FK_TotalSurvey_inherits_Survey]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[BaseEntities_Survey]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------