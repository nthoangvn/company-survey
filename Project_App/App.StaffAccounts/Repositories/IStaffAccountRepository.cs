﻿using System.Collections.Generic;
using App.Common.Repositories;
using App.Common.Domain;

namespace App.StaffAccounts.Repositories
{
    public interface IStaffAccountRepository : IGenericRepository<BaseEntities_AccountOfStaff>
    {
        /// <summary>
        /// Get All Account By BranchCompany Id
        /// </summary>
        /// <param name="branchCompanyId"></param>
        /// <returns></returns>
        IList<BaseEntities_AccountOfStaff> GetAllAccountByBranchCompany(int branchCompanyId);

        BaseEntities_AccountOfStaff GetAccountById(int accountId);

        IList<BaseEntities_AccountOfStaff> GetAccounts();

        /// <summary>
        /// Get Staff Of Department List
        /// </summary>
        /// <param name="departmentIdList"></param>
        /// <returns></returns>
        List<BaseEntities_AccountOfStaff> GetStaffOfDepartmentList(List<int> departmentIdList);

        /// <summary>
        /// Get Staff Accounts By IdList
        /// </summary>
        /// <param name="staffIds"></param>
        /// <returns></returns>
        List<BaseEntities_AccountOfStaff> GetActivatedStaffAccountsByIdList(List<int> staffIds);

        void AddOrUpdateStaffAccount(BaseEntities_AccountOfStaff staffAccount);
        BaseEntities_AccountOfStaff GetStaffAccountById(int staffAccountId);
        void DeleteStaffAccount(BaseEntities_AccountOfStaff staffAccount);
    }
}
