﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Common.Domain;

namespace App.StaffAccounts.Repositories
{
    [Serializable]
    public class StaffDbContext : CompanyContext
    {
        protected override string Schema => "App.StaffAccounts";
    }
}
