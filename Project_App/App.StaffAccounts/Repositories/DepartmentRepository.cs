﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Common.Domain;
using App.Common.Repositories;

namespace App.StaffAccounts.Repositories
{
    public class DepartmentRepository : StaffGenericRepository<BaseEntities_Department>, IDepartmentRepository
    {
//        public DepartmentRepository(CompanyContext dbContext) : base(dbContext)
//        {
//        }

        public List<BaseEntities_Department> FindAllDepartmentOfCompanyList(List<int> branchIds)
        {
            var departmentList = FindAll(d => branchIds.Contains(d.BranchCompanyId), 
                d => d.BaseEntities_AccountOfStaff, d => d.BaseEntities_BranchCompany).ToList();
            return departmentList;
        }

        public BaseEntities_Department FindDepartmentByDepartmentId(int branchId)
        {
            var department = Find(d => branchId == d.Id);
            return department;
        }

        public IList<BaseEntities_Department> FindDepartmentByBranchId(int branchId)
        {
            return FindAll(d => branchId == d.BranchCompanyId);
        }
    }
}
