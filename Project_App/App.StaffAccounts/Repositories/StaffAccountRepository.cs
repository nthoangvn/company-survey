﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using App.Common.Domain;
using App.Common.Repositories;

namespace App.StaffAccounts.Repositories
{
    public class StaffAccountsRepository : StaffGenericRepository<BaseEntities_AccountOfStaff>, IStaffAccountRepository
    {
//        public StaffAccountsRepository(CompanyContext dbContext) : base(dbContext)
//        {
//        }

        public IList<BaseEntities_AccountOfStaff> GetAllAccountByBranchCompany(int branchCompanyId)
        {
            var accountList = FindAll(a => a.BaseEntities_Department.BranchCompanyId == branchCompanyId);
            return accountList;
        }

        public BaseEntities_AccountOfStaff GetAccountById(int accountId)
        {
            var account = Find(a => a.Id == accountId);
            return account;
        }

        public IList<BaseEntities_AccountOfStaff> GetAccounts()
        {
            var account = FindAll();
            return account;
        }

        public List<BaseEntities_AccountOfStaff> GetStaffOfDepartmentList(List<int> departmentIdList)
        {
            return FindAll(s => departmentIdList.Contains(s.DepartmentId)).OrderBy(s => s.DepartmentId).ToList();
        }

        public BaseEntities_AccountOfStaff GetStaffAccountById(int staffAccountId)
        {
            return Find(s => s.Id == staffAccountId);
        }

        public List<BaseEntities_AccountOfStaff> GetActivatedStaffAccountsByIdList(List<int> staffIds)
        {
            return FindAll(s => staffIds.Contains(s.Id) && s.Activated && !s.LeaveDate.HasValue, 
                s => s.BaseEntities_Department)
                .OrderBy(s => s.DepartmentId).ToList();
        }

        public void AddOrUpdateStaffAccount(BaseEntities_AccountOfStaff staffAccount)
        {
            AddOrUpdate(staffAccount);
            Commit();
        }

        public void DeleteStaffAccount(BaseEntities_AccountOfStaff staffAccount)
        {
            Delete(staffAccount);
            Commit();
        }
    }
}
