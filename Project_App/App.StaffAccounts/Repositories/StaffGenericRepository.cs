﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Auth.Repositories;
using App.Common.Domain;
using App.Common.Factory;
using App.Common.Repositories;
using App.StaffAccounts.Repositories;

namespace App.StaffAccounts.Repositories
{
    public class StaffGenericRepository<T> : GenericRepository<T, StaffDbContext>, IStaffGenericRepository<T> where T : BaseEntity
    {
        public override StaffDbContext DbContext => DbContextFactory.GetContextPerRequest<StaffDbContext>();

        public void SaveChangeWithDisableDetectChange()
        {
            DbContext.Configuration.AutoDetectChangesEnabled = false;
            Commit();
            DbContext.Configuration.AutoDetectChangesEnabled = true;
        }
    }
}
