﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Common.Domain;
using App.Common.Repositories;

namespace App.StaffAccounts.Repositories
{
    public interface IDepartmentRepository : IGenericRepository<BaseEntities_Department>
    {
        /// <summary>
        /// Find All Department Of Company 
        /// </summary>
        /// <param name="branchIds"></param>
        /// <returns></returns>
        List<BaseEntities_Department> FindAllDepartmentOfCompanyList(List<int> branchIds);
        IList<BaseEntities_Department> FindDepartmentByBranchId(int branchId);
        BaseEntities_Department FindDepartmentByDepartmentId(int departmentId);
    }
}
