﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Common.Domain;
namespace App.StaffAccounts.Services
{
    public interface IStaffAccountService
    {
        /// <summary>
        /// Get Single Survey By Branch Director ID
        /// </summary>
        /// <param name="staffAccountId"></param>
        /// <returns></returns>
        BaseEntities_AccountOfStaff GetStaffAccountById(int staffAccountId);
        IList<BaseEntities_AccountOfStaff> GetStaffAccounts();
        IList<BaseEntities_AccountOfStaff> GetStaffAccountsByDepartmentId(int departmentId);

        /// <summary>
        /// Get Staff Of Department List
        /// </summary>
        /// <param name="departmentIdList"></param>
        /// <returns></returns>
        List<BaseEntities_AccountOfStaff> GetStaffOfDepartmentList(List<int> departmentIdList);

        /// <summary>
        /// Get Staff Of Department Dictionary
        /// </summary>
        /// <param name="departmentIdList"></param>
        /// <returns></returns>
        Dictionary<int, List<BaseEntities_AccountOfStaff>> GetStaffOfDepartmentDic(List<int> departmentIdList);

        /// <summary>
        /// Get Activated Staff Accounts By IdList
        /// </summary>
        /// <param name="staffIds"></param>
        /// <returns></returns>
        List<BaseEntities_AccountOfStaff> GetActivatedStaffAccountsByIdList(List<int> staffIds);

        BaseEntities_AccountOfStaff CreateStaffAccount(BaseEntities_AccountOfStaff staffAccount);
        BaseEntities_AccountOfStaff UpdateStaffAccount(BaseEntities_AccountOfStaff staffAccount);
        bool DeleteStaffAccount(int staffAccountId);
    }
}
