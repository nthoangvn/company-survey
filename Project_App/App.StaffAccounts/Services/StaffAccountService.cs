﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using App.Common;
using App.StaffAccounts.Repositories;
using App.Common.Domain;
using App.StaffAccounts.Services;
using BaseEntities_AccountOfStaff = App.Common.Domain.BaseEntities_AccountOfStaff;
using BaseEntities_Department = App.Common.Domain.BaseEntities_Department;

namespace App.StaffAccounts.Services
{
    public class StaffAccountService : IStaffAccountService
    {
        private readonly IStaffAccountRepository _staffAccountRepository;
        private readonly IDepartmentRepository _departmentRepository;

        public StaffAccountService(IStaffAccountRepository staffAccountRepository, IDepartmentRepository departmentRepository)
        {
            _staffAccountRepository = staffAccountRepository;
            _departmentRepository = departmentRepository;
        }

        public BaseEntities_AccountOfStaff GetStaffAccountById(int staffAccountId)
        {
            return _staffAccountRepository.GetAccountById(staffAccountId);
        }

        public IList<BaseEntities_AccountOfStaff> GetStaffAccounts()
        {
            return _staffAccountRepository.GetAccounts().ToList();
        }

        public IList<BaseEntities_AccountOfStaff> GetStaffAccountsByDepartmentId(int departmentId)
        {
            return _staffAccountRepository.GetAllAccountByBranchCompany(departmentId);
        }

        public List<BaseEntities_AccountOfStaff> GetStaffOfDepartmentList(List<int> departmentIdList)
        {
            return _staffAccountRepository.GetStaffOfDepartmentList(departmentIdList);
        }

        public Dictionary<int, List<BaseEntities_AccountOfStaff>> GetStaffOfDepartmentDic(List<int> branchIds)
        {
            var departmentList = _departmentRepository.FindAll(d => branchIds.Contains(d.BranchCompanyId),
                d => d.BaseEntities_AccountOfStaff, d => d.BaseEntities_BranchCompany).ToList();
            var result = new Dictionary<int, List<BaseEntities_AccountOfStaff>>();
            // var staffList = departmentList.ToDictionary(k => k.BranchCompanyId, v => v.BaseEntities_AccountOfStaff.ToList());
            if (!departmentList.Any())
            {
                return null;
            }
            foreach (var department in departmentList)
            {
                if (result.ContainsKey(department.BranchCompanyId))
                {
                    result[department.BranchCompanyId].AddRange(department.BaseEntities_AccountOfStaff.ToList());
                }
                else
                {
                    result.Add(department.BranchCompanyId, department.BaseEntities_AccountOfStaff.ToList());
                }
                
            }

            return result;
        }

        public List<BaseEntities_AccountOfStaff> GetActivatedStaffAccountsByIdList(List<int> staffIds)
        {
            return _staffAccountRepository.GetActivatedStaffAccountsByIdList(staffIds);
        }

        public BaseEntities_AccountOfStaff CreateStaffAccount(BaseEntities_AccountOfStaff staffAccount)
        {
            BaseEntities_Department department = _departmentRepository.FindDepartmentByDepartmentId(staffAccount.DepartmentId);
            if (department == null)
            {
                return null;
            }

            staffAccount.Activated = true;
            _staffAccountRepository.AddOrUpdateStaffAccount(staffAccount);
            return staffAccount;
        }

        public BaseEntities_AccountOfStaff UpdateStaffAccount(BaseEntities_AccountOfStaff staffAccount)
        {
            BaseEntities_Department department = _departmentRepository.FindDepartmentByDepartmentId(staffAccount.DepartmentId);
            if (department == null)
            {
                return null;
            }

            _staffAccountRepository.AddOrUpdateStaffAccount(staffAccount);
            return staffAccount;
        }

        public bool DeleteStaffAccount(int staffAccountId)
        {
            BaseEntities_AccountOfStaff staffAccount = _staffAccountRepository.GetStaffAccountById(staffAccountId);
            if (staffAccount == null)
            {
                return false;
            }

            _staffAccountRepository.DeleteStaffAccount(staffAccount);
            return true;
        }
    }
}
