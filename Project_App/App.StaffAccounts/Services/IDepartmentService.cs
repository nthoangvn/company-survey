﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Common.Domain;

namespace App.StaffAccounts.Services
{
    public interface IDepartmentService
    {
        Dictionary<int, List<BaseEntities_AccountOfStaff>> GetStaffOfBranchCompanyDic(List<int> branchIds);
        IList<BaseEntities_Department> GetDepartmentsByBranchId(int branchId);
    }
}
