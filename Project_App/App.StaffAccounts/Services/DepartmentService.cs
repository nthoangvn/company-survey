﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Common.Domain;
using App.StaffAccounts.Repositories;

namespace App.StaffAccounts.Services
{
    public class DepartmentService : IDepartmentService
    {
        private readonly IDepartmentRepository _departmentRepository;
        private readonly IStaffAccountService _staffAccountService;

        public DepartmentService(IDepartmentRepository departmentRepository, IStaffAccountService staffAccountService)
        {
            _departmentRepository = departmentRepository;
            _staffAccountService = staffAccountService;
        }

        public Dictionary<int, List<BaseEntities_AccountOfStaff>> GetStaffOfBranchCompanyDic(List<int> branchIds)
        {
//            var result = new Dictionary<int, List<BaseEntities_AccountOfStaff>>();
//            var allDepartments = _departmentRepository.FindAllDepartmentOfCompanyList(branchIds);
//            if (allDepartments.Count == 0 || !allDepartments.All(u => u.BaseEntities_BranchCompany.Director_Id.HasValue))
//            {
//                return new Dictionary<int, List<BaseEntities_AccountOfStaff>>();
//            }

            var accountListOfDepartmentList =
                _staffAccountService.GetStaffOfDepartmentDic(branchIds);
            // accountListOfDepartmentList.ToDictionary(k => k.)
//            foreach (var department in allDepartments)
//            {
//                if (accountListOfDepartmentList.ContainsKey(department.Id))
//                {
//                    if (department.BaseEntities_BranchCompany.Director_Id != null 
//                        && result.ContainsKey(department.BaseEntities_BranchCompany.Director_Id.Value))
//                    {
//                        result[department.BaseEntities_BranchCompany.Director_Id.Value].AddRange(accountListOfDepartmentList[department.Id]);
//                    }
//                    else
//                    {
//                        if (department.BaseEntities_BranchCompany.Director_Id != null)
//                        {
//                            result.Add(department.BaseEntities_BranchCompany.Director_Id.Value,
//                                accountListOfDepartmentList[department.Id]);
//                        }
//                            
//                    }
//                    
//                }
//            }
            return accountListOfDepartmentList;
        }

        public IList<BaseEntities_Department> GetDepartmentsByBranchId(int branchId)
        {
            var departments = _departmentRepository.FindDepartmentByBranchId(branchId);
            return departments;
        }
    }
}
