namespace App.Common.Domain
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class DepartmentRepresentation
    {
        
        public DepartmentRepresentation()
        {
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public int BranchCompanyId { get; set; }
    }
}
