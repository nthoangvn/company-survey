﻿//using System;
//using System.Collections.Generic;
//using App.Common.Controllers;
//using EHP.ChartOfAccounts.Controllers;
//using EHP.Common.Enum;
//using EHP.Common.Resources;
//using WebApi.Hal;

using System;
using WebApi.Hal;

namespace App.StaffAccounts.Representations
{
    public class StaffAccountRepresentation
    {
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string StaffVisa { get; set; }

        public string Phone { get; set; }

        public bool Activated { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime? LeaveDate { get; set; }

        public string DepartmentName { get; set; } 

        public string Position { get; set; }

        public int DepartmentId { get; set; }

        //        public override string Rel => LinkTemplates.KontenplanLink.Kontenplan.Rel;
        //        public override string Href => LinkTemplates.KontenplanLink.Kontenplan.CreateLink(new { id = Id }).Href;
        //        public string Name { get; set; }
        //        public long Id { get; set; }
        //
        //        public List<KontoWertRepresentation> KontoWerte { get; set; }
        //        public List<AusgewaehlteBrancheRepresentation> AusgewaehlteBranches { get; set; }
        //        public SteuerungsKriteriumListPresentation AusgewaehlteBranchesList { get; set; }
        //
        //        public string InstitutName { get; set; }
        //        public string Zulassungtyp { get; set; }
        //        public string UmfrageNameDe { get; set; }
        //        public string UmfrageNameEn { get; set; }
        //        public string UmfrageNameFr { get; set; }
        //        public string UmfrageNameIt { get; set; }
        //
        //        public KontenplanStatus Status { get; set; }
        //        public bool IsNotSettingEditKontenplan { get; set; }
        //        public Guid ErfassungswaehrungFestlegen { get; set; }
        //
        //        public DateTime? StichdatumWaehrungsumrechnung { get; set; }
        //        public string StichdatumWaehrungsumrechnungTime { get; set; }
        //        public string Zeitzone { get; set; }
        //
        //        public Guid WertebereichFestlegen { get; set; }
        //        public int MaxLevel { get; set; }
        //        public int ErrorCount { get; set; }
        //        public bool BranchenBestaetigt { get; set; }
    }
}
