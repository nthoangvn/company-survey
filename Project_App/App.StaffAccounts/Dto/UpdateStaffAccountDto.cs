﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Survey.Dto
{
    public class UpdateStaffAccountDto
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string StaffVisa { get; set; }

        public string Phone { get; set; }

        public bool Activated { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime? LeaveDate { get; set; }

        public string Position { get; set; }

        public int DepartmentId { get; set; }
    }
}
