﻿using App.Common.Controllers;
using App.Common.Domain;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using App.StaffAccounts.Representations;
using App.StaffAccounts.Services;
using App.Auth.Services;
using App.Survey.Dto;

namespace App.StaffAccounts.Controllers
{
    [RoutePrefix("api/staffaccounts")]
    public class StaffAccountController : BaseApiController
    {
        private readonly IStaffAccountService _staffAccountService;
        private readonly ICompanyService _companyService;

        public StaffAccountController(IStaffAccountService staffAccountService, ICompanyService companyService)
        {
            _staffAccountService = staffAccountService;
            _companyService = companyService;
        }

        [HttpGet]
        [Route("")]
        public List<StaffAccountRepresentation> GetStaffAccounts()
        {
            var staffAccounts = _staffAccountService.GetStaffAccounts().ToList();
            var result = Mapper.Map<List<BaseEntities_AccountOfStaff>, List<StaffAccountRepresentation>>(staffAccounts);
            return result;
        }

        [HttpGet]
        [Route("branch/{branchId}")]
        public List<StaffAccountRepresentation> GetStaffAccountsByBranchId(int branchId)
        {
            var staffAccounts = _staffAccountService.GetStaffAccountsByDepartmentId(branchId);
            if (staffAccounts != null)
            {
                var result = Mapper.Map<List<BaseEntities_AccountOfStaff>, List<StaffAccountRepresentation>>(staffAccounts.ToList());
                return result;
            }
            else
            {
                return null;
            }
        }

        [HttpGet]
        [Route("{staffAccountId}")]
        public StaffAccountRepresentation GetStaffAccount(int staffAccountId)
        {
            var staffAccount = _staffAccountService.GetStaffAccountById(staffAccountId);
            var result = Mapper.Map<BaseEntities_AccountOfStaff, StaffAccountRepresentation>(staffAccount);
            return result;
        }

        [HttpPost]
        [Route("")]
        public StaffAccountRepresentation CreateStaffAccount([FromBody] CreateStaffAccountDto accountStaffDto)
        {
            var staffAccountData = Mapper.Map<CreateStaffAccountDto, BaseEntities_AccountOfStaff>(accountStaffDto);
            var staffAccount = _staffAccountService.CreateStaffAccount(staffAccountData);
            if (staffAccount == null)
            {
                return null;
            }
            var result = Mapper.Map<BaseEntities_AccountOfStaff, StaffAccountRepresentation>(staffAccount);
            return result;
        }

        [HttpPut]
        [Route("{staffAccountId}")]
        public StaffAccountRepresentation UpdateStaffAccount(int staffAccountId, [FromBody] UpdateStaffAccountDto accountStaffDto)
        {
            var staffAccountData = Mapper.Map<UpdateStaffAccountDto, BaseEntities_AccountOfStaff>(accountStaffDto);
            staffAccountData.Id = staffAccountId;
            var staffAccount = _staffAccountService.UpdateStaffAccount(staffAccountData);
            var result = Mapper.Map<BaseEntities_AccountOfStaff, StaffAccountRepresentation>(staffAccount);
            return result;
        }

        [HttpDelete]
        [Route("{staffAccountId}")]
        public IHttpActionResult DeleteStaffAccount(int staffAccountId)
        {
            var success = _staffAccountService.DeleteStaffAccount(staffAccountId);
            if (success)
            {
                return Ok("Delete staff account success!");
            }
            return BadRequest("Delete staff account failed!");
        }
    }
}
