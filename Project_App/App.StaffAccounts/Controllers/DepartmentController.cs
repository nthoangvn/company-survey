﻿using App.Common.Controllers;
using App.Common.Domain;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using App.StaffAccounts.Representations;
using App.StaffAccounts.Services;
using App.Auth.Services;
using App.Survey.Dto;

namespace App.StaffAccounts.Controllers
{
    [RoutePrefix("api/department")]
    public class DepartmentController : BaseApiController
    {
        private readonly IDepartmentService _departmentService;

        public DepartmentController(IDepartmentService departmentService)
        {
            _departmentService = departmentService;
        }

        [HttpGet]
        [Route("{branchId}")]
        public List<DepartmentRepresentation> GetDepartmentsByBranchId(int branchId)
        {
            var departments = _departmentService.GetDepartmentsByBranchId(branchId);
            if (departments == null)
            {
                return null;
            }
            var result = Mapper.Map<List<BaseEntities_Department>, List<DepartmentRepresentation>>(departments.ToList());
            return result;
        }

    }
}
