USE [MultiNationalCompanyDb_Hoang]
GO
SET IDENTITY_INSERT [dbo].[BranchCompanies] ON 

INSERT [dbo].[BranchCompanies] ([CountryName], [EstablishDate], [Address], [DirectorId]) VALUES ( N'Viet Nam', CAST(N'2005-01-01T00:00:00.000' AS DateTime), N'117 Ly Thuong Kiet, HCM city', N'1')
INSERT [dbo].[BranchCompanies] ([CountryName], [EstablishDate], [Address], [DirectorId]) VALUES ( N'Japan', CAST(N'2008-01-01T00:00:00.000' AS DateTime), N'19 Hashirama, Tokyo', N'2')
INSERT [dbo].[BranchCompanies] ([CountryName], [EstablishDate], [Address], [DirectorId]) VALUES ( N'United Stated', CAST(N'2010-01-01T00:00:00.000' AS DateTime), N'21 Parkson, Washington', N'3')
SET IDENTITY_INSERT [dbo].[BranchCompanies] OFF
SET IDENTITY_INSERT [dbo].[Departments] ON 

INSERT [dbo].[Departments] ([Name], [BranchCompanyId]) VALUES (N'Research & Development', 1)
INSERT [dbo].[Departments] ([Name], [BranchCompanyId]) VALUES (N'Accountant', 1)
INSERT [dbo].[Departments] ([Name], [BranchCompanyId]) VALUES (N'IT system', 1)
INSERT [dbo].[Departments] ([Name], [BranchCompanyId]) VALUES (N'Human Resource', 1)
INSERT [dbo].[Departments] ([Name], [BranchCompanyId]) VALUES ( N'Research & Development', 2)
INSERT [dbo].[Departments] ([Name], [BranchCompanyId]) VALUES ( N'Accountant', 2)
INSERT [dbo].[Departments] ([Name], [BranchCompanyId]) VALUES ( N'IT system', 2)
INSERT [dbo].[Departments] ([Name], [BranchCompanyId]) VALUES ( N'Human Resource', 2)
INSERT [dbo].[Departments] ([Name], [BranchCompanyId]) VALUES ( N'IT system', 3)
INSERT [dbo].[Departments] ([Name], [BranchCompanyId]) VALUES (N'Research & Development', 3)
INSERT [dbo].[Departments] ([Name], [BranchCompanyId]) VALUES (N'Accountant', 3)
INSERT [dbo].[Departments] ([Name], [BranchCompanyId]) VALUES (N'Human Resource', 3)
INSERT [dbo].[Departments] ([Name], [BranchCompanyId]) VALUES ( N'Sales Tech', 1)
SET IDENTITY_INSERT [dbo].[Departments] OFF
SET IDENTITY_INSERT [dbo].[AccountOfStaffs] ON 

INSERT [dbo].[AccountOfStaffs] ([FirstName], [LastName], [StaffVisa], [Phone], [Activated], [StartDate], [LeaveDate], [BranchCompanyId], [SalaryValue], [DepartmentId]) VALUES (N'Nhan', N'Nguyen Thien', N'NNT', N'123541434', 1, CAST(N'2018-01-01T00:00:00.000' AS DateTime), NULL, N'1', NULL, 1)
INSERT [dbo].[AccountOfStaffs] ([FirstName], [LastName], [StaffVisa], [Phone], [Activated], [StartDate], [LeaveDate], [BranchCompanyId], [SalaryValue], [DepartmentId]) VALUES (N'Hoang', N'Nguyen Thai', N'HNT', N'143454354', 1, CAST(N'2017-01-01T00:00:00.000' AS DateTime), NULL, N'1', NULL, 1)
INSERT [dbo].[AccountOfStaffs] ([FirstName], [LastName], [StaffVisa], [Phone], [Activated], [StartDate], [LeaveDate], [BranchCompanyId], [SalaryValue], [DepartmentId]) VALUES (N'Nam', N'Bui Hai', N'NBH', N'214453434', 1, CAST(N'2018-01-01T00:00:00.000' AS DateTime), NULL, N'1', NULL, 2)
SET IDENTITY_INSERT [dbo].[AccountOfStaffs] OFF
SET IDENTITY_INSERT [dbo].[Directors] ON 

INSERT [dbo].[Directors] ([FirstName], [LastName], [Email], [Phone], [Role], [IsActive], [BranchCompanyId]) VALUES (N'John', N'Cena', N'johncena@bk.com', N'123456789', N'GeneralDirector', 1, NULL)
INSERT [dbo].[Directors] ([FirstName], [LastName], [Email], [Phone], [Role], [IsActive], [BranchCompanyId]) VALUES (N'Helena', N'Parkson', N'helenaparkson@bk.com', N'214234324', N'BranchDirector', 1, 1)
INSERT [dbo].[Directors] ([FirstName], [LastName], [Email], [Phone], [Role], [IsActive], [BranchCompanyId]) VALUES ( N'Takashi', N'Hiro', N'takashi@bk.com', N'252343433', N'BranchDirector', 1, 2)
INSERT [dbo].[Directors] ([FirstName], [LastName], [Email], [Phone], [Role], [IsActive], [BranchCompanyId]) VALUES ( N'Alex', N'Xander', N'alexxander@bk.com', N'532423423', N'BranchDirector', 1, 3)
SET IDENTITY_INSERT [dbo].[Directors] OFF
